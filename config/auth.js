// config/auth.js

// expose our config directly to our application using module.exports
module.exports = {

    'facebookAuth' : {
        'clientID'        : '2262703443838847', // your App ID
        'clientSecret'    : '34014eea067462c8b552fbb5d4581b93', // your App Secret
        'callbackURL'     : 'http://localhost:8001/api/v1/auth/facebook/callback', // 'http://localhost:8000/api/v1/auth/facebook/callback',
        'profileURL'      : 'https://graph.facebook.com/v2.5/me?fields=first_name,last_name,email',
        'profileFields'   : ['id', 'email', 'name'] // For requesting permissions from Facebook API

    },

    'twitterAuth' : {
        'consumerKey'        : 'your-consumer-key-here',
        'consumerSecret'     : 'your-client-secret-here',
        'callbackURL'        : 'http://localhost:8080/auth/twitter/callback'
    },

    'googleAuth' : {
        'clientID'         : '1037055028710-0s1te2g060l2uimbn5ojvk1nqmcndmcq.apps.googleusercontent.com',
        'clientSecret'     : 'NLoEhs5-1z7uYFcrYimhb7Y3',
        'callbackURL'      : 'http://localhost:8001/api/v1/auth/google/callback'
    },

    'youTubeAuth' : {
        'clientID'         : 'your-secret-clientID-here',
        'clientSecret'     : 'your-client-secret-here',
        'callbackURL'      : 'http://localhost:8080/auth/youtube/callback'
    }

};
