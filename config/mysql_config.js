var dbConfig = require('./mysql_config.json');
let config = {
  host    : dbConfig.development.host,
  user    : dbConfig.development.username,
  password: dbConfig.development.password,
  database: dbConfig.development.database,
  timezone: 'UTC+0'
};

module.exports = config;
