// load all the things we need
var LocalStrategy    = require('passport-local').Strategy;
var FacebookStrategy = require('passport-facebook').Strategy;
var TwitterStrategy  = require('passport-twitter').Strategy;
var GoogleStrategy   = require('passport-google-oauth').OAuth2Strategy;
var YoutubeStrategy  = require('passport-youtube-v3').Strategy;

let db = require('./../models/associations');

var crypto = require('crypto');
var cryptoRandomString = require('crypto-random-string');
Sequelize = require("sequelize");
Op = Sequelize.Op;

db = require("./../models/associations");
const config = require("../config/default.json");

// load the auth variables
var configAuth = require('./auth'); // use this one for testing

module.exports = function(passport) {

    // =========================================================================
    // passport session setup ==================================================
    // =========================================================================
    // required for persistent login sessions
    // passport needs ability to serialize and unserialize users out of session

    // used to serialize the user for the session
    // passport.serializeUser(function(user, done) {
    //     done(null, user.id);
    // });

    // used to deserialize the user
    // passport.deserializeUser(function(id, done) {
    //     Influencer.findById(id, function(err, user) {
    //         done(err, user);
    //     });
    // });

    // =========================================================================
    // LOCAL LOGIN =============================================================
    // =========================================================================
    passport.use('local-login', new LocalStrategy({
        // by default, local strategy uses username and password, we will override with email
        usernameField : 'email',
        passwordField : 'password',
        passReqToCallback : true // allows us to pass in the req from our route (lets us check if a user is logged in or not)
    },
    function(req, email, password, done) {
        //console.log(email, password);
        
        if (email)
            email = email.toLowerCase(); // Use lower-case e-mails to avoid case-sensitive e-mail matching

        // asynchronous
        process.nextTick(function() {
            influencerObj.findOne({
                where: {
                    email: email,
                },
                order: [ [ 'id', 'DESC' ]],
            }).then(user => {
                // console.log(user.get({
                //     plain: true
                // }))
                if (!user){
                    return done(null, false, "'No user found.'");
                }

                if (user.password == crypto.createHash('md5').update(password).digest('hex')){
                    return done(null, false, "Oops! Wrong password.");
                }

                // all is well, return user
                else{
                    return done(null, user);
                    // return done(null, user.get({
                    //     plain: true
                    // }));
                }
                    
              }
            )
            // .then(([user, created]) => {
            //     console.log(user.get({
            //       plain: true
            //     }))
            //     console.log(user, created);
                
            //     // if(created){
            //     //     if (!user)
            //     //         return done(null, false, req.flash('loginMessage', 'No user found.'));

            //     //     if (!user.validPassword(password))
            //     //         return done(null, false, req.flash('loginMessage', 'Oops! Wrong password.'));
            //     //     res.json({responseCode: 200, responseDetails: 'Success! Influencer has been successfully logged in.'});
            //     // } else{
            //     //   res.json({responseCode: 202, responseDetails: 'Check your email!'});
            //     // }
            //   })
              .catch(error => {
                // Ooops, do some error-handling
                console.log("Something went worng!");
                console.log(error);
                return done(error, false, req.flash('loginMessage', 'No user found.'));

                //res.json({responseCode: 420, responseDetails: 'Something went wrong!', error: error});
              });
            // Influencer.findOne({ 'local.email' :  email }, function(err, user) {
            //     // if there are any errors, return the error
            //     if (err)
            //         return done(err);

            //     // if no user is found, return the message
            //     if (!user)
            //         return done(null, false, req.flash('loginMessage', 'No user found.'));

            //     if (!user.validPassword(password))
            //         return done(null, false, req.flash('loginMessage', 'Oops! Wrong password.'));

            //     // all is well, return user
            //     else
            //         return done(null, user);
            // });
        });

    }));

    // lets create our strategy for web token
    let strategy = new JwtStrategy(jwtOptions, function(jwt_payload, next) {
        //console.log('payload received', jwt_payload);
        //let user = getInfluencer({ id: jwt_payload.id });

        let user = async (obj = { id: jwt_payload.id }) => {
            return await influencerObj.findOne({
                where: obj,
              });
        }
    
        if (user) {
        next(null, user);
        } else {
        next(null, false);
        }
    });
    // use the strategy
    passport.use('jwt', strategy);
  

    // =========================================================================
    // LOCAL SIGNUP ============================================================
    // =========================================================================
    passport.use('local-signup', new LocalStrategy({
        // by default, local strategy uses username and password, we will override with email
        usernameField : 'email',
        passwordField : 'password',
        passReqToCallback : true // allows us to pass in the req from our route (lets us check if a user is logged in or not)
    },
    function(req, email, password, done) {
        if (email)
            email = email.toLowerCase(); // Use lower-case e-mails to avoid case-sensitive e-mail matching

        // asynchronous
        process.nextTick(function() {
            // if the user is not already logged in:
            if (!req.user) {
                Influencer.findOne({ 'local.email' :  email }, function(err, user) {
                    // if there are any errors, return the error
                    if (err)
                        return done(err);

                    // check to see if theres already a user with that email
                    if (user) {
                        return done(null, false, req.flash('signupMessage', 'That email is already taken.'));
                    } else {

                        // create the user
                        var newUser            = new Influencer();

                        newUser.local.email    = email;
                        newUser.local.password = newUser.generateHash(password);

                        newUser.save(function(err) {
                            if (err)
                                return done(err);

                            return done(null, newUser);
                        });
                    }

                });
            // if the user is logged in but has no local account...
            } else if ( !req.user.local.email ) {
                // ...presumably they're trying to connect a local account
                // BUT let's check if the email used to connect a local account is being used by another user
                Influencer.findOne({ 'local.email' :  email }, function(err, user) {
                    if (err)
                        return done(err);

                    if (user) {
                        return done(null, false, req.flash('loginMessage', 'That email is already taken.'));
                        // Using 'loginMessage instead of signupMessage because it's used by /connect/local'
                    } else {
                        var user = req.user;
                        user.local.email = email;
                        user.local.password = user.generateHash(password);
                        user.save(function (err) {
                            if (err)
                                return done(err);

                            return done(null,user);
                        });
                    }
                });
            } else {
                // user is logged in and already has a local account. Ignore signup. (You should log out before trying to create a new account, user!)
                return done(null, req.user);
            }

        });

    }));

    // =========================================================================
    // FACEBOOK ================================================================
    // =========================================================================
    var fbStrategy = configAuth.facebookAuth;
    fbStrategy.passReqToCallback = true;  // allows us to pass in the req from our route (lets us check if a user is logged in or not)
    passport.use(new FacebookStrategy(fbStrategy,
    function(req, token, refreshToken, profile, done) {
        
        //console.log(req, token, refreshToken, profile, done);
        
        // asynchronous
        process.nextTick(function() {
            // check if the user is already logged in
            if (!req.userInfo) {

                let first_name = profile.name.givenName;
                let last_name = profile.name.familyName;
                let user_email = (profile.emails[0].value || '').toLowerCase();;
                let submitData = {
                    first_name: first_name,
                    last_name: last_name,
                    email: user_email,
                    password: '',
                    platform_id: '1',
                    description: '',
                    industry: '',
                    contact_number: '',
                    picture: '',
                    verication_token: '',
                    status: 'Inactive',
                }
                //console.log(submitData);
                
                // influencer list fetch by Sequalize ORM
                db.influencer
                .findOne({where: {email: submitData.email}})
                .then((user) => {
                    // console.log(user.get({
                    //   plain: true
                    // }))
                    if(!user){
                        db.influencer.create(submitData)
                        .then((resultUser) => {
                            if(resultUser){
                                let data = {
                                    social_media_id : profile.id,
                                    social_media_token : token,
                                    influencer_id: resultUser.id,
                                    platform_id: 1
                                }
        
                                db.influencerPlatformMap
                                    .findOne({where: {social_media_token: profile.id, platform_id: 1}}).then((influencerPlatformData) => {
                                        // let emailActivationLink = '';
                                        // let keyValues = { 
                                        //     ACTIVATION_LINK: emailActivationLink,
                                        //     USER_NAME: first_name
                                        // }
                                        let payload = { id: resultUser.id };
                                        let jwtToken = jwt.sign(payload, jwtOptions.secretOrKey);
        
                                        // send sign up mail
                                        //EmailTemplate.sendMailWithTemplate('influencer_sign_up', user_email, keyValues);
                                        
                                        if(influencerPlatformData){
                                            influencerPlatformData.update(data).then((r)=>{
                                                //req.token = jwtToken;
                                                return done(null, resultUser);
                                            }).catch(error => {
                                                // Ooops, do some error-handling
                                                return done(error);
                                            });
                                        } else{
                                            db.influencerPlatformMap.create(data).then((r)=>{
                                                return done(null, jwtToken);
                                            }).catch(error => {
                                                // Ooops, do some error-handling
                                                return done(error);
                                            });
                                        }

                                    }).catch(err => {
                                        return done(err);
                                    });
                            }
                        });
                        
                    } else{
                        return done(null);
                    }
                }).catch(error => {
                    // Ooops, do some error-handling
                    return done(error);
                });


                // db.influencerPlatformMap.findOne({ 'social_media_id' : 1 })
                //     .then((influencerPlatform) =>{
                //         if (influencerPlatform) {

                //             // if there is a user id already but no token (user was linked at one point and then removed)
                //             if (!influencerPlatform.social_media_token) {
                //                 let data = {
                //                     social_media_token : token
                //                 }
                                
                //                 //user.facebook.name  = profile.name.givenName + ' ' + profile.name.familyName;
                //                 //user.facebook.email = (profile.emails[0].value || '').toLowerCase();
    
                //                 influencerPlatform.update(data).then((result) => {
                //                     return done(null, result);
                //                 }).catch(err => {
                //                     return done(err);
                //                 });
                //             }
    
                //             return done(null, user); // user found, return that user
                //         } else {
                //             // if there is no user, create them
                            
                //         }
                //     }).catch(err => {
                //         return done(err);
                //     });

            } else {
                // user already exists and is logged in, we have to link accounts
                var user            = req.userInfo; // pull the user out of the session

                user.facebook.id    = profile.id;
                user.facebook.token = token;
                user.facebook.name  = profile.name.givenName + ' ' + profile.name.familyName;
                user.facebook.email = (profile.emails[0].value || '').toLowerCase();

                user.save(function(err) {
                    if (err)
                        return done(err);

                    return done(null, user);
                });

            }
        });

    }));

    // =========================================================================
    // TWITTER =================================================================
    // =========================================================================
    passport.use(new TwitterStrategy({

        consumerKey     : configAuth.twitterAuth.consumerKey,
        consumerSecret  : configAuth.twitterAuth.consumerSecret,
        callbackURL     : configAuth.twitterAuth.callbackURL,
        passReqToCallback : true // allows us to pass in the req from our route (lets us check if a user is logged in or not)

    },
    function(req, token, tokenSecret, profile, done) {

        // asynchronous
        process.nextTick(function() {

            // check if the user is already logged in
            if (!req.user) {

                Influencer.findOne({ 'twitter.id' : profile.id }, function(err, user) {
                    if (err)
                        return done(err);

                    if (user) {
                        // if there is a user id already but no token (user was linked at one point and then removed)
                        if (!user.twitter.token) {
                            user.twitter.token       = token;
                            user.twitter.username    = profile.username;
                            user.twitter.displayName = profile.displayName;

                            user.save(function(err) {
                                if (err)
                                    return done(err);

                                return done(null, user);
                            });
                        }

                        return done(null, user); // user found, return that user
                    } else {
                        // if there is no user, create them
                        var newUser                 = new Influencer();

                        newUser.twitter.id          = profile.id;
                        newUser.twitter.token       = token;
                        newUser.twitter.username    = profile.username;
                        newUser.twitter.displayName = profile.displayName;

                        newUser.save(function(err) {
                            if (err)
                                return done(err);

                            return done(null, newUser);
                        });
                    }
                });

            } else {
                // user already exists and is logged in, we have to link accounts
                var user                 = req.user; // pull the user out of the session

                user.twitter.id          = profile.id;
                user.twitter.token       = token;
                user.twitter.username    = profile.username;
                user.twitter.displayName = profile.displayName;

                user.save(function(err) {
                    if (err)
                        return done(err);

                    return done(null, user);
                });
            }

        });

    }));

    // =========================================================================
    // GOOGLE ==================================================================
    // =========================================================================
    passport.use(new GoogleStrategy({

        clientID        : configAuth.googleAuth.clientID,
        clientSecret    : configAuth.googleAuth.clientSecret,
        callbackURL     : configAuth.googleAuth.callbackURL,
        passReqToCallback : true // allows us to pass in the req from our route (lets us check if a user is logged in or not)

    },
    function(req, token, refreshToken, profile, done) {
        // asynchronous
        process.nextTick(function() {

            var url_parts = url.parse(req.url.replace('#', '?'), true);
            var query = url_parts.query;
            let userId = query['state'];
            console.log('b', userId);
            

            // check if the user is already logged in
            db.influencerPlatformMap
                .findOne({where:{influencer_id:userId, platform_id:3}})
                .then(result =>{
                    if(result){
                        // update auth
                        result
                            .update({auth:JSON.stringify(token)})
                            .then(r => { console.log('auth saved!'); })
                            .catch(e => { console.log(e); });
                        return done(null);
                    } else{
                        // create auth
                        db.influencerPlatformMap
                            .create({influencer_id: userId, platform_id: 3, auth: JSON.stringify(token)})
                            .then(r => { console.log('auth saved!'); })
                            .catch(e => { console.log(e); });
                        return done(null);
                    }
                }).catch(e =>{
                    console.log(e);
                    return done(e);
                });

        });

    }));

    // =========================================================================
    // YOUTUBE ==================================================================
    // =========================================================================
    passport.use(new YoutubeStrategy({
        clientID: configAuth.youTubeAuth.clientID,
        clientSecret: configAuth.youTubeAuth.clientSecret,
        callbackURL: configAuth.youTubeAuth.callbackURL,
        scope: ['https://www.googleapis.com/auth/youtube.readonly'],
        passReqToCallback : true
    },
    function(accessToken, refreshToken, profile, done) {
        // asynchronous
        process.nextTick(function() {
            // check if the user is already logged in
            if (!req.user) {
                influencerObj
                .findOrCreate({where: {email: submitData.email}, defaults: submitData})
                .then(([user, created]) => {
                console.log(user.get({
                    plain: true
                }))
                if(created){
                    res.json({responseCode: 200, responseDetails: 'Success! Influencer data has been successfully saved.'});
                } else{
                    res.json({responseCode: 202, responseDetails: 'Email already exist.'});
                }
                }).catch(error => {
                // Ooops, do some error-handling
                console.log("Something went worng!");
                console.log(error);
                res.json({responseCode: 420, responseDetails: 'Something went wrong!', error: error});
                });
            } else{
                
            }
        });
    }));
};
