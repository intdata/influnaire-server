'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('proposals', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      campaign_id: {
        type: Sequelize.INTEGER
      },
      influencer_id: {
        type: Sequelize.INTEGER
      },
      subject: {
        type: Sequelize.TEXT
      },
      brief: {
        type: Sequelize.TEXT
      },
      description: {
        type: Sequelize.TEXT
      },
      delivery_date: {
        type: Sequelize.DATE
      },
      currency: {
        type: Sequelize.STRING
      },
      price: {
        type: Sequelize.FLOAT
      },
      required_resources: {
        type: Sequelize.TEXT
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      status: {
        type: Sequelize.ENUM,
        values: ['ACCEPTED', 'REJECTED', 'PENDING'],
        defaultValue: 'PENDING'
      },
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('proposals');
  }
};