'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('notification', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      title: {
        type: Sequelize.TEXT
      },
      content: {
        type: Sequelize.TEXT
      },
      sender_id: {
        type: Sequelize.INTEGER
      },
      receiver_id: {
        type: Sequelize.INTEGER
      },
      receiver_type: {
        type: Sequelize.ENUM,
        values: ['Influencer', 'BrandOwner'],
        defaultValue: null
      },
      deleted_by_sender: {
        type: Sequelize.ENUM,
        values: ['0', '1'],
        defaultValue: '0'
      },
      deleted_by_receiver: {
        type: Sequelize.ENUM,
        values: ['0', '1'],
        defaultValue: '0'
      },
      status: {
        type: Sequelize.ENUM,
        values: ['0', '1'],
        defaultValue: '1'
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('notification');
  }
};