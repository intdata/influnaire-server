'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('brand_owner', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      title: {
        type: Sequelize.STRING
      },
      fname: {
        type: Sequelize.STRING
      },
      lname: {
        type: Sequelize.STRING
      },
      company_name: {
        type: Sequelize.STRING
      },
      country_code: {
        type: Sequelize.STRING
      },
      currency: {
        type: Sequelize.STRING
      },
      email: {
        type: Sequelize.STRING
      },
      password: {
        type: Sequelize.STRING
      },
      profile_image: {
        type: Sequelize.STRING
      },
      company_logo: {
        type: Sequelize.STRING
      },
      industry: {
        type: Sequelize.INTEGER
      },
      company_type: {
        type: Sequelize.INTEGER
      },
      company_ph_no: {
        type: Sequelize.STRING
      },
      picture: {
        type: Sequelize.STRING
      },
      verication_token: {
        type: Sequelize.STRING
      },
      forgot_password_token: {
        type: Sequelize.STRING
      },
      forgot_password_requested_at: {
        type: Sequelize.DATE
      },
      is_email_verified: {
        type: Sequelize.BOOLEAN
      },
      status: {
        type: Sequelize.STRING
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE
      }
    }).then(() => {
      console.log('created brand_owner table');
      return queryInterface.sequelize.query(`
        CREATE EVENT expire_token_brand_owner
        ON SCHEDULE AT CURRENT_TIMESTAMP + INTERVAL 15 MINUTE
        DO
        UPDATE brand_owner SET verication_token = '' WHERE createdAt < DATE_SUB(NOW(), INTERVAL 15 MINUTE);
        `)
    }).then(() => { console.log('expireToken event created') });;;
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('brand_owner');
  }
};