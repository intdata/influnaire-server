'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('influencer', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      first_name: {
        type: Sequelize.STRING
      },
      first_name: {
        type: Sequelize.STRING
      },
      platform_id: {
        type: Sequelize.INTEGER
      },
      description: {
        type: Sequelize.TEXT
      },
      email: {
        type: Sequelize.INTEGER
      },
      password: {
        type: Sequelize.STRING
      },
      contact_number: {
        type: Sequelize.STRING
      },
      industry: {
        type: Sequelize.STRING
      },
      gender: {
        type: Sequelize.ENUM,
        values: ['M', 'F', 'O'],
        defaultValue: null
      },
      dob:  {
        type: Sequelize.DATE
      },
      country: {
        type: Sequelize.STRING
      },
      nationality: {
        type: Sequelize.STRING
      },
      picture: {
        type: Sequelize.STRING
      },
      is_agree_to_perform: {
        type: Sequelize.ENUM,
        values: ['0', '1'],
        defaultValue: null
      },
      verication_token: {
        type: Sequelize.STRING
      },
      is_email_verified: {
        type: Sequelize.BOOLEAN
      },
      forgot_password_token: {
        type: Sequelize.STRING
      },
      forgot_password_requested_at: {
        type: Sequelize.DATE
      },
      verication_token: {
        type: Sequelize.STRING
      },
      is_email_verified: {
        type: Sequelize.BOOLEAN
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE
      }
    }).then(() => {
      console.log('created influencer table');
      return queryInterface.sequelize.query(`
        CREATE EVENT expire_token_influncer
        ON SCHEDULE AT CURRENT_TIMESTAMP + INTERVAL 15 MINUTE 
        DO
        UPDATE influencer SET verication_token = '' WHERE createdAt < DATE_SUB(NOW(), INTERVAL 15 MINUTE);
        `)
    }).then(() => { console.log('expireToken event created') });;
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('influencer');
  }
};