var express = require('express');
var router = express.Router();
var moment = require('moment');
var im = require('imagemagick');
var fs = require('fs');

const auth = require("../middleware/auth");
const upload = require("../middleware/upload");
const config = require("../config/default.json");

db = require("./../models/associations");
Sequelize = require("sequelize");
Op = Sequelize.Op;
Brand = require("./../models/brand");
var brandObj = Brand(sequelize, Sequelize);

const brandImagePath = config.adminAssetpath + "uploads/brand/";
const brandImageHttpPath = config.adminBaseUrl + "uploads/brand/";
const brandImageThumbPath = config.adminAssetpath + "uploads/brand/thumb/";
/**
 * Create a new brand;
 */
router.post('/save', [auth, upload.single({path: brandImagePath, fieldName: 'logo'})], passport.authenticate('jwt', { session: false }),  async function(req, res){
  
  let { industry, country, brandName, platform} = req.body;
  console.log(industry+'==>'+country+'==>'+brandName+'==>'+platform);
  if(!req.userInfo){
    return res.status(200).json({
        responseCode: "420",
        responseDetails: "Something went wrong!",
    });
  }

  if (!industry || !country || !brandName || !platform) {
      return res.status(200).json({
          responseCode: "201",
          responseDetails: "Parameter missing.",
      });
  }
  let brand_owner_id = req.userInfo.id;
  let submitData = {
    industry_id: industry,
    country: country,
    brand_name: brandName,
    platform_id: platform,
    brand_owner_id: brand_owner_id,
    created_at: sequelize.fn('NOW')
  }
  let imgName = '';
  
  if(req.fileValidationError) {
      return res.status(200).json({
        responseCode: "202",
        responseDetails: req.fileValidationError,
      });
  }else{
    if(req.file){
      imgName = req.file.filename;  
      submitData.logo =  imgName;
      fs.readFile(req.file.path, function (err, data) {
        fs.writeFile(brandImagePath, data, function (err) {
            // write file to uploads/thumbs folder
            im.resize({
                srcPath: brandImagePath + imgName,
                dstPath: brandImageThumbPath + imgName,
                width: 200
            }, function (err, stdout, stderr) {
                
            });
    
        });
      }); 
    }
  }
 
  //check if the same brandname,country,industry already exists
  let lookupValue = brandName.toLowerCase();
  let isIexists =  await brandObj.count({
                    where: {
                      brand_owner_id: brand_owner_id,
                      industry_id: industry,
                      country: country,
                      brand_name: sequelize.where(sequelize.fn('LOWER', sequelize.col('brand_name')), '=', lookupValue),
                    },
                  });

  //console.log(isIexists);return;
  if(isIexists==0)
  {
    return db.brand.create(submitData)
    .then(retData => { 
      return res.status(200).json({ responseCode: 200, responseDetails: 'Brand Created Successfully', data: retData});
    })
    .catch(reason => {
      return res.status(200).json({ responseCode: 420, responseDetails: 'Something went wrong!.', "error": reason});
    });
  }else{
    return res.status(200).json({ responseCode: 420, responseDetails: 'Brand name alreay exists.', "error": ''});
  }

});

/**
 * Edit brand
 */
router.post('/edit', [auth, upload.single({path: brandImagePath, fieldName: 'logo'})], passport.authenticate('jwt', { session: false }),  async function(req, res){
  
  let { industry, country, brandName, platform, oldImage} = req.body;
  //console.log(industry+'=>'+ country+'=>'+ brandName+'=>'+ platform+'=>'+ oldImage)
  if(!req.userInfo){
    return res.status(200).json({
        responseCode: "420",
        responseDetails: "Something went wrong!",
    });
  }

  if (!industry || !country || !brandName || !platform) {
      return res.status(200).json({
          responseCode: "201",
          responseDetails: "Parameter missing.",
      });
  }
  let brand_owner_id = req.userInfo.id;
  let submitData = {
    industry_id: industry,
    country: country,
    brand_name: brandName,
    platform_id: platform,
    brand_owner_id: brand_owner_id,
    updated_at: sequelize.fn('NOW')
  }
  let imgName = '';
  let brandId = req.body.id;
  //console.log(brandId);return;
  //check if the same brandname,country,industry already exists
  let lookupValue = brandName.toLowerCase();
  let isIexists =  await brandObj.count({
                    where: {
                      id: { [Op.notIn]: [brandId]},
                      brand_owner_id: brand_owner_id,
                      industry_id: industry,
                      country: country,
                      brand_name: sequelize.where(sequelize.fn('LOWER', sequelize.col('brand_name')), '=', lookupValue),
                    },
                  });

  if(isIexists > 0)
  {
    return res.status(200).json({ responseCode: 420, responseDetails: 'Brand name alreay exists.', "error": ''});
  }
  
  if(req.fileValidationError) {
      return res.status(200).json({
        responseCode: "202",
        responseDetails: req.fileValidationError,
      });
  }else{
    if(req.file){
      imgName = req.file.filename;  
      submitData.logo =  imgName
      if(oldImage){
        fs.unlink(brandImagePath + oldImage, (err) => {
          if (err) {
            console.error(err)
            return
          }
          //file removed
        });
      }
      fs.readFile(req.file.path, function (err, data) {
        fs.writeFile(brandImagePath, data, function (err) {
            // write file to uploads/thumbs folder
            im.resize({
                srcPath: brandImagePath + imgName,
                dstPath: brandImageThumbPath + imgName,
                width: 200
            }, function (err, stdout, stderr) {
              if(oldImage){
                fs.unlink(brandImageThumbPath + oldImage, (err) => {
                  if (err) {
                    console.error(err)
                    return
                  }
                  //file removed
                });
              }
                
            });
    
        });
      });
    }
  }
  
  return db.brand.update(submitData,{where:{'id':req.body.id}})
    .then(retData => { 
      return res.status(200).json({ responseCode: 200, responseDetails: 'Brand Edited Successfully', data: retData});
    })
    .catch(reason => {
      return res.status(200).json({ responseCode: 420, responseDetails: 'Something went wrong!.', "error": reason});
    });


});

/**
 * Get brands of a brand owner
 */
router.post('/brand-of-owner', auth, passport.authenticate('jwt', { session: false }),  async function(req, res, next) {

  if(!req.userInfo){
    return res.status(200).json({
        responseCode: "420",
        responseDetails: "Something went wrong!",
    });
  }
  let brand_owner_id = req.userInfo.id;

  return db.brand.findAll({
    where:[{ brand_owner_id: brand_owner_id}],
    include:[{model:db.platform, attributes: ['class_name', 'name']}, {model:db.industries}]
  })
  .then(data => { 
    return res.status(200).json({ responseCode: 200, responseDetails: 'Get brand data.', data: data, brandImagePath:brandImageHttpPath});
  })
  .catch(reason => {
    return res.status(200).json({ responseCode: 420, responseDetails: 'Something went wrong!.', "error": reason});
  });
   
  });

/**
 * Get brands of a brand owner
 */
router.post('/brand-of-owner-list', auth, passport.authenticate('jwt', { session: false }),  async function(req, res, next) {

  if(!req.userInfo){
    return res.status(200).json({
        responseCode: "420",
        responseDetails: "Something went wrong!",
    });
  }
  let brand_owner_id = req.userInfo.id;

  return db.brand.findAll({
    where:[{ brand_owner_id: brand_owner_id}],
    attributes: ['id', 'brand_name']
  })
  .then(data => { 
    return res.status(200).json({ responseCode: 200, responseDetails: 'Get brand list.', data: data, brandImagePath:brandImageHttpPath});
  })
  .catch(reason => {
    return res.status(200).json({ responseCode: 420, responseDetails: 'Something went wrong!.', "error": reason});
  });
   
  });


/**
 *Delete Single Brand
 */
router.post('/delete', auth, passport.authenticate('jwt', { session: false }),  async function(req, res, next) {

  if(!req.userInfo){
    return res.status(200).json({
        responseCode: "420",
        responseDetails: "Something went wrong!",
    });
  }
  let brand_owner_id = req.userInfo.id;
  let id = req.body.id;
  if (!id) {
    return res.status(200).json({
        responseCode: "201",
        responseDetails: "Parameter missing.",
    });
}


return db.brand.destroy({
  where:{ id: id},
})
.then(data => { 
  return res.status(200).json({ responseCode: 200, responseDetails: 'Brand Deleted Successfully.', data: data, brandImagePath:brandImageHttpPath});
})
.catch(reason => {
  return res.status(200).json({ responseCode: 420, responseDetails: 'Something went wrong!.', "error": reason});
});
  
});

module.exports = router;
