var express = require('express');
var crypto = require('crypto');
var cryptoRandomString = require('crypto-random-string');
var router = express.Router();
var moment = require('moment');
var im = require('imagemagick');
var fs = require('fs');
var https = require('https');
var request = require("request");

const auth = require("../middleware/auth");
const upload = require("../middleware/upload");
const config = require("../config/default.json");

//**=============== Sequalize init =============== */
Sequelize = require("sequelize");
Op = Sequelize.Op;
Influencer = require("./../models/influencer");
BrandOwner = require("./../models/brandowner");
Newsletter = require("./../models/newsletter");
Currency = require("./../models/currencies");
Industry = require("./../models/industries");
EmailTemplate = require("./../models/emailtemplate")(sequelize, Sequelize);
//InfluencersPlatformMap = require("./../models/influencer_platform_map");

var influencerObj = Influencer(sequelize, Sequelize);
var brandOwnerObj = BrandOwner(sequelize, Sequelize);
var newsletterObj = Newsletter(sequelize, Sequelize);
var currencyObj = Currency(sequelize, Sequelize);
var industryObj = Industry(sequelize, Sequelize);
//**=============== Sequalize init =============== */


const brandOwnerImagePath = config.adminAssetpath + "uploads/brand_owner/";
const brandOwnerImageThumbPath = config.adminAssetpath + "uploads/brand_owner/thumb/";
const influencerImagePath = config.adminAssetpath + "uploads/influencer/";
const influencerImageThumbPath = config.adminAssetpath + "uploads/influencer/thumb/";

/* GET users listing. */
router.get('/', function(req, res, next) {
  return res.send('Unauthorized!');
});

//Node.js Function to save image from External URL.
function saveImageToDisk(url, localPath) {
  let fullUrl = url;
  let file = fs.createWriteStream(localPath);
  let request = https.get(url, function(response) {
    response.pipe(file);
  });
}

function fbImageSave(url, localPath){
  request(url).pipe(fs.createWriteStream(localPath))
    .on('close', function(){
        console.log("fb image saving process is done!");
    });
}

/**
 * Submit influencer signup data
 */
router.post('/influencer_signup',
    function (req, res) {      
        let name = req.body.name.split(" ");
        var user_email = req.body.email;
        let password = req.body.password;
        let hashPassword = crypto.createHash('md5').update(password).digest('hex');     // Password Hashing
        var token = cryptoRandomString({length: 16, type: 'url-safe'});
        let submitData = {
          first_name: name.hasOwnProperty('0') ? name[0] : '',
          last_name: name.hasOwnProperty('1') ? name[1] : '',
          email: req.body.email,
          password: hashPassword,
          platform_id: '0',
          description: '',
          industry: '',
          contact_number: '',
          picture: '',
          verication_token: token,
          status: 'Inactive',
        }
        //console.log(submitData);
        
        // influencer list fetch by Sequalize ORM
        influencerObj
          .findOrCreate({where: {email: submitData.email}, defaults: submitData})
          .then(([user, created]) => {
            // console.log(user.get({
            //   plain: true
            // }))
            if(created){
              let emailActivationLink = config.frontEndBaseUrl + '/signup?type=influencer&do=email_verification&email='+ user_email + '&verication_token='+ token;
              let keyValues = { 
                ACTIVATION_LINK: emailActivationLink,
                USER_NAME: req.body.name
              }

              // send sign up mail
              EmailTemplate.sendMailWithTemplate('influencer_sign_up', user_email, keyValues);

              return res.json({responseCode: 200, responseDetails: 'Success! Influencer data has been successfully saved.'});
            } else{
              return res.json({responseCode: 202, responseDetails: 'Email already exist.'});
            }
          }).catch(error => {
            // Ooops, do some error-handling
            console.log("Something went worng!");
            console.log(error);
            return res.json({responseCode: 420, responseDetails: 'Something went wrong!', error: error});
          });
    }
);

/**
 * Influencer social connect
 */
router.post('/influencer_social_connect',
    function (req, res) {    
        let name = req.body.name.split(" ");
        let user_email = req.body.email;
        let doLogin = req.body.doLogin | false;
        let sendMail = req.body.sendMail | true;
        let platform_id = 0;
        if(req.body.provider == 'facebook'){
          platform_id = 1
        } else if(req.body.provider == 'google'){
          platform_id = 3;
        }
        let image = req.body.image;
        
        let submitData = {
          first_name: name.hasOwnProperty('0') ? name[0] : '',
          last_name: name.hasOwnProperty('1') ? name[1] : '',
          email: req.body.email,
          password: '',
          platform_id: platform_id,
          description: '',
          industry: '',
          contact_number: '',
          picture: '',
          verication_token: '',
          status: doLogin ? 'Active' : 'Inactive',
        }

        // influencer list fetch by Sequalize ORM
        influencerObj
          .findOne({where: {email: submitData.email}})
          .then((influencerData) => {
            // console.log(user.get({
            //   plain: true
            // }))
            if(!influencerData){

              // upload image
              if(image != ''){
                let imageName = `social-${Date.now()}.jpg`;
                let imagePath = `${influencerImagePath}${imageName}`;
                let imagePathThumb = influencerImageThumbPath + imageName;
                saveImageToDisk(image, imagePath);
                saveImageToDisk(image, imagePathThumb);
                submitData.picture = imageName;
              }

              influencerObj
              .create(submitData)
              .then((influencerData) => {

                  if(sendMail){
                    let keyValues = { 
                      USER_NAME: submitData.first_name
                    }
                    // send sign up mail
                    EmailTemplate.sendMailWithTemplate('influencer_social_sign_up', user_email, keyValues); 
                  }
    
                  let data = {
                    social_media_id : req.body.id,
                    social_media_token : req.body.token,
                    influencer_id: influencerData.id,
                    platform_id: platform_id
                  }

                  db.influencerPlatformMap
                    .findOne({where: {social_media_id: data.social_media_id, platform_id: platform_id}})
                    .then((influencerPlatformData) => {
                        let payload = { id: influencerData.id };
                        let jwtToken = jwt.sign(payload, jwtOptions.secretOrKey);
                        let profileImage = config.adminBaseUrl + 'uploads/influencer/' + influencerData.picture;
                        let noImage = config.noprofileimage;
                        let userData = {
                          name: submitData.first_name,
                          profileImage: influencerData.picture ? profileImage : noImage
                        };
                        
                        if(influencerPlatformData){
                            influencerPlatformData.update(data).then((r)=>{
                              if(doLogin){
                                return res.json({responseCode: 200, responseDetails: 'Success! logged in.', token: jwtToken, userInfo: userData});
                              } else{
                                return res.json({responseCode: 200, responseDetails: 'Success! Influencer data has been successfully saved.'});
                              }
                            }).catch(error => {
                                // Ooops, do some error-handling
                                return res.json({responseCode: 420, responseDetails: 'Something went wrong4!', error: error});
                            });
                        } else{
                            db.influencerPlatformMap.create(data).then((r)=>{
                              if(doLogin){
                                return res.json({responseCode: 200, responseDetails: 'Success! logged in.', token: jwtToken, userInfo: userData});
                              } else{
                                return res.json({responseCode: 200, responseDetails: 'Success! Influencer data has been successfully saved.'});
                              }
                            }).catch(error => {
                                // Ooops, do some error-handling
                                return res.json({responseCode: 420, responseDetails: 'Something went wrong3!', error: error});
                            });
                        }
    
                    }).catch(error => {
                      return res.json({responseCode: 420, responseDetails: 'Something went wrong2!', error: error});
                    });
              }).catch(error => {
                return res.json({responseCode: 420, responseDetails: 'Something went wrong1!', error: error});
              });

            } else{
              if(influencerData.platform_id == '0'){
                return res.json({responseCode: 202, responseDetails: 'Email already exist.'});
              } else{
                if(!doLogin){
                  return res.json({responseCode: 202, responseDetails: 'User already registered.'});
                }

                // upload image
                if(image != ''){
                  let imageName = `social-${Date.now()}.jpg`;
                  let imagePath = `${influencerImagePath}${imageName}`;
                  let imagePathThumb = influencerImageThumbPath + imageName;
                  let oldImgName = influencerData.picture;
                  if(req.body.provider == 'facebook'){
                    fbImageSave(image.replace("normal", "large"), imagePath);
                    fbImageSave(image, imagePathThumb);
                  } else if(req.body.provider == 'google'){
                    saveImageToDisk(image, imagePath);
                    saveImageToDisk(image, imagePathThumb);
                  }
                  influencerData
                  .update({picture: imageName})
                  .then((r) => {
                    fs.unlink(`${influencerImagePath}${oldImgName}`, (err) => {
                      if (err) {
                        console.error(err)
                        return
                      }
                      //file removed
                    });
                    fs.unlink(`${influencerImageThumbPath}${oldImgName}`, (err) => {
                      if (err) {
                        console.error(err)
                        return
                      }
                      //file removed
                    });
                  }).catch(error => {

                  });
                }

                // update platform token
                let data = {
                  social_media_id : req.body.id,
                  social_media_token : req.body.token,
                  influencer_id: influencerData.id,
                  platform_id: platform_id
                }

                db.influencerPlatformMap
                .findOne({where: {social_media_id: data.social_media_id, platform_id: platform_id}})
                .then((influencerPlatformData) => {
                    let payload = { id: influencerData.id };
                    let jwtToken = jwt.sign(payload, jwtOptions.secretOrKey);
                    let profileImage = config.adminBaseUrl + 'uploads/influencer/' + influencerData.picture;
                    let noImage = config.noprofileimage;
                    let userData = {
                      name: influencerData.first_name,
                      profileImage: influencerData.picture ? profileImage : noImage
                    };
                    
                    if(influencerPlatformData){
                        influencerPlatformData.update(data).then((r)=>{
                          if(doLogin){
                            return res.json({responseCode: 200, responseDetails: 'Success! logged in.', token: jwtToken, userInfo: userData});
                          } else{
                            return res.json({responseCode: 200, responseDetails: 'Success! Influencer data has been successfully saved.'});
                          }
                        }).catch(error => {
                            // Ooops, do some error-handling
                            return res.json({responseCode: 420, responseDetails: 'Something went wrong!', error: error});
                        });
                    } else{
                        db.influencerPlatformMap.create(data).then((r)=>{
                          if(doLogin){
                            return res.json({responseCode: 200, responseDetails: 'Success! loged in.', token: jwtToken, userInfo: userData});
                          } else{
                            return res.json({responseCode: 200, responseDetails: 'Success! Influencer data has been successfully saved.'});
                          }
                        }).catch(error => {
                            // Ooops, do some error-handling
                            return res.json({responseCode: 420, responseDetails: 'Something went wrong!', error: error});
                        });
                    }

                }).catch(error => {
                  return res.json({responseCode: 420, responseDetails: 'Something went wrong!', error: error});
                });
              }
            }
          }).catch(error => {
            // Ooops, do some error-handling
            console.log("Something went worng!");
            console.log(error);
            return res.json({responseCode: 420, responseDetails: 'Something went wrong!', error: error});
          });
    }
);

/**
 * Submit brand owner signup data
 */
router.post('/brand_owner_signup',
    function (req, res) {
        let password = req.body.password;
        let hashPassword = crypto.createHash('md5').update(password).digest('hex');     // Password Hashing
        var token = cryptoRandomString({length: 16, type: 'url-safe'});
        let submitData = {
          company_name: req.body.companyName,
          country_code: req.body.countryName,
          title: req.body.title,
          fname: req.body.firstName,
          lname: req.body.lastName,
          email: req.body.email,
          title: req.body.title,
          password: hashPassword,
          profile_image: '',
          company_logo: '',
          industry: 1,
          company_type: 1,
          verication_token: token,
          company_ph_no: req.body.phoneNumber,
          status: 'Inactive',
        }
        //console.log('submitData=>',submitData);
        
        // influencer list fetch by Sequalize ORM
        brandOwnerObj
          .findOrCreate({where: {email: submitData.email}, defaults: submitData})
          .then(([user, created]) => {
            // console.log(user.get({
            //   plain: true
            // }))
            if(created){
              var emailActivationLink = config.frontEndBaseUrl + '/signup?type=brand_owner&do=email_verification&email='+ req.body.email + '&verication_token='+ token;

              let keyValues = { 
                ACTIVATION_LINK: emailActivationLink,
                USER_NAME: req.body.firstName
              }

              // send sign up mail
              EmailTemplate.sendMailWithTemplate('brand_owner_sign_up', req.body.email, keyValues);

              return res.json({responseCode: 200, responseDetails: 'Success! Brand Owner data has been successfully saved.'});
            } else{
              return res.json({responseCode: 202, responseDetails: 'Email already exist.'});
            }
          }).catch(error => {
            // Ooops, do some error-handling
            console.log("Something went worng!");
            console.log(error);
            return res.json({responseCode: 420, responseDetails: 'Something went wrong!', error: error});
          });
    }
);

/**
 * Submit influencer login data
 */
router.post('/influencer_login', async function(req, res, next) {
  const { email, password } = req.body;
  if (email && password) {
    let user = await getInfluencer({ email: email });
    if (!user) {
      return res.status(200).json({responseCode: 404, responseDetails: 'No such user found.'});
    }
    if (user.password == crypto.createHash('md5').update(password).digest('hex')) {
      // from now on we'll identify the user by the id and the id is the 
      // only personalized value that goes into our token
      if(user.is_email_verified == 1){      
        let payload = { id: user.id };
        let token = jwt.sign(payload, jwtOptions.secretOrKey);
        let profileImage = config.adminBaseUrl + 'uploads/influencer/' + user.picture;
        let noImage = config.noprofileimage;
        let userData = {
          name: user.first_name,
          profileImage: user.picture ? profileImage : noImage
        };
        return res.json({responseCode: 200, responseDetails: 'Influencer login success!', token: token, userInfo: userData});
      }
      else{
        var token = cryptoRandomString({length: 16, type: 'url-safe'});

        //console.log("====================");
        // influencer list fetch by Sequalize ORM
        user
          .update({verication_token: token})
          .then((InfluencerData) => {
     
         //console.log(token); 
         //console.log("+++++++++++++++++++++");  
              let emailActivationLink = config.frontEndBaseUrl + '/signup?type=influencer&do=email_verification&email='+ email + '&verication_token='+ token;
              let keyValues = { 
                ACTIVATION_LINK: emailActivationLink,
                USER_NAME: user.first_name
              }

              // send activation mail
              EmailTemplate.sendMailWithTemplate('influencer_sign_up', email, keyValues);
            
          }).catch(error => {
            return res.json({responseCode: 420, responseDetails: 'Something went wrong!', error: error});
          });

        return res.json({responseCode: 202, responseDetails: 'Email is not veryfied. We have sent a verification link into your email. After verification '});
      }
    } else {
      return res.status(200).json({responseCode: 201, responseDetails: 'Password is incorrect.'});
    }
  }
});

/**
 * Submit brand owner login data
 */
router.post('/brand_owner_login', async function(req, res, next) {
  const { email, password } = req.body;
  if (email && password) {
    let user = await getBrandOwner({ email: email });
    if (!user) {
      return res.status(200).json({responseCode: 404, responseDetails: 'No such user found.'});
    }
    if (user.password == crypto.createHash('md5').update(password).digest('hex')) {
      // from now on we'll identify the user by the id and the id is the 
      // only personalized value that goes into our token
      //console.log(user.is_email_verified);return;
      if(user.is_email_verified == 2){
        let payload = { id: user.id };
        let token = jwt.sign(payload, jwtOptions.secretOrKey);
        let profileImage = config.adminBaseUrl + 'uploads/brand_owner/' + user.profile_image;
        let noImage = config.noprofileimage;
        let userData = {
          name: user.fname,
          profileImage: user.profile_image ? profileImage : noImage
        };
        return res.json({responseCode: 200, responseDetails: 'Successfully loggedin!', token: token, userInfo: userData});
      }
      else
      {
        if(user.is_email_verified == 0){
          var token = cryptoRandomString({length: 16, type: 'url-safe'});
          //console.log("====================");
          // influencer list fetch by Sequalize ORM
          user
            .update({verication_token: token})
            .then((InfluencerData) => {
          //console.log(token); 
          //console.log("+++++++++++++++++++++");  
                let emailActivationLink = config.frontEndBaseUrl + '/signup?type=brand_owner&do=email_verification&email='+ req.body.email + '&verication_token='+ token;
                let keyValues = { 
                  ACTIVATION_LINK: emailActivationLink,
                  USER_NAME: user.fname
                }
                // send activation mail
                EmailTemplate.sendMailWithTemplate('brand_owner_sign_up', email, keyValues);
              
            }).catch(error => {
              return res.json({responseCode: 420, responseDetails: 'Something went wrong!', error: error});
            });
          return res.json({responseCode: 202, responseDetails: 'Email is not veryfied. We have sent a verification link into your email. After verification '});
          }else if(user.is_email_verified == 1)
          {
            return res.json({responseCode: 203, responseDetails: 'You can login after admin approval'});
          }
        
      }
    } else {
      return res.status(200).json({responseCode: 201, responseDetails: 'Password is incorrect.'});
    }
  }
});

// protected route
router.post('/protected', auth, passport.authenticate('jwt', { session: false }), function(req, res) {
  return res.json('Success! You can now see this without a token.');
});

/**
 * Method to get brand owner details by id
 */
router.post('/get-brand-owner-by-id', auth, passport.authenticate('jwt', { session: false }),  async function(req, res) {
  
  let { id } = req.body;
  if(!req.userInfo){
    return res.status(200).json({
        responseCode: "420",
        responseDetails: "Something went wrong!",
    });
  }

  if (!id) {
      id = req.userInfo.id;
      // return res.status(200).json({
      //     responseCode: "201",
      //     responseDetails: "Parameter missing.",
      // });
  }
    
  let user = await getBrandOwner({ id: id });
  console.log('==================>')
  console.log(user)
  if(user){
    let currency = await getCurrency({ id: user.currency });
    //console.log(currency);return;
    let currencyName = currency.name;
    let industry = await industryObj.findOne({
      where: {id:user.industry},
    });

    let industryName = industry.name;
   
    let profileImage = config.adminBaseUrl + 'uploads/brand_owner/' + user.profile_image;
    let companyLogo = config.adminBaseUrl + 'uploads/brand_owner/' + user.company_logo;
    let noImagePath = config.noprofileimage;
    let companyDefaultLogo = config.companyDefaultLogo;
    personalData = {
      title: user.title,
      firstName: user.fname,
      lastName: user.lname,
      email: user.email,
      personalPhNo: user.company_ph_no,
      photo: user.profile_image ? profileImage : noImagePath
    };
    companyData = {
      companyName: user.company_name,
      companyLogo: user.company_logo ? companyLogo : companyDefaultLogo,
      companyPhNo: user.company_ph_no,
      industry: user.industry,
      industryName: industryName,
      countryCode: user.country_code,
      countryName: user.countryName,
      companyType: user.company_type,
      currency: user.currency,
      currencyName: currencyName,
    }
    return res.status(200).json({
        responseCode: 200,
        responseDetails: "Success",
        data: { personalData: personalData, companyData: companyData }
    });
  } else{
    return res.status(200).json({
        responseCode: 404,
        responseDetails: "No user found.",
    });
  }
});

/**
 * Method to update brand owner personal info
 */
router.post('/change-brand-personal-info', [auth, upload.single({path: brandOwnerImagePath, fieldName: 'profileImage'})], passport.authenticate('jwt', { session: false }),  async function(req, res){
  let { firstName, lastName, title, email, phNo } = req.body;

  if(!req.userInfo){
    return res.status(200).json({
        responseCode: "420",
        responseDetails: "Something went wrong!",
    });
  }

  if (!firstName || !lastName || !title || !email || !phNo) {
      return res.status(200).json({
          responseCode: "201",
          responseDetails: "Parameter missing.",
      });
  }

  let imgName = '';
  let id = req.userInfo.id;
  let user = await getBrandOwner({ id: id });

  if(req.fileValidationError) {
      return res.status(200).json({
        responseCode: "202",
        responseDetails: req.fileValidationError,
      });
  } else{
    if(req.file){
      imgName = req.file.filename;
      let oldImgName = user.profile_image;
      fs.unlink(brandOwnerImagePath + oldImgName, (err) => {
        if (err) {
          console.error(err)
          return
        }
      
        //file removed
      });

      fs.readFile(req.file.path, function (err, data) {
          fs.writeFile(brandOwnerImagePath, data, function (err) {
              // write file to uploads/thumbs folder
              im.resize({
                  srcPath: brandOwnerImagePath + imgName,
                  dstPath: brandOwnerImageThumbPath + imgName,
                  width: 200
              }, function (err, stdout, stderr) {
                  //if (err) throw err;
                  //console.log('resized image to fit within 200x200px');
                  fs.unlink(brandOwnerImageThumbPath + oldImgName, (err) => {
                    if (err) {
                      console.error(err)
                      return
                    }
                  
                    //file removed
                  });
              });

          });
      });
    }
  }

  if(user){
    let submitData = {
      fname: firstName,
      lname: lastName,
      title: title,
      email: email,
      company_ph_no: phNo
    }
    if(imgName !== ''){
      submitData.profile_image = imgName;
    }

    // update password
    user
      .update(submitData)
      .then((result) => {
        if(result){
          let profileImage = config.adminBaseUrl + 'uploads/brand_owner/' + user.profile_image;
          let companyLogo = config.adminBaseUrl + 'uploads/brand_owner/' + user.profile_image;
          let noImagePath = config.noprofileimage;
          let companyDefaultLogo = config.companyDefaultLogo;
          personalData = {
            title: user.title,
            firstName: user.fname,
            lastName: user.lname,
            email: user.email,
            personalPhNo: user.company_ph_no,
            photo: user.profile_image ? profileImage : noImagePath
          };
          companyData = {
            companyName: user.company_name,
            companyLogo: user.company_logo ? companyLogo : companyDefaultLogo,
            companyPhNo: user.company_ph_no,
            industry: user.industry,
            countryCode: user.country_code,
            countryName: user.countryName,
            companyType: user.company_type,
            //currency: 'Euro'
          }
          return res.status(200).json({
              responseCode: 200,
              responseDetails: "Success! Brand Owner information has been changed.",
              data: { personalData: personalData, companyData: companyData }
          });
        } else{
          return res.json({responseCode: 420, responseDetails: 'Something went wrong!'});
        }
      }).catch(error => {
        // Ooops, do some error-handling
        return res.json({responseCode: 420, responseDetails: 'Something went wrong!', error: error});
      });
  } else{
    return res.status(200).json({
        responseCode: 421,
        responseDetails: "Session Expired.",
    });
  }

});

/**
 * Method to update brand owner company info
 */
router.post('/change-brand-company-info', [auth, upload.single({path: brandOwnerImagePath, fieldName: 'companyLogo'})], passport.authenticate('jwt', { session: false }),  async function(req, res){
  let { companyName, companyType, currency, country, industry } = req.body;

  if(!req.userInfo){
    return res.status(200).json({
        responseCode: "420",
        responseDetails: "Something went wrong!",
    });
  }

  if (!companyName || !companyType || !currency || !country || !industry) {
      return res.status(200).json({
          responseCode: "201",
          responseDetails: "Parameter missing.",
      });
  }

  let imgName = '';
  let id = req.userInfo.id;
  let user = await getBrandOwner({ id: id });

  if(req.fileValidationError) {
      return res.status(200).json({
        responseCode: "202",
        responseDetails: req.fileValidationError,
      });
  } else{
    if(req.file){
      imgName = req.file.filename;
      let oldImgName = user.company_logo;
      fs.unlink(brandOwnerImagePath + oldImgName, (err) => {
        if (err) {
          console.error(err)
          return
        }
      
        //file removed
      });

      fs.readFile(req.file.path, function (err, data) {
          fs.writeFile(brandOwnerImagePath, data, function (err) {
              // write file to uploads/thumbs folder
              im.resize({
                  srcPath: brandOwnerImagePath + imgName,
                  dstPath: brandOwnerImageThumbPath + imgName,
                  width: 200
              }, function (err, stdout, stderr) {
                  //if (err) throw err;
                  //console.log('resized image to fit within 200x200px');
                  fs.unlink(brandOwnerImageThumbPath + oldImgName, (err) => {
                    if (err) {
                      console.error(err)
                      return
                    }
                  
                    //file removed
                  });
              });

          });
      });
    }
  }

  if(user){
    let submitData = {
      company_name: companyName,
      company_type: companyType,
      country_code: country,
      industry: industry,
      currency: currency
    }
    if(imgName !== ''){
      submitData.company_logo = imgName;
    }

    // update password
    user
      .update(submitData)
      .then((result) => {
        if(result){
          let profileImage = config.adminBaseUrl + 'uploads/brand_owner/' + user.profile_image;
          let companyLogo = config.adminBaseUrl + 'uploads/brand_owner/' + user.company_logo;
          let noImagePath = config.noprofileimage;
          let companyDefaultLogo = config.companyDefaultLogo;
          personalData = {
            title: user.title,
            firstName: user.fname,
            lastName: user.lname,
            email: user.email,
            personalPhNo: user.company_ph_no,
            photo: user.profile_image ? profileImage : noImagePath
          };
          companyData = {
            companyName: user.company_name,
            companyLogo: user.company_logo ? companyLogo : companyDefaultLogo,
            companyPhNo: user.company_ph_no,
            industry: user.industry,
            countryCode: user.country_code,
            countryName: user.countryName,
            companyType: user.company_type,
            currency: user.currency,
          }
          return res.status(200).json({
              responseCode: 200,
              responseDetails: "Success! Brand Owner information has been changed.",
              data: { personalData: personalData, companyData: companyData }
          });
        } else{
          return res.json({responseCode: 420, responseDetails: 'Something went wrong!'});
        }
      }).catch(error => {
        // Ooops, do some error-handling
        return res.json({responseCode: 420, responseDetails: 'Something went wrong!', error: error});
      });
  } else{
    return res.status(200).json({
        responseCode: 421,
        responseDetails: "Session Expired.",
    });
  }

});

/**
 * Method to change password for brand owner
 */
router.post('/change-brand-owner-password', auth, passport.authenticate('jwt', { session: false }),  async function(req, res) {
  let { oldPassword, newPassword, confirmPassword } = req.body;
  if(!req.userInfo){
    return res.status(200).json({
        responseCode: "420",
        responseDetails: "Something went wrong!",
    });
  }

  if (!oldPassword || !newPassword || !confirmPassword) {
      return res.status(200).json({
          responseCode: "201",
          responseDetails: "Parameter missing.",
      });
  } else if(newPassword  !== confirmPassword){
      return res.status(200).json({
          responseCode: "201",
          responseDetails: "New password and confirm passwords must match.",
      });
  }

  let id = req.userInfo.id;
  let encryptedPassword = crypto.createHash('md5').update(oldPassword).digest('hex');
  let user = await getBrandOwner({ id: id, password: encryptedPassword });

  if(user){
    let hashPassword = crypto.createHash('md5').update(newPassword).digest('hex');     // Password Hashing

    if(encryptedPassword == hashPassword){
      return res.status(200).json({
          responseCode: 404,
          responseDetails: "New password cannot be same as old password",
      });
    }

    let submitData = {
      password: hashPassword
    }

    // update password
    user
      .update(submitData)
      .then((result) => {
        if(result){
          return res.json({responseCode: 200, responseDetails: 'Success! Brand Owner password has been changed.'});
        } else{
          return res.json({responseCode: 420, responseDetails: 'Something went wrong!'});
        }
      }).catch(error => {
        // Ooops, do some error-handling
        return res.json({responseCode: 420, responseDetails: 'Something went wrong!', error: error});
      });
  } else{
    return res.status(200).json({
        responseCode: 404,
        responseDetails: "Old Password not matched.",
    });
  }

});

/**
 * Method to change password for influencer
 */
router.post('/change-influencer-password', auth, passport.authenticate('jwt', { session: false }),  async function(req, res) {
  
  let { oldPassword, newPassword, confirmPassword } = req.body;
  if(!req.userInfo){
    return res.status(200).json({
        responseCode: "420",
        responseDetails: "Something went wrong!",
    });
  }

  if (!oldPassword || !newPassword || !confirmPassword) {
      return res.status(200).json({
          responseCode: "201",
          responseDetails: "Parameter missing.",
      });
  } else if(newPassword  !== confirmPassword){
      return res.status(200).json({
          responseCode: "201",
          responseDetails: "New password and confirm passwords must match.",
      });
  }

  let id = req.userInfo.id;
  let encryptedPassword = crypto.createHash('md5').update(oldPassword).digest('hex');
  let user = await getInfluencer({ id: id, password: encryptedPassword });

  if(user){
    let hashPassword = crypto.createHash('md5').update(newPassword).digest('hex');     // Password Hashing
  
    if(encryptedPassword == hashPassword){
      return res.status(200).json({
          responseCode: 404,
          responseDetails: "New password cannot be same as old password",
      });
    }

    let submitData = {
      password: hashPassword
    }

    // update password
    user
      .update(submitData)
      .then((result) => {
        if(result){
          return res.json({responseCode: 200, responseDetails: 'Success! Influncer password has been changed.'});
        } else{
          return res.json({responseCode: 420, responseDetails: 'Something went wrong!'});
        }
      }).catch(error => {
        // Ooops, do some error-handling
        return res.json({responseCode: 420, responseDetails: 'Something went wrong!', error: error});
      });
  } else{
    return res.status(200).json({
        responseCode: 404,
        responseDetails: "Old Password not matched.",
    });
  }

});

/**
 * Method to get influencer details by id
 */
router.post('/get-influencer-by-id', auth, passport.authenticate('jwt', { session: false }),  async function(req, res) {
  
  let { id } = req.body;
  
  if(!req.userInfo){
    return res.status(200).json({
        responseCode: "420",
        responseDetails: "Something went wrong!",
    });
  }

  if (!id) {
      id = req.userInfo.id;
      // return res.status(200).json({
      //     responseCode: "201",
      //     responseDetails: "Parameter missing.",
      // });
  }

  let user = await getInfluencer({ id: id });
  let userPlatform = await db.influencerPlatformMap
                .findAll({
                  attributes: ['id', 'platform_id', 'influencer_id', 'followers', 'engagement_ratio'], 
                  where: { influencer_id: id },
                  include :[{
                    attributes: ['name', 'icon', 'class_name'],
                    model:db.platform
                  }]
                })
                .then(r => { return r; })
                .catch(e => { console.log(e) });
  let platforms = await db.platform
            .findAll({
              attributes: ['id', 'name', 'icon', 'class_name'],
              order: [ [ 'name', 'ASC' ]],
              where: {status:'Y'}
            })
            .then(r => { return r; })
            .catch(e => { console.log(e) });
  if(user){
    let profilePicture = config.adminBaseUrl + 'uploads/influencer/' + user.picture;
    let noImagePath = config.noprofileimage;
    personalData = {
      firstName: user.first_name,
      lastName: user.last_name,
      email: user.email,
      phoneNumber: user.contact_number,
      profilePicture: user.picture ? profilePicture : noImagePath,
      industry: user.industry,
      country: user.country,
      nationality: user.nationality,
      description: user.description,
      isAgreeToPerform: user.is_agree_to_perform,
      userPlatform: userPlatform,
      paymentType: user.payout_type,
      payoutMail: user.payout_mail,
    };
    return res.status(200).json({
        responseCode: 200,
        responseDetails: "Success",
        data: { personalData: personalData, platforms: platforms }
    });
  } else{
    return res.status(200).json({
        responseCode: 404,
        responseDetails: "No user found.",
    });
  }
});

/**
 * Method to update influencer personal info
 */
router.post('/change-influencer-personal-info', [auth, upload.single({path: influencerImagePath, fieldName: 'profilePicture'})], passport.authenticate('jwt', { session: false }),  async function(req, res){
  let { name, email, phoneNumber, industry, paymentType, payoutMail} = req.body;

  if(!req.userInfo){
    return res.status(200).json({
        responseCode: "420",
        responseDetails: "Something went wrong!",
    });
  }

  if (!name || !email || !phoneNumber || !industry) {
      return res.status(200).json({
          responseCode: "201",
          responseDetails: "Parameter missing.",
      });
  }

  let imgName = '';
  let nameArray = name.split(" ");
  let firstName = nameArray.length ? nameArray[0] : '';
  let lastName = nameArray.length ? nameArray[1] : '';
  let id = req.userInfo.id;
  let user = await getInfluencer({ id: id });
  if(paymentType=='Manual') 
  {
    payoutMail = '';
  }

  if(req.fileValidationError) {
      return res.status(200).json({
        responseCode: "202",
        responseDetails: req.fileValidationError,
      });
  } else{
    if(req.file){
      imgName = req.file.filename;
      let oldImgName = user.picture;
      fs.unlink(influencerImagePath + oldImgName, (err) => {
        if (err) {
          console.error(err)
          return
        }
      
        //file removed
      });

      fs.readFile(req.file.path, function (err, data) {
          fs.writeFile(influencerImagePath, data, function (err) {
              // write file to uploads/thumbs folder
              im.resize({
                  srcPath: influencerImagePath + imgName,
                  dstPath: influencerImageThumbPath + imgName,
                  width: 200
              }, function (err, stdout, stderr) {
                  //if (err) throw err;
                  //console.log('resized image to fit within 200x200px');
                  fs.unlink(influencerImageThumbPath + oldImgName, (err) => {
                    if (err) {
                      console.error(err)
                      return
                    }
                  
                    //file removed
                  });
              });

          });
      });
    }
  }

  if(user){
    let submitData = {
      first_name: firstName,
      last_name: lastName,
      industry: industry,
      email: email,
      contact_number: phoneNumber,
      payout_type: paymentType,
      payout_mail: payoutMail
    }
    if(imgName !== ''){
      submitData.picture = imgName;
    }
  console.log(submitData);
    // update password
    user
      .update(submitData)
      .then((result) => {
        if(result){
          let profilePicture = config.adminBaseUrl + 'uploads/influencer/' + user.picture;
          let noImagePath = config.noprofileimage;
          personalData = {
            firstName: user.first_name,
            lastName: user.last_name,
            email: user.email,
            phoneNumber: user.contact_number,
            profilePicture: user.picture ? profilePicture : noImagePath,
            industry: user.industry,
            country: user.country,
            nationality: user.nationality,
            description: user.description,
            isAgreeToPerform: user.is_agree_to_perform,
            paymentType: user.payout_type,
            payoutMail: user.payout_mail,
          };
          return res.status(200).json({
              responseCode: 200,
              responseDetails: "Success! Influencer information has been changed.",
              data: { personalData: personalData }
          });
        } else{
          return res.json({responseCode: 420, responseDetails: 'Something went wrong!'});
        }
      }).catch(error => {
        // Ooops, do some error-handling
        return res.json({responseCode: 420, responseDetails: 'Something went wrong!', error: error});
      });
  } else{
    return res.status(200).json({
        responseCode: 421,
        responseDetails: "Session Expired.",
    });
  }

});

/**
 * Method to update influencer details info
 */
router.post('/change-influencer-details-info', auth, passport.authenticate('jwt', { session: false }),  async function(req, res){
  let { country, nationality, description, isAgreeToPerform } = req.body;

  if(!req.userInfo){
    return res.status(200).json({
        responseCode: "420",
        responseDetails: "Something went wrong!",
    });
  }

  if (!country || !nationality || !description) {
      return res.status(200).json({
          responseCode: "201",
          responseDetails: "Parameter missing.",
      });
  }

  let id = req.userInfo.id;
  let user = await getInfluencer({ id: id });

  if(user){
    let submitData = {
      country: country,
      nationality: nationality,
      description: description,
      is_agree_to_perform: isAgreeToPerform ? '1' : '0',
    }

    // update password
    user
      .update(submitData)
      .then((result) => {
        if(result){
          let profilePicture = config.adminBaseUrl + 'uploads/influencer/' + user.picture;
          let noImagePath = config.noprofileimage;
          detailsData = {
            firstName: user.first_name,
            lastName: user.last_name,
            email: user.email,
            phoneNumber: user.contact_number,
            profilePicture: user.picture ? profilePicture : noImagePath,
            industry: user.industry,
            country: user.country,
            nationality: user.nationality,
            description: user.description,
            isAgreeToPerform: user.is_agree_to_perform,
            paymentType: user.payout_type,
            payoutMail: user.payout_mail,
          };
          return res.status(200).json({
              responseCode: 200,
              responseDetails: "Success! Influencer details has been changed.",
              data: { detailsData: detailsData }
          });
        } else{
          return res.json({responseCode: 420, responseDetails: 'Something went wrong!'});
        }
      }).catch(error => {
        // Ooops, do some error-handling
        return res.json({responseCode: 420, responseDetails: 'Something went wrong!', error: error});
      });
  } else{
    return res.status(200).json({
        responseCode: 421,
        responseDetails: "Session Expired.",
    });
  }

});

/**
 * Method to send forgot password mail 
 * for influencer
 */
router.post('/influencer_forgot_password',
  async function (req, res) {      
    var user_email = req.body.email;

    if(!user_email){
      return res.status(200).json({
          responseCode: "201",
          responseDetails: "Parameter missing.",
      });
    }

    let user = await getInfluencer({ email: user_email });

    if(!user){
      return res.status(200).json({
          responseCode: "420",
          responseDetails: "Something went wrong!",
      });
    }

    var token = cryptoRandomString({length: 16, type: 'url-safe'});
    let submitData = {
      forgot_password_token: token,
      forgot_password_requested_at: moment().format('YYYY-MM-DD HH:mm:ss')
    }
    
    // influencer list fetch by Sequalize ORM
    user
      .update(submitData)
      .then((result) => {
        // console.log(user.get({
        //   plain: true
        // }))
        if(result){
          let forgotEmailLink = config.frontEndBaseUrl + '/reset-password?type=influencer&email='+ user_email + '&verication_token='+ token;
          let keyValues = { 
            FORGOT_EMAIL_LINK: forgotEmailLink,
            USER_NAME: result.first_name
          }

          // send sign up mail
          EmailTemplate.sendMailWithTemplate('influencer_forgot_password', user_email, keyValues);

          return res.json({responseCode: 200, responseDetails: 'A mail has been sent to the email address. Check your inbox!'});
        } else{
          return res.json({responseCode: 200, responseDetails: 'A mail has been sent to the email address. Check your inbox!'});
        }
      }).catch(error => {
        // Ooops, do some error-handling
        console.log(error);
        return res.json({responseCode: 420, responseDetails: 'Something went wrong!', error: error});
      });
  }
);

/**
 * Method to send forgot password mail 
 * for brand owner
 */
router.post('/brand_owner_forgot_password',
    async function (req, res) {
      var user_email = req.body.email;

      if(!user_email){
        return res.status(200).json({
            responseCode: "201",
            responseDetails: "Parameter missing.",
        });
      }

      let user = await getBrandOwner({ email: user_email });

      if(!user){
        return res.status(200).json({
            responseCode: "420",
            responseDetails: "Something went wrong!",
        });
      }

      var token = cryptoRandomString({length: 16, type: 'url-safe'});
      let submitData = {
        forgot_password_token: token,
        forgot_password_requested_at: moment().format('YYYY-MM-DD HH:mm:ss') 
      }


      // brand owner list fetch by Sequalize ORM
      user
        .update(submitData)
        .then((result) => {
          // console.log(user.get({
          //   plain: true
          // }))
          if(result){
            var forgotEmailLink = config.frontEndBaseUrl + '/reset-password?type=brand_owner&email='+ user_email + '&verication_token='+ token;

            let keyValues = { 
              FORGOT_EMAIL_LINK: forgotEmailLink,
              USER_NAME: result.fname
            }

            // send sign up mail
            EmailTemplate.sendMailWithTemplate('brand_owner_forgot_password', user_email, keyValues);

            return res.json({responseCode: 200, responseDetails: 'A mail has been sent to the email address. Check your inbox!'});
          } else{
            return res.json({responseCode: 202, responseDetails: 'Email already exist.'});
          }
        }).catch(error => {
          // Ooops, do some error-handling
          console.log(error);
          return res.json({responseCode: 420, responseDetails: 'Something went wrong!', error: error});
        });
    }
);

/**
 * Method to reset password for influencer
 */
router.post('/influencer_reset_password',  async function(req, res) {
  let { newPassword, confirmPassword, email, token } = req.body;

  if (!newPassword || !confirmPassword || !email || !token) {
      return res.status(200).json({
          responseCode: "201",
          responseDetails: "Parameter missing.",
      });
  } else if(newPassword  !== confirmPassword){
      return res.status(200).json({
          responseCode: "201",
          responseDetails: "New password and confirm passwords must match.",
      });
  }

  let user = await getInfluencer({ email: email, forgot_password_token: token });

  if(user){
    let hashPassword = crypto.createHash('md5').update(newPassword).digest('hex');     // Password Hashing
    let submitData = {
      password: hashPassword
    }

    // update password
    user
      .update(submitData)
      .then((result) => {
        if(result){
          return res.json({responseCode: 200, responseDetails: 'Success! Influencer password has been changed.'});
        } else{
          return res.json({responseCode: 420, responseDetails: 'Something went wrong!'});
        }
      }).catch(error => {
        // Ooops, do some error-handling
        return res.json({responseCode: 420, responseDetails: 'Something went wrong!', error: error});
      });
  } else{
    return res.status(200).json({
        responseCode: 404,
        responseDetails: "Old Password not matched.",
    });
  }

});

/**
 * Submit newsletter subscription
 */
router.post('/newsletter_subscription',
    function (req, res) {      
        var email = req.body.email;        
        let submitData = {
          email: req.body.email,          
          status: 'Active',
        }
        //console.log(submitData);
        
        // newsletter list fetch by Sequalize ORM
        newsletterObj
          .findOrCreate({where: {email: submitData.email}, defaults: submitData})
          .then(([user, created]) => {
            // console.log(user.get({
            //   plain: true
            // }))
            if(created){
              
              let keyValues = {               
                EMAIL: email
              }

              // send sign up mail
              EmailTemplate.sendMailWithTemplate('newsletter_subscription', email, keyValues);

              return res.json({responseCode: 200, responseDetails: 'Successfully subscribed. '});
            } else{
              return res.json({responseCode: 202, responseDetails: 'Email already exist.'});
            }
          }).catch(error => {
            // Ooops, do some error-handling
            console.log("Something went worng!");
            console.log(error);
            return res.json({responseCode: 420, responseDetails: 'Something went wrong!', error: error});
          });
    }
);

/**
 * Method to reset password for brand owner
 */
router.post('/brand_owner_reset_password',  async function(req, res) {
  let { newPassword, confirmPassword, email, token } = req.body;

  if (!newPassword || !confirmPassword || !email || !token) {
      return res.status(200).json({
          responseCode: "201",
          responseDetails: "Parameter missing.",
      });
  } else if(newPassword  !== confirmPassword){
      return res.status(200).json({
          responseCode: "201",
          responseDetails: "New password and confirm passwords must match.",
      });
  }

  let user = await getBrandOwner({ email: email, forgot_password_token: token });

  if(user){
    let hashPassword = crypto.createHash('md5').update(newPassword).digest('hex');     // Password Hashing
    let submitData = {
      password: hashPassword
    }

    // update password
    user
      .update(submitData)
      .then((result) => {
        if(result){
          return res.json({responseCode: 200, responseDetails: 'Success! Brand owner password has been changed.'});
        } else{
          return res.json({responseCode: 420, responseDetails: 'Something went wrong!'});
        }
      }).catch(error => {
        // Ooops, do some error-handling
        return res.json({responseCode: 420, responseDetails: 'Something went wrong!', error: error});
      });
  } else{
    return res.status(200).json({
        responseCode: 404,
        responseDetails: "Old Password not matched.",
    });
  }

});

//router.post('/facebookDataCount',  async function(req, res) {
router.post('/facebookDataCount', auth, passport.authenticate('jwt', { session: false }),  async function(req, res, next) {
  let data = {
    social_media_id : req.body.social_media_id,
    social_media_token : req.body.social_media_token,
    influencer_id: req.userInfo.id,
    platform_id: req.body.platform_id,
    total_like: req.body.total_like,
    total_comments: req.body.total_comment,
    followers: req.body.page_likes, 
    engagement_ratio : req.body.engagement,
    total_post: req.body.total_post
  }
  
    db.influencerPlatformMap
      .findOne({where: {social_media_id: data.social_media_id, platform_id: data.platform_id}})
      .then((influencerPlatformData) => {
        if(influencerPlatformData){
            influencerPlatformData.update(data).then((r)=>{
              return res.json({responseCode: 200, responseDetails: 'Success! Social media connected.'});
            }).catch(error => {
                // Ooops, do some error-handling
                return res.json({responseCode: 420, responseDetails: 'Something went wrong5!', error: error});
            });
        } else{
            db.influencerPlatformMap.create(data).then((r)=>{
              return res.json({responseCode: 200, responseDetails: 'Success! Social media connected.'});
            }).catch(error => {
                // Ooops, do some error-handling
                return res.json({responseCode: 420, responseDetails: 'Something went wrong6!', error: error});
            });
        }
    });

    


});

/**
 * facebook graph API data fetch
*/
router.post('/facebook-search', (req, res) => {

  let user_access_token = 'EAAHKrSiotscBANjOQBYQ4tB1X7Frko0vGvJsMKWMZAUwuLBbLuYrqZAQiJWeqHJ6KFqE3RuDdRhA4hvsZCS1qLSEHz33IjbPtqbGTpRADEyRZAri2EU0NLuFHnJYrXbBeXcHEIImcwIM6C0saTqJaj5yptVI89AZBFoJJQBtZC68zDrTfDe60kwKgANRhzQ9euOrbbi5f5V3xU88iS3lYa';
  // you need permission for most of these fields
  const userFieldSet = 'id, name, about, email, accounts, link, is_verified, significant_other, relationship_status, website, picture, photos, feed';
    
  const options = {
    method: 'GET',
    uri: 'https://graph.facebook.com/v2.8/'+req.body.id,
    qs: {
      access_token: user_access_token,
      fields: userFieldSet
    }
  };
  
  request(options,function(response) {
    console.log(response)
    return res.json({responseCode: 200, responseDetails: 'Success! Brand owner password has been changed.', data:response});
  })
})

router.get('/youtube-api',auth, passport.authenticate('jwt', { session: false }),  async function (req, res) {
  var totalLike = 0;
  var totalComment = 0;
  var channelId = 'UCcO_jTi2JuZqwtIegcYAxFw';
  var key = 'AIzaSyBeMKbEb7Ap8n8NLffBnVe0g2CAAJmzlL8';
  var url = 'https://www.googleapis.com/youtube/v3/search?channelId='+channelId+'&order=date&part=snippet&type=video&maxResults=50&key='+key;

  const curl = new (require( 'curl-request' ))();
 
  // curl.setHeaders([
  //   'user-agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36'
  // ])

  curl.setHeaders([
      'user-agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36'
  ]).get(url)
  .then(({statusCode, body, headers}) => {
      //console.log(statusCode, body, headers)
      var data= JSON.parse(body);
      
      //console.log(data);
      var channelUrl = 'https://www.googleapis.com/youtube/v3/channels?id='+channelId+'&order=date&part=statistics&key='+key;
          const curl = new (require( 'curl-request' ))();
          curl.setHeaders([
            'user-agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36'
        ]).get(channelUrl)
          .then(({statusCode, body, headers}) => {
            var channelData= JSON.parse(body);
            var followers = channelData.items[0].statistics.subscriberCount;
            console.log(followers);
                if(data.items.length>0){
                  for(var i=0;i<data.items.length; i++){
                    var videoId = data.items[i].id.videoId;
                    var videoUrl = 'https://www.googleapis.com/youtube/v3/videos?id='+videoId+'&order=date&part=statistics&key='+key;
                    const curl = new (require( 'curl-request' ))();
                    curl.setHeaders([
                      'user-agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36'
                  ]).get(videoUrl)
                    .then(({statusCode, body, headers}) => {
                        //console.log(statusCode, body, headers)
                        var data= JSON.parse(body);              
            
                        //totalLike = parseInt(totalLike) + parseInt(data.items[0].statistics.likeCount);
                        //totalComment = parseInt(totalComment) + parseInt(data.items[0].statistics.commentCount);
                        //console.log('totalLike',totalLike);
                        totalLike = parseInt(data.items[0].statistics.likeCount);
                        totalComment = parseInt(data.items[0].statistics.commentCount);
                      //return;
                      console.log(req.userInfo);
                      let youtubeData = {
                        social_media_id : channelId,
                        social_media_token : key,
                        influencer_id: req.userInfo.id,
                        platform_id: 3,
                        total_like: totalLike,
                        total_comments: totalComment,
                        followers : followers            
                      }
                      db.influencerPlatformMap
                        .findOne({where: {social_media_id: youtubeData.social_media_id, platform_id: youtubeData.platform_id}})
                        .then((influencerPlatformData) => {
                          if(influencerPlatformData){
                            sequelize.query("UPDATE influencer_platform_map SET total_like = total_like + "+youtubeData.total_like+",total_comments = total_comments+"+youtubeData.total_comments+" WHERE social_media_id = '"+youtubeData.social_media_id+"' AND platform_id = '"+youtubeData.platform_id+"'").then((r)=>{
                                //console.log(youtubeData);
                              }).catch(error => {
                                  // Ooops, do some error-handling
                                  return res.json({responseCode: 420, responseDetails: 'Something went wrong1!', error: error});
                              });
                          } else{
                              db.influencerPlatformMap.create(youtubeData).then((r)=>{
                                //console.log(data);
                              }).catch(error => {
                                  // Ooops, do some error-handling
                                  return res.json({responseCode: 420, responseDetails: 'Something went wrong2!', error: error});
                              });
                          }
                      });

                    })
                    .catch((e) => {
                        console.log(e);
                    });

                  }
                }
    })
    .catch((e) => {
        console.log(e);
    });
     return;
  })
  .catch((e) => {
      console.log(e);
  });

})

/**
 * Method to get all Influencers
 */
const getAllInfluencers = async () => {
  return await influencerObj.findAll();
};

/**
 * Method to get Influencer by given condition
 * @param {*} obj 
 */
const getInfluencer = async obj => {
  return await influencerObj.findOne({
    where: obj,
  });
};

/**
 * Method to get Brand Owner by given condition
 * @param {*} obj 
 */
const getBrandOwner = async obj => {
  return await brandOwnerObj.findOne({
    where: obj
  });
};

/**
 * Method to get currency details by currency id
 * @param {*} obj 
 */
const getCurrency = async obj => {
  return await currencyObj.findOne({
    where: obj,
  });
};

module.exports = router;
