var express = require('express');
var router = express.Router();
var moment = require('moment');
var auth = require("../middleware/auth");

/** Create Sequalize object  */
Sequelize = require("sequelize");
Op = Sequelize.Op;

db = require("./../models/associations");

const config = require("../config/default.json");
const campaignImagePath = config.adminBaseUrl + "uploads/campaign/";
const uploadedCampaignImagePath = config.adminServereUrl + "uploads/campaign/";
/**
 * Method to get all invitation 
 * to influencer from brand owner
 */
router.post('/get-all-invitation', auth, passport.authenticate('jwt', { session: false }),  async function(req, res) {

  let { id } = req.body;
  if(!req.userInfo){
    return res.status(200).json({
        responseCode: 420,
        responseDetails: "Something went wrong!",
    });
  }

  if (!id) {
      id = req.userInfo.id;
      // return res.status(200).json({
      //     responseCode: "201",
      //     responseDetails: "Parameter missing.",
      // });
  }

  //console.log(moment().format());
  
  db.campaignAssign.findAll({
    where: { influencer_id: id , status: 'PENDING', expiry_date: {[Op.lte]: moment().format('YYYY-MM-DD')}},
    include:[{ 
        model : db.campaign,
        require:true,
        where:{
          status:['ANNOUNCED', 'IN_PROGRESS'],
          /*id:65,
          invitation_expiery_date: {
            [Op.lte]: moment().format('YYYY-MM-DD')
          }*/
        },
        include:[{ 
          model : db.brandowner
        },{ 
          model : db.proposal
        }]
    }]
  })
  .then((pendingInvitations)=>{
      return db.campaignAssign.findAll({
        where: { 
          influencer_id: id , 
          [Op.or]: [{status: 'ACCEPT'}, {status: 'SUBMITTED'}, {status: 'CONFIRM'}]
        },
        include:[{ 
            model : db.campaign,
            include:[{ 
              model : db.brandowner
            },{ 
              model : db.proposal
            }]
        }]
      })
      .then((acceptInvitations)=>{
          return db.campaignAssign.findAll({
            where: { influencer_id: id , status: 'REJECT' },
            include:[{ 
                model : db.campaign,
                include:[{ 
                  model : db.brandowner
                },{ 
                  model : db.proposal
                }]
            }]
          })
          .then((rejectInvitations)=>{
              let invitations = {
                pendingInvitations: pendingInvitations,
                acceptInvitations: acceptInvitations,
                rejectInvitations: rejectInvitations
              }
              return res.status(200).json({ responseCode: 200, responseDetails: 'Get campaign data.', Invitations: invitations});
          })
          .catch(reason => {
            return res.status(420).json({responseCode: 420, responseDetails: 'Something went wrong!', error: reason});
          });
      })
      .catch(reason => {
        return res.status(420).json({responseCode: 420, responseDetails: 'Something went wrong!', error: reason});
      });
  })
  .catch(reason => {
    return res.status(420).json({responseCode: 420, responseDetails: 'Something went wrong!', error: reason});
  });
});

/**
 * Method to get invitation details by id
 */
router.post('/get-invitation-by-id', auth, passport.authenticate('jwt', { session: false }),  async function(req, res) {

  let { id, invitationId } = req.body;
  
  if(!req.userInfo){
    return res.status(200).json({
        responseCode: 420,
        responseDetails: "Something went wrong!",
    });
  }

  if (!invitationId) {
      return res.status(200).json({
          responseCode: "201",
          responseDetails: "Parameter missing.",
      });
  }

  if (!id) {
      id = req.userInfo.id;
      // return res.status(200).json({
      //     responseCode: "201",
      //     responseDetails: "Parameter missing.",
      // });
  }

  db.campaignAssign.findOne({
    where: { influencer_id: id , id: invitationId },
    include:[{ 
        model : db.campaign,
        include:[{ model : db.campaignFiles},{  model : db.brandowner}, {model: db.CampaignPlatformMap, include: [{model : db.platform,  attributes:['name', 'class_name']}]
        
      }]
    }]
  })
  .then((invitation)=>{
      return res.status(200).json({ responseCode: 200, responseDetails: 'Get campaign data.', Invitation: invitation, campaignImagePath : uploadedCampaignImagePath});
  })
  .catch(reason => {
    return res.status(420).json({responseCode: 420, responseDetails: 'Something went wrong!', error: reason});
  });
});

/**
 * Method to update invitation by id
 */
router.post('/update-invitation-by-id', auth, passport.authenticate('jwt', { session: false }),  async function(req, res) {

  let { id, invitationId, status } = req.body;
  
  if(!req.userInfo){
    return res.status(200).json({
        responseCode: 420,
        responseDetails: "Something went wrong!",
    });
  }

  if (!invitationId || !status) {
      return res.status(200).json({
          responseCode: "201",
          responseDetails: "Parameter missing.",
      });
  }

  if (!id) {
      id = req.userInfo.id;
      // return res.status(200).json({
      //     responseCode: "201",
      //     responseDetails: "Parameter missing.",
      // });
  }

  let invitation = await getInvitation({ influencer_id: id , id: invitationId });

  if(invitation){
    let notificationTemplateAlias = '';
    var notificationAlias = '';
    let campaignHistoryAction = '';
    let invitationObj = {};
    switch(status){
      case 'ACCEPT':
          notificationTemplateAlias = 'invitation_accept'; 
          notificationAlias = 'Invitation Accepted'; 
          campaignHistoryAction = 'Invitation Accepted';
          invitationObj.status = status; 
          invitationObj.accept_date_time = moment().format('YYYY-MM-DD HH:mm:ss');
          db.campaign.update({'status': 'IN_PROGRESS'}, {where:{id:invitation.campaign_id, 'status':'ANNOUNCED'}});
          break;
      case 'REJECT':
          notificationTemplateAlias = 'invitation_reject';  
          notificationAlias = 'Invitation Rejected';  
          campaignHistoryAction = '';
          invitationObj.status = status;
          invitationObj.reject_date_time = moment().format('YYYY-MM-DD HH:mm:ss');
          break;
    }
    // update invitation
    invitation
      .update(invitationObj)
      .then((result) => {
        if(result){

          let notificationTemplate = getNotificationTemplate({ alias: notificationTemplateAlias });   // get notification template

          notificationTemplate.then((temp) => {
              // create campaign history
              let campaignHistoryData = {
                campaign_id: invitation.campaign_id,
                influencer_id: id,
                brand_owner_id: invitation.Campaign.brand_owner_id,
                action: notificationAlias,
                taken_by: 'Influencer'
              }
              
              db.campaignHistory.create(campaignHistoryData).catch(e => { console.log(e); });

              // create notification
              let notificationData = {
                title: temp.title,
                content: temp.content,
                sender_id: id,
                receiver_id: invitation.Campaign.BrandOwner.id,
                receiver_type: 'BrandOwner'
              }

              db.notification
              .create(notificationData)
              .then((result) =>{
                console.log('Notification created.');
              }).catch(reason => {
                console.log('Notification not created.', reason);
              });

          }).catch(reason => {
            console.log('Notification template issue.', reason);
          });

          
          db.campaignAssign.findAll({
            where: { influencer_id: id , status: 'PENDING' },
            include:[{ 
                model : db.campaign,
                where:{status:['ANNOUNCED', 'IN_PROGRESS']},
                require:true,
                include:[{ 
                  model : db.brandowner
                },{ 
                  model : db.proposal
                }]
            }]
          })
          .then((pendingInvitations)=>{
              return db.campaignAssign.findAll({
                where: { 
                  influencer_id: id , 
                  [Op.or]: [{status: 'ACCEPT'}, {status: 'SUBMITTED'}, {status: 'CONFIRM'}]
                },
                include:[{ 
                    model : db.campaign,
                    include:[{ 
                      model : db.brandowner
                    },{ 
                      model : db.proposal
                    }]
                }]
              })
              .then((acceptInvitations)=>{
                  return db.campaignAssign.findAll({
                    where: { influencer_id: id , status: 'REJECT' },
                    include:[{ 
                        model : db.campaign,
                        include:[{ 
                          model : db.brandowner
                        },{ 
                          model : db.proposal
                        }]
                    }]
                  })
                  .then((rejectInvitations)=>{
                      let invitations = {
                        pendingInvitations: pendingInvitations,
                        acceptInvitations: acceptInvitations,
                        rejectInvitations: rejectInvitations
                      }
                      return res.status(200).json({ responseCode: 200, responseDetails: 'Get campaign data.', Invitations: invitations});
                  })
                  .catch(reason => {
                    return res.status(420).json({responseCode: 420, responseDetails: 'Something went wrong!', error: reason});
                  });
              })
              .catch(reason => {
                return res.status(420).json({responseCode: 420, responseDetails: 'Something went wrong!', error: reason});
              });
            });
        } else{
          return res.json({responseCode: 420, responseDetails: 'Something went wrong!'});
        }
      }).catch(error => {
        // Ooops, do some error-handling
        return res.json({responseCode: 420, responseDetails: 'Something went wrong!', error: error});
      });
  } else{
      return res.status(200).json({
          responseCode: 421,
          responseDetails: "Session Expired.",
      });
  }
});

/**
 * Method to get invitation by given condition
 * @param {*} obj 
 */
const getInvitation = async obj => {
  return await db.campaignAssign.findOne({
    where: obj,
    include:[{ 
        model : db.campaign,
        include:[{ 
          model : db.brandowner
        }]
    }]
  });
};

/**
 * Method to get notification template by given condition
 * @param {*} obj 
 */
const getNotificationTemplate = async obj => {
  return await db.notificationTemplate.findOne({
    where: obj,
  });
};

module.exports = router;