var express = require('express');
var router = express.Router();

/** Create Sequalize object  */
Sequelize = require("sequelize");
Op = Sequelize.Op;

db = require("./../models/associations");
const config = require("../config/default.json");
const auth = require("../middleware/auth");

/**
 * Get Brand List
 */
router.post('/list', auth, passport.authenticate('jwt', { session: false }),  async function(req, res, next) {

  return db.brandowner.findAll({
      attributes: ['id', 'company_name'],
      order: [ [ 'id', 'DESC' ]],
     
    })
    .then(data => { 
      return res.status(200).json({ responseCode: 200, responseDetails: 'Get brandowner data.', data: data});
    })
    .catch(reason => {
      return res.status(200).json({ responseCode: 420, responseDetails: 'Something went wrong!.', "error": reason});
    });
});

module.exports = router;