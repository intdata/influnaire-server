var express = require('express');
var router = express.Router();

//**=============== Sequalize init =============== */
Sequelize = require("sequelize");
Op = Sequelize.Op;
Countries = require("./../models/countries");
Industries = require("./../models/industries");
Currencies = require("./../models/currencies");
Page = require("./../models/page")(sequelize, Sequelize);
db = require("./../models/associations");

const config = require("../config/default.json");
BrandOwner = require("./../models/brandowner");

var countriesObj = Countries(sequelize, Sequelize);
var industriesObj = Industries(sequelize, Sequelize);
var currenciesObj = Currencies(sequelize, Sequelize);
//**=============== Sequalize init =============== */

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express powered by Plesk' });
});

/**
 * Method to get all active country list
 */
router.get('/get-countries', function(req, res, next) {
  return countriesObj.findAll({
    where: { status: 1 },order: [
            ['countryname', 'ASC']
        ],
  })
  .then(countries => { 
    return res.status(200).json({ responseCode: 200, responseDetails: 'All active country list.', countries: countries});
  })
  .catch(reason => {
    return res.status(200).json({ responseCode: 420, responseDetails: 'Something went wrong!.', "error": reason});
  });
});

/**
 * Method to get all active industry list
 */
router.get('/get-industries', function(req, res, next) {
  return industriesObj.findAll()
  .then(industries => { 
    return res.status(200).json({ responseCode: 200, responseDetails: 'All active industry list.', industries: industries});
  })
  .catch(reason => {
    console.log(reason);
    
    return res.status(200).json({ responseCode: 420, responseDetails: 'Something went wrong!.', "error": reason});
  });
});

/**
 * Method to get all currency list
 */
router.get('/get-currencies', function(req, res, next) {
  return currenciesObj.findAll({
    order: [
            ['name', 'ASC']
        ],
  })
  .then(currencies => { 
    return res.status(200).json({ responseCode: 200, responseDetails: 'All active currency list.', currencies: currencies});
  })
  .catch(reason => {
    return res.status(200).json({ responseCode: 420, responseDetails: 'Something went wrong!.', "error": reason});
  });
});

/*------------------------------------------ Testing ----------------------------------------- */

db = require("./../models/associations");

router.get('/get-brandowner', function(req, res, next) {
  return db.brandowner.findAll({
    include: [{
      model: db.industries,
      as: 'industries'
    }]
  })
  .then(brandowners => { 
    return res.status(200).json({ responseCode: 200, responseDetails: 'All active industry list.', brand_owners: brandowners});
  })
  .catch(reason => {
    console.log(reason);
    
    return res.status(200).json({ responseCode: 420, responseDetails: 'Something went wrong!.', "error": reason});
  });
});

/**
 * Method to get page data
*/
router.get('/get_page_data',function(req, res, next) {
  console.log(req.query.slug)
  return db.page.findOne({
    where: { slug: req.query.slug },
    include:[{ model : db.cmsLang, where:{lang_code : 'en'}}]
  })
  .then(page => { 
    return res.status(200).json({ responseCode: 200, responseDetails: 'Get page data.', page: page});
  })
  .catch(reason => {
    return res.status(200).json({ responseCode: 420, responseDetails: 'Something went wrong!.', "error": reason});
  });
})

/** Get Latest testimoneals */
router.get('/get_last_testimoneal', function(req, res, next){
  return db.testimonial.findOne({
    order: [ [ 'id', 'DESC' ]]
  })
  .then(data => { 
    imagepath = config.adminBaseUrl + 'uploads/testimonial/thumb/';
    return res.status(200).json({ responseCode: 200, responseDetails: 'Get testimonial data.', data: data, imagepath:imagepath});
  })
  .catch(reason => {
    return res.status(200).json({ responseCode: 420, responseDetails: 'Something went wrong!.', "error": reason});
  });
})

/** Get All testimoneals */
router.get('/get_all_testimoneal', function(req, res, next){
  
  return db.testimonial.findAll({
    order: [ [ 'id', 'DESC' ]]
  })
  .then(data => { 
    imagepath = config.adminBaseUrl + 'uploads/testimonial/thumb/';
    return res.status(200).json({ responseCode: 200, responseDetails: 'Get testimonial data.', data: data, imagepath:imagepath});
  })
  .catch(reason => {
    return res.status(200).json({ responseCode: 420, responseDetails: 'Something went wrong!.', "error": reason});
  });
})

/** Get All testimoneals */
router.get('/timer-end', function(req, res, next){
  
  return res.status(200).json({ responseCode: 200, responseDetails: 'Get Timer End.', data: config.timerEnd});
})

module.exports = router;
