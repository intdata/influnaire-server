var express = require('express');
var router = express.Router();
var moment = require('moment');

const auth = require("../middleware/auth");

db = require("./../models/associations");

/**
 * Method to submit proposal
 */
router.post('/submit-proposal', auth, function(req, res, next) {
  let { campaign_id, invitation_id, subject, brief, description, price, required_resources, platforms, delivary_date } = req.body;
  
  if(!req.userInfo){
    return res.status(200).json({
        responseCode: "420",
        responseDetails: "Something went wrong!",
    });
  }
  //console.log(delivary_date);return
  

  influencer_id = req.userInfo.id;
  
  if (!campaign_id || !invitation_id || !subject || !brief || !description || !delivary_date || !price || !required_resources || !platforms) {
      return res.status(200).json({
          responseCode: "201",
          responseDetails: "Parameter missing.",
      });
  }

  let submitData = {
    campaign_id: campaign_id,
    invitation_id: invitation_id,
    influencer_id: influencer_id,
    subject: subject,
    brief: brief,
    description: description,
    delivery_date: moment(delivary_date).format('YYYY-MM-DD HH:mm:ss'),
    //delivery_date: '2019-12-20',
    currency: 'Euro',
    price: price,
    required_resources: required_resources
  }
  //console.log(submitData); return;
  // insert contact us data in db
  db.proposal
    .create(submitData)
    .then((created) => {
      // console.log(user.get({
      //   plain: true
      // }))
      if(created){

        // update with proposal id
        let platformArray = platforms.map((v, i) => v ? { proposal_id: created.id, platform_id: v.platform_id } : null)
          .filter(v => v !== null);

        db.proposalPlatformMap
          .bulkCreate(platformArray)
          .then((result) => {

            let invitationInfo = getInvitation({ id: invitation_id });   // get invitation
            invitationInfo.then((invitationInfoResult) => {
              invitationInfoResult
                .update({status: 'SUBMITTED'})
                .then((result) => {
                    let notificationTemplateAlias = 'proposal_submitted';
                    let notificationTemplate = getNotificationTemplate({ alias: notificationTemplateAlias });   // get notification template
            
                    notificationTemplate.then((temp) => {
                      console.log(temp)
                        // create notification
                        let notificationData = {
                          title: temp.title,
                          content: temp.content,
                          sender_id: influencer_id,
                          receiver_type: 'BrandOwner'
                        }
                        let campaignInfo = getCampaign({ id: campaign_id });   // get campaign

                        campaignInfo.then((info) => {
                          notificationData.receiver_id = info.brand_owner_id;
                          // create campaign history
                          let campaignHistoryData = {
                            campaign_id: campaign_id,
                            influencer_id: influencer_id,
                            brand_owner_id: info.brand_owner_id,
                            action: 'Proposal Sent',
                            taken_by: 'Influencer'
                          }
                          db.campaignHistory.create(campaignHistoryData);

                          db.notification
                            .create(notificationData)
                            .then((result) =>{
                              console.log('Notification created.');
                              return res.json({responseCode: 200, responseDetails: 'Success! Proposal has been submitted.'});
                            }).catch(reason => {
                              console.log('Notification not created.', reason);
                            });
                            
                        }).catch(reason => {
                          console.log('Campaign not found.', reason);
                        });
                    }).catch(reason => {
                      console.log('Notification template issue.', reason);
                    });
                }).catch(reason => {
                  console.log('Invitation status update issue.', reason);
                });
              
            }).catch(reason => {
              console.log('Proposal platform map issue.', reason);
            });
          }).catch(reason => {
            console.log('Proposal platform map issue.', reason);
          });
      }
    }).catch(error => {
      // Ooops, do some error-handling
      console.log("Something went worng!");
      console.log(error);
      return res.json({responseCode: 420, responseDetails: 'Something went wrong!', error: error});
    });
});

/**
 * Proposal accept by brand owner
 */
router.post('/acceptByBrandowner', [auth], passport.authenticate('jwt', { session: false }),  async function(req, res, next) {
  
  if(!req.userInfo){
    return res.status(200).json({
        responseCode: "420",
        responseDetails: "Something went wrong!",
    });
  }
  
  var campaign_assign = await db.campaignAssign.findOne({
     where:{'id':req.body.invitation_id}
  });
  
  //Create object for history table update
  var history_update={}
  history_update.campaign_id = campaign_assign.campaign_id
  history_update.influencer_id = campaign_assign.influencer_id
  history_update.brand_owner_id = req.userInfo.id
  history_update.action = (req.body.type == 'REFUSE')?'Proposal Rejected':'Proposal Accepted';
  history_update.taken_by = 'BrandOwner'
  history_update.created_at = sequelize.fn('NOW')
  
  //Notification Update data
  let notificationTemplateAlias = (req.body.type == 'REFUSE')?'proposal_rejected':'proposal_accepted';
  let notificationTemplate = await getNotificationTemplate({ alias: notificationTemplateAlias });
  //console.log(notificationTemplate)
  var notification_update = {}
  notification_update.title	=  (notificationTemplate && notificationTemplate.length  && notificationTemplate.title)?notificationTemplate.title:''
  notification_update.content	=  (notificationTemplate && notificationTemplate.length  && notificationTemplate.content)?notificationTemplate.content:''
  notification_update.sender_id = req.userInfo.id
  notification_update.receiver_id = campaign_assign.influencer_id
  notification_update.campaign_id = campaign_assign.campaign_id
  notification_update.receiver_type = 'Influencer'
  notification_update.created_at = sequelize.fn('NOW')

  db.campaignAssign.update({status:req.body.type}, {where:{'id':req.body.invitation_id}})
  .then(retData => { 
    db.campaignHistory.create(history_update);
    db.notification.create(notification_update);
    return res.status(200).json({ responseCode: 200, responseDetails: '', data: retData});
  })
  .catch(reason => {
    return res.status(200).json({ responseCode: 420, responseDetails: 'Something went wrong!.', "error": reason});
  });
})
/**
 * Method to get notification template by given condition
 * @param {*} obj 
 */
const getNotificationTemplate = async obj => {
  return await db.notificationTemplate.findOne({
    where: obj,
  });
};

/**
 * Method to get campaign data by given condition
 * @param {*} obj 
 */
const getCampaign = async obj => {
  return await db.campaign.findOne({
    where: obj,
  });
};

/**
 * Method to get invitaion data by given condition
 * @param {*} obj 
 */
const getInvitation = async obj => {
  return await db.campaignAssign.findOne({
    where: obj,
  });
};


module.exports = router;
