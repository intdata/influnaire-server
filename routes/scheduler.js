var express = require('express');
var router = express.Router();
var moment = require('moment');
var https = require('https');
var fs = require('fs');
var readline = require('readline');
var {google} = require('googleapis');
var OAuth2 = google.auth.OAuth2;

// If modifying these scopes, delete your previously saved credentials
// at ~/.credentials/youtube-nodejs-quickstart.json
var SCOPES = ['https://www.googleapis.com/auth/youtube.readonly'];
var TOKEN_DIR = (process.env.HOME || process.env.HOMEPATH ||
    process.env.USERPROFILE) + '/.credentials/';
var TOKEN_PATH = TOKEN_DIR + 'youtube-nodejs-quickstart.json';
var googleKeys = './config/google_keys.json';

//**=============== Sequalize init =============== */
Sequelize = require("sequelize");
const Op = Sequelize.Op
Influencer = require("./../models/influencer");
BrandOwner = require("./../models/brandowner");

var influencerObj = Influencer(sequelize, Sequelize);
var brandOwnerObj = BrandOwner(sequelize, Sequelize);
//**=============== Sequalize init =============== */

/**
 * To verify Influencer email
 */
router.get('/influencer_email_verification_token_remove', async function(req, res, next) {
 
  return influencerObj.findAll({
    where: { 
      created_at: {
        $gte: moment().subtract(15, 'minutes').toDate()
      },
      $or: [
        {
            verication_token: { $not: ''},
            is_email_verified: { $eq: 0 }
        }
      ]
    }
  })
  .then(user => { 
    if (user) {
      user
      .update({ verication_token: ''})
      .then((result) => {
        if(result){
          res.json({responseCode: 200, responseDetails: 'Success! Email verification tiken has removed.'});
        } else{
          res.json({responseCode: 420, responseDetails: 'Something went wrong!'});
        }
      }).catch(error => {
        // Ooops, do some error-handling
        res.json({responseCode: 420, responseDetails: 'Something went wrong!', error: error});
      });
    } else {
      
    }
  })
  .catch(reason => {
    return res.status(404).json({"error": reason});
  });
});

/**
 * To verify Brand Owner email
 */
router.get('/brand_owner_email_verification_token_remove', async function(req, res, next) {

  return brandOwnerObj.findAll({
    where: { 
      verication_token: { [Op.not]: ''},
      created_at: {
        [Op.gte]: moment().subtract(15, 'minutes').toDate()
      },
      verication_token: { [Op.not]: ''}
    }
  })
  .then(user => { 
    if (user) {
      user
      .update({ verication_token: ''})
      .then((result) => {
        if(result){
          res.json({responseCode: 200, responseDetails: 'Success! Email verification tiken has removed.'});
        } else{
          res.json({responseCode: 420, responseDetails: 'Something went wrong!'});
        }
      }).catch(error => {
        // Ooops, do some error-handling
        res.json({responseCode: 420, responseDetails: 'Something went wrong!', error: error});
      });
    } else {
      
    }
  })
  .catch(reason => {
    return res.status(404).json({"error": reason});
  });
});


router.get('/influencer_youtube_data_fetch', async function(req, res, next) {
  // Load client secrets from a local file.
  fs.readFile(googleKeys, function processClientSecrets(err, content) {
    if (err) {
      console.log('Error loading client secret file: ' + err);
      return;
    }

    // Authorize a client with the loaded credentials, then call the YouTube API.
    authorize(JSON.parse(content), getChannel);
  });


});

/**
 * Create an OAuth2 client with the given credentials, and then execute the
 * given callback function.
 *
 * @param {Object} credentials The authorization client credentials.
 * @param {function} callback The callback to call with the authorized client.
 */
function authorize(credentials, callback) {
  var clientSecret = credentials.web.client_secret;
  var clientId = credentials.web.client_id;
  var redirectUrl = credentials.web.redirect_uris[0];
  var oauth2Client = new OAuth2(clientId, clientSecret, redirectUrl);

  //callback(oauth2Client);
  // Check if we have previously stored a token.
  fs.readFile(TOKEN_PATH, function(err, token) {
    if (err) {
      getNewToken(oauth2Client, callback);
    } else {
      oauth2Client.credentials = JSON.parse(token);
      callback(oauth2Client);
    }
  });
}

/**
 * Get and store new token after prompting for user authorization, and then
 * execute the given callback with the authorized OAuth2 client.
 *
 * @param {google.auth.OAuth2} oauth2Client The OAuth2 client to get token for.
 * @param {getEventsCallback} callback The callback to call with the authorized
 *     client.
 */
function getNewToken(oauth2Client, callback) {
  var authUrl = oauth2Client.generateAuthUrl({
    //access_type: 'offline',
    approval_prompt: 'force',
    scope: SCOPES
  });
  console.log('Authorize this app by visiting this url: ', authUrl);

  var rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
  });

  rl.question('Enter the code from that page here: ', function(code) {
    rl.close();
    oauth2Client.getToken(code, function(err, token) {
      if (err) {
        console.log('Error while trying to retrieve access token', err);
        return;
      }
      oauth2Client.credentials = token;
      storeToken(token);
      callback(oauth2Client);
    });
  });
}

/**
 * Store token to disk be used in later program executions.
 *
 * @param {Object} token The token to store to disk.
 */
function storeToken(token) {
  try {
    fs.mkdirSync(TOKEN_DIR);
  } catch (err) {
    if (err.code != 'EEXIST') {
      throw err;
    }
  }
  fs.writeFile(TOKEN_PATH, JSON.stringify(token), (err) => {
    if (err) throw err;
    console.log('Token stored to ' + TOKEN_PATH);
  });
}

/**
 * Lists the names and IDs of up to 10 files.
 *
 * @param {google.auth.OAuth2} auth An authorized OAuth2 client.
 */
function getChannel(auth) {
  var service = google.youtube('v3');
  service.channels.list({
    auth: auth,
    part: 'snippet,contentDetails,statistics',
    mine: 'true'
  }, function(err, response) {
    if (err) {
      console.log('The API returned an error: ' + err);
      return;
    }
    var channels = response.data.items;
    if (channels.length == 0) {
      console.log('No channel found.');
    } else {
      console.log('This channel\'s ID is %s. Its title is \'%s\', and ' +
                  'it has %s views.',
                  channels[0].id,
                  channels[0].snippet.title,
                  channels[0].statistics.viewCount,
                  channels[0].statistics.commentCount,
                  channels[0].statistics.subscriberCount,
                  channels[0].statistics.videoCount,);
    }
  });
}


module.exports = router;
