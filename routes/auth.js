var express = require('express');
var router = express.Router();

const auth = require("../middleware/auth");

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('Unauthorized!');
});

// =============================================================================
// AUTHENTICATE (FIRST LOGIN) ==================================================
// =============================================================================

// facebook -------------------------------

// send to facebook to do the authentication
router.get('/facebook', passport.authenticate('facebook', { scope : ['public_profile', 'email','last_name'] }));

// handle the callback after facebook has authenticated the user
// router.get('/facebook/callback',
//     passport.authenticate('facebook', {
//         successRedirect : '/profile',
//         failureRedirect : '/',
//         session: false
//     }));
// router.get('/facebook/callback',
//     passport.authenticate('facebook', { failureRedirect: '/login', session: false }),
//     //passport.authenticate('facebook', {session: false}),
//     // function(req, res) {
//     //     console.log('FB_response_success');
        
//     //     // Successful authentication, redirect home.
//     //     //res.redirect('/login');
//     // },
//     function(req, res) {
//         console.log('FB_response_failure');
        
//         // Successful authentication, redirect home.
//         //res.redirect('/');
//     });
router.get('/facebook/callback',
    passport.authenticate('facebook', { session: false }),
    function(req, res) {
        console.log(req.user);
        res.send(req.user? 200 : 401);
        //res.json({ token: req.jwtToken });
    });

// twitter --------------------------------

// send to twitter to do the authentication
router.get('/twitter', passport.authenticate('twitter', { scope : 'email' }));

// handle the callback after twitter has authenticated the user
router.get('/twitter/callback',
    passport.authenticate('twitter', {
        successRedirect : '/profile',
        failureRedirect : '/'
    }));


// google ---------------------------------

// send to google to do the authentication
router.get('/google', auth, passport.authenticate(['jwt', 'google'], { scope : ['profile', 'email'], state: 11 }));

// router.get('/google', auth, passport.authenticate('jwt', { session: false }), async function(req, res, next) {
//     let userId = req.userInfo.id;
//     console.log('a', userId);
    
//     passport.authenticate('google', { scope : ['profile', 'email'], state: userId });
// });

// the callback after google has authenticated the user
router.get('/google/callback',
    passport.authenticate('google', {
        failureRedirect : '/api/v1/auth/google/failure',
        session: false
    }),
    function(req, res) {
        return res.status(200).json({
            responseCode: "200",
            responseDetails: "Success",
            details: req.user
        });
    }
);

router.get('/google/success', function(req, res, next){
    return res.status(200).json({
        responseCode: "200",
        responseDetails: req,
    });
});

router.get('/google/failure', function(req, res, next){
    return res.status(200).json({
        responseCode: "420",
        responseDetails: "Something went wrong!",
    });
});

// =============================================================================
// AUTHORIZE (ALREADY LOGGED IN / CONNECTING OTHER SOCIAL ACCOUNT) =============
// =============================================================================

// locally --------------------------------
router.get('/connect/local', function(req, res) {
    res.render('connect-local.ejs', { message: req.flash('loginMessage') });
});

router.post('/connect/local', passport.authenticate('local-login', { session: false, failWithError: false }),
  function(req, res, next) {
      //console.log(req, res);
      
    // handle success
    if (req.xhr) { return res.json({ id: req.user.id }); }
    //return res.redirect('/');
  },
  function(err, req, res, next) {
    // handle error
    if (req.xhr) { return res.json(err); }
    //return res.redirect('/login');
  }
);
// facebook -------------------------------

// send to facebook to do the authentication
router.get('/connect/facebook', passport.authorize('facebook', { scope : ['public_profile', 'email','last_name'] }));

// handle the callback after facebook has authorized the user
router.get('/connect/facebook/callback',
    passport.authorize('facebook', {
        successRedirect : '/profile',
        failureRedirect : '/'
    }));

// twitter --------------------------------

// send to twitter to do the authentication
router.get('/connect/twitter', passport.authorize('twitter', { scope : 'email' }));

// handle the callback after twitter has authorized the user
router.get('/connect/twitter/callback',
    passport.authorize('twitter', {
        successRedirect : '/profile',
        failureRedirect : '/'
    }));



// route middleware to ensure user is logged in
function isLoggedIn(req, res, next) {
  if (req.isAuthenticated())
      return next();

  res.json({responseCode: 440, responseDetails: 'User not logged in!'});
}

module.exports = router;
