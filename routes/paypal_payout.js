var express = require('express');
var router = express.Router();

/** Create Sequalize object  */
Sequelize = require("sequelize");
Op = Sequelize.Op;
const auth = require("../middleware/auth");
db = require("./../models/associations");
const config = require("../config/default.json");
sharedService = require("./sharedService")
const campaignImagePath = config.adminAssetpath + "uploads/campaign/";
var im = require('imagemagick');
var fs = require('fs');
const upload = require("../middleware/upload");
var qs = require('querystring');
var paypal = require('paypal-rest-sdk');
let beautify = require('js-beautify').js_beautify;

paypal.configure({
  'mode': config.mode, //sandbox or live
  'client_id': config.client_id,
  'client_secret': config.client_secret,
  'headers': {
    'custom': 'header'
  }
});

router.get('/get_token', function (req, res, next) {
  request.post({
    uri: config.uri,
    headers: {
      "Accept": "application/json",
      "Accept-Language": "en_US",
      "content-type": "application/x-www-form-urlencoded"
    },
    auth: {
      'user': config.client_id,
      'pass': config.client_secret,
      // 'sendImmediately': false
    },
    form: {
      "grant_type": "client_credentials"
    }
  }, function (error, response, body) {
    //let result = beautify(JSON.stringify(JSON.parse(body)), { indent_size: 2 });
    res.status(200).json(JSON.parse(body));
    //console.log(body);
  });
});

router.post('/payout', function (req, res, next) {

  let id = req.body.id;
  let influencerPaypalCharge = req.body.influencerPaypalCharge;
  let influencerDeductionPercentage = req.body.influencerDeductionPercentage;
  let influencerId = req.body.influencerId;
  var dataArr = {};
  let currency = '';
  db.BrandOwnerAdminTransaction.findOne({
    where: { id: id },
    include: [
      {
        model: db.campaignAssign,
        include: [
          {
            model: db.influencer,
            attributes: ['payout_type', 'payout_mail', 'id', 'currency', 'total_earning'],

            include: [
              {
                model: db.currency,
              }
            ]

          }
        ]

      },
    ]
  })
    .then(data => {
      db.currency.findOne({
        where: { 'is_default': '1' },
      })
        .then(settingData => {
          //console.log(data.CampaignAssign.Influencer.Currency);return;
          //set currency values
          let defaultCurrencyId = settingData.id;
          //let currency = settingData.code;
          //let currencyFeePertg = settingData.online_currency_fee;
          let currency = data.CampaignAssign.Influencer.Currency.code;
          let currencyFeePertg = data.CampaignAssign.Influencer.Currency.online_currency_fee;
          let influencerCurrencyId = data.CampaignAssign.Influencer.currency;

          //check influencer payment type
          let payOutType = data.CampaignAssign.Influencer.payout_type;
          let payOutEmail = data.CampaignAssign.Influencer.payout_mail.toString();
          //console.log(data); return;

          let totalAmount = data.net_amount;
          //console.log(totalAmount); return;

          //calculate admin commission percentage from influencer
          let adminDeductionAmt = Number(totalAmount) * Number(influencerDeductionPercentage / 100);
          //console.log(adminDeductionAmt); return;

          let afterDeduction = totalAmount - adminDeductionAmt;
          //console.log(afterDeduction); return;

          //paypal charge on deducted amount
          let paypalChargeAmnt = Number(afterDeduction) * Number(influencerPaypalCharge / 100);
          paypalChargeAmnt = paypalChargeAmnt.toFixed(2);
          //console.log(paypalChargeAmnt); return;
          dataArr['brand_owner_admin_transaction_id'] = data.id;
          dataArr['influencer_id'] = influencerId;
          //console.log(dataArr); return;

          dataArr['payout_mail'] = payOutEmail;
          dataArr['total_amount'] = totalAmount;
          dataArr['admin_commission_influncer_pertg'] = influencerDeductionPercentage;
          dataArr['admin_commission_influncer_amt'] = adminDeductionAmt;

          dataArr['default_currency_id'] = defaultCurrencyId;
          dataArr['influencer_currency_id'] = influencerCurrencyId;
          dataArr['currency_fee_pertg'] = currencyFeePertg;

          //console.log(dataArr);return

          let netAmount = 0;
          let currencyFeeAmt = 0;
          let netAmountWithCommisn = 0;

          if (payOutType == 'Auto') {
            //console.log(afterDeduction); return;

            //assuming zero represent default currency 
            if (influencerCurrencyId > 0 && influencerCurrencyId != defaultCurrencyId) {
              //currency mismatch calculate currency fee

              netAmountWithCommisn = Number(afterDeduction) - Number(paypalChargeAmnt);
              currencyFeeAmt = Number(netAmountWithCommisn) * Number(currencyFeePertg / 100);
              netAmount = Number(netAmountWithCommisn) - Number(currencyFeeAmt);


            } else {
              netAmount = Number(afterDeduction) - Number(paypalChargeAmnt);
              currencyFeeAmt = 0;
            }

            netAmount = netAmount.toFixed(2);
            //console.log(netAmount);return;

            dataArr['paypal_charge_influencer_pertg'] = influencerPaypalCharge;
            dataArr['paypal_charge_influencer_amt'] = paypalChargeAmnt;
            dataArr['payout_type'] = 'Auto';
            dataArr['net_amount'] = netAmount;
            dataArr['currency_fee_amt'] = currencyFeeAmt;


            //START PAYPAL PAYOUT
            var sender_batch_id = Math.random().toString(36).substring(9);
            var create_payout_json = {
              "sender_batch_header": {
                "sender_batch_id": sender_batch_id,
                "email_subject": "You have a payment"
              },
              "items": [
                {
                  "recipient_type": "EMAIL",
                  "amount": {
                    "value": netAmount.toString(),
                    "currency": currency.toString(),
                  },
                  "receiver": payOutEmail,
                  "note": "Thank you.",
                  "sender_item_id": "item_1"
                },
                /*{
                    "recipient_type": "EMAIL",
                    "amount": {
                        "value": 6,
                        "currency": "GBP"
                    },
                    "receiver": "sb-phzkq516077@personal.example.com",
                    "note": "Thank you.",
                    "sender_item_id": "item_2"
                }*/
              ]
            };


            paypal.payout.create(create_payout_json, function (error, payout) {
              if (error) {
                console.log(error.response);
                return res.status(200).json({ responseCode: 420, responseDetails: 'Something went wrong!.', "error": error.response });
                //throw error;
              } else {
                console.log("Create Single Payout Response");
                console.log(payout);
                var payoutId = payout.batch_header.payout_batch_id;
                paypal.payout.get(payoutId, function (error, payout) {
                  if (error) {
                    console.log(error);
                    return res.status(200).json({ responseCode: 420, responseDetails: 'Something went wrong!.', "error": error.response });
                    //throw error;
                  } else {
                    console.log("Get Payout Response");
                    let payoutResponse = JSON.stringify(payout);
                    dataArr['net_amount'] = payout.batch_header.amount.value;
                    dataArr['transaction_id'] = payout.batch_header.payout_batch_id;
                    dataArr['payout_status'] = payout.batch_header.batch_status;
                    dataArr['transaction_details'] = payout;
                    //dataArr['currency_fee_amt'] = 0;
                    dataArr['invoice_number'] = 'INF' + Date.now();
                    //console.log(dataArr);return
                    return db.InfluencerAdminTransaction.create(dataArr)
                      .then(retData => {
                        if (payout.batch_header.batch_status == "SUCCESS") {
                          sequelize.query("UPDATE influencer SET total_earning = total_earning + " + netAmount + "  WHERE id = '" + influencerId + "'").then((r) => {
                            //console.log(youtubeData);
                          }).catch(error => {
                            // Ooops, do some error-handling
                            return res.json({ responseCode: 420, responseDetails: 'Something went wrong1!', error: error });
                          });

                        }
                        return res.status(200).json({ responseCode: 200, responseDetails: '', data: retData });
                      })
                    console.log(JSON.stringify(payout));
                  }
                });
              }
            });

          }
          else {
            let netAmount = afterDeduction;
            netAmount = netAmount.toFixed(2);
            dataArr['net_amount'] = netAmount;
            dataArr['payout_type'] = 'Manual';
            dataArr['invoice_number'] = 'INF' + Date.now();
            return db.InfluencerAdminTransaction.create(dataArr)
              .then(retData => {
                return res.status(200).json({ responseCode: 200, responseDetails: '', data: retData });
              })

          }

        })
        .catch(reason => {
          return res.status(200).json({ responseCode: 420, responseDetails: 'Something went wrong!.', "error": reason });
        });


      //return res.status(200).json({ responseCode: 200, responseDetails: 'Get campaign data.', data: data});
    })
    .catch(reason => {
      return res.status(200).json({ responseCode: 420, responseDetails: 'Something went wrong!.', "error": reason });
    });

  //console.log(z);







});

/**
 * Method to get default currency details
 * @param {*} obj 
 */
const getDeaultCurrency = async obj => {
  return await currencyObj.findOne({
    where: obj,
  });
};

module.exports = router;