var express = require('express');
var router = express.Router();

/** Create Sequalize object  */
Sequelize = require("sequelize");
Op = Sequelize.Op;

db = require("./../models/associations");
const config = require("../config/default.json");
const auth = require("../middleware/auth");

/**
 * Get Platform List
 */
router.post('/list', auth, passport.authenticate('jwt', { session: false }),  async function(req, res, next) {
  
  if(!req.userInfo){
    return res.status(200).json({
        responseCode: "420",
        responseDetails: "Something went wrong!",
    });
  }
  return db.platform.findAll({
    attributes: ['id', 'name', 'icon', 'class_name'],
    order: [ [ 'name', 'ASC' ]],
    where:[{ status: 'Y'}]
  })
  .then(data => { 
    return res.status(200).json({ responseCode: 200, responseDetails: 'Get platform data.', data: data});
  })
  .catch(reason => {
    return res.status(200).json({ responseCode: 420, responseDetails: 'Something went wrong!.', "error": reason});
  });
   
  });

/**
 * Get Brand List with id maping
 */
router.post('/getDist', auth, passport.authenticate('jwt', { session: false }),  async function(req, res, next) {

  return db.platform.findAll({
    attributes: ['id', 'name', 'icon', 'class_name'],
    order: [ [ 'name', 'ASC' ]],
    where:[{ status: 'Y'}]
  })
  .then(data => { 
    retData=[]
    for(i=0;i<data.length;i++){
      retData[data[i]['id']]=data[i];
    }
    return res.status(200).json({ responseCode: 200, responseDetails: 'Get platform data.', data: retData});
  })
  .catch(reason => {
    return res.status(200).json({ responseCode: 420, responseDetails: 'Something went wrong!.', "error": reason});
  });
   
  });

//**=============== Sequalize init =============== */

/**
 * Method to get all active country list
 */
router.get('/list', auth, passport.authenticate('jwt', { session: false }),  async function(req, res, next) {

  return db.platform.findAll({
      attributes: ['id', 'name', 'icon', 'class_name'],
      order: [ [ 'name', 'ASC' ]],
      where:[{ status: 'Y'}]
    })
    .then(data => { 
      return res.status(200).json({ responseCode: 200, responseDetails: 'Get platform data.', data: data});
    })
    .catch(reason => {
      return res.status(200).json({ responseCode: 420, responseDetails: 'Something went wrong!.', "error": reason});
    });
});

/**
 * Get Logged in influncer platform
 */
router.post('/logged-in-influncer-platform', auth, passport.authenticate('jwt', { session: false }),  async function(req, res, next) {
  if(!req.userInfo){
    return res.status(200).json({
        responseCode: "420",
        responseDetails: "Something went wrong!",
    });
  }
  
  let influencer_id = req.userInfo.id;
  return db.influencerPlatformMap.findAll({
    where:{ 'influencer_id': influencer_id},
    include: [{model: db.platform, attributes: ['id', 'name', 'icon', 'class_name'] }]
  })
  .then(data => { 
    return res.status(200).json({ responseCode: 200, responseDetails: 'Get platform data.', data: data});
  })
  .catch(reason => {
    return res.status(200).json({ responseCode: 420, responseDetails: 'Something went wrong!.', "error": reason});
  });

});

/**
 * Save Price for influncer platform
 */
router.post('/save-price', auth, passport.authenticate('jwt', { session: false }),  async function(req, res, next) {
  if(!req.userInfo){
    return res.status(200).json({
        responseCode: "420",
        responseDetails: "Something went wrong!",
    });
  }

  let data = req.body.data;
  let type = req.body.type;
  let influencer_id = req.userInfo.id;
  let keys = Object.keys(data)
  for (let id of keys) {
    let price = data[id];
    if(type == 'rm'){
      if(price > 0){
        var resp = await db.influencerPlatformMap.update({'readymade_content_price': price}, {where:{'id':id}});
      }
      
    }else{
      if(price >0){
        var resp = await db.influencerPlatformMap.update({'generate_content_price': price}, {where:{'id':id}});
      }
    }
    
  }
  return res.status(200).json({ responseCode: 200, responseDetails: 'price updated Successfully.'});

});

module.exports = router;
