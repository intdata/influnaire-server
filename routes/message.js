var express = require('express');
var router = express.Router();

/** Create Sequalize object  */
Sequelize = require("sequelize");
Op = Sequelize.Op;
const auth = require("../middleware/auth");
db = require("./../models/associations");
const config = require("../config/default.json");
sharedService = require("./sharedService")
const campaignImagePath = config.adminAssetpath + "uploads/campaign/";
const messageFilePath = config.adminAssetpath + "uploads/message_files/";
var im = require('imagemagick');
var fs = require('fs');
const upload = require("../middleware/upload");
var qs = require('querystring');
const brandOwnerImagePath = config.adminBaseUrl + "uploads/brand_owner/";
const filePath = config.adminBaseUrl + "uploads/message_files/";
const influencerImagePath = config.adminBaseUrl + "uploads/influencer/";

/**
 * Get latest message for brand owner panel
*/
router.post('/getLatestMessage', auth, passport.authenticate('jwt', { session: false }),  async  function(req, res, next) {
  
  if(!req.userInfo){
    return res.status(200).json({
        responseCode: "420",
        responseDetails: "Something went wrong!",
    });
  }
  // return db.campaign.findAll({
  //     where:{brand_owner_id: req.userInfo.id},
  //     include : [{model:db.CampaignMessageConversation,attributes:['campaign_id', 'influencer_id', 'message', 'message_from', [sequelize.fn('count', sequelize.col('CampaignMessageConversations.id')), 'commentCnt']], include :[{model:db.influencer}]}, {model:db.brandowner}],
  //     group: ['CampaignMessageConversations.influencer_id','CampaignMessageConversations.campaign_id']
  //   })
  return  db.campaign.findAll({
      where:{brand_owner_id: req.userInfo.id},
      order: [['id', 'DESC']],
      include : [{model:db.campaignAssign, include :[{model:db.influencer}]}, {model:db.brandowner}]
    })
    .then(data => { 
      return res.status(200).json({ responseCode: 200, responseDetails: 'Get message data.', data: data, brandOwnerImagePath:brandOwnerImagePath,filePath:filePath, influencerImagePath:influencerImagePath});
    })
    .catch(reason => {
      return res.status(200).json({ responseCode: 420, responseDetails: 'Something went wrong!.', "error": reason});
    });
});

/**
 * get influncer message count campaign wise
 */
router.post('/influncerMessageCount', [auth, upload.single({path: campaignImagePath, fieldName: 'up_file'})], passport.authenticate('jwt', { session: false }),  async function(req, res, next) {
  
  if(!req.userInfo){
    return res.status(200).json({
        responseCode: "420",
        responseDetails: "Something went wrong!",
    });
  }
  brand_owner_id = req.userInfo.id
  return db.CampaignMessageConversation.findAll({
    where:{brand_owner_id: brand_owner_id},
    attributes:['campaign_id', 'influencer_id', [sequelize.fn('count', sequelize.col('id')), 'commentCnt']],
    group: ['influencer_id','campaign_id']
  })
  .then(data => { 
    return res.status(200).json({ responseCode: 200, responseDetails: 'Get message data.', data: data, brandOwnerImagePath:brandOwnerImagePath,filePath:filePath, influencerImagePath:influencerImagePath});
  })
  .catch(reason => {
    return res.status(200).json({ responseCode: 420, responseDetails: 'Something went wrong!.', "error": reason});
  });

});

/**
 * Get message of a influncer for the campaign
*/
router.post('/getInfluncerDetailMessage', [auth, upload.single({path: campaignImagePath, fieldName: 'up_file'})], passport.authenticate('jwt', { session: false }),  async function(req, res, next) {
  campaignId = req.body.campaignId
  influncerId = req.body.influncerId
  let campainDetail = await getCampaign({id:campaignId});
  let brandCurrencyDetail = await getBrandOwnerCurrency({id:campainDetail.brand_owner_id});
  let brandCurrencyPertg = await getBrandOwnerCurrencyPertg({id:brandCurrencyDetail.currency});

  
  return db.CampaignMessageConversation.findAll({
    where:{campaign_id: campaignId, influencer_id: influncerId},
    include :[{model:db.influencer}, {model:db.brandowner}]
  })
  .then(data => { 
    db.campaignHistory.findAll(
      {
        where: {
          campaign_id: campaignId, 
          influencer_id: influncerId
        }
      }
    ).then(campaignHistory => {
      db.campaignAssign.findOne(
        {
          where: {
            campaign_id: campaignId, 
            influencer_id: influncerId,
          },
          include: [{
            model: db.proposal,
            attributes: ['campaign_id', 'influencer_id', 'invitation_id','price','currency'],
            //where: {invitation_id: campaignAssign.id}
          },
          {
            model: db.campaign,
            attributes: ['campaign_id', 'brand_owner_id']
          },
          {
            model: db.BrandOwnerAdminTransaction,
            attributes: ['id', 'brand_owner_id', 'campaign_assign_id','total_amount'],
            include: [
              { 
                model: db.InfluencerAdminTransaction,
                attributes: ['id', 'brand_owner_admin_transaction_id', 'payout_type','payout_status'],
              }],
        }]
        }
      ).then(campaignAssign => {
        return res.status(200).json({ 
          responseCode: 200, 
          responseDetails: 'Get message data.', 
          data: data, 
          brandOwnerImagePath:brandOwnerImagePath,
          campaignHistory: campaignHistory,
          campaignAssign: campaignAssign,
          filePath:filePath, 
          influencerImagePath:influencerImagePath,
          campainDetail:campainDetail,
          brandCurrencyDetail:brandCurrencyDetail,
          brandCurrencyPertg:brandCurrencyPertg
        });
      }).catch(reason => {
        return res.status(200).json({ responseCode: 420, responseDetails: 'Something went wrong!.', "error": reason});
      });
    }).catch(reason => {
      return res.status(200).json({ responseCode: 420, responseDetails: 'Something went wrong!.', "error": reason});
    });
  }).catch(reason => {
    return res.status(200).json({ responseCode: 420, responseDetails: 'Something went wrong!.', "error": reason});
  });

});

/**
 * Get message of a infulcer brandowner 
*/
router.post('/getInfluncerMessage', auth, passport.authenticate('jwt', { session: false }),  async  function(req, res, next) {
  
  if(!req.userInfo){
    return res.status(200).json({
        responseCode: "420",
        responseDetails: "Something went wrong!",
    });
  }
  var campaignId =req.body.campaign
  var influncerId =req.body.influncer?req.body.influncer : req.userInfo.id;

  return db.CampaignMessageConversation.findAll({
      where:{campaign_id: campaignId, influencer_id: influncerId},
      order: [['id', 'ASC']],
      include : [{model:db.influencer}, {model:db.brandowner}],
      
    })
    .then(data => { 
        db.campaignHistory.findAll(
          {
            where: {
              campaign_id: campaignId, 
              influencer_id: influncerId
            }
          }
        ).then(campaignHistory => {
          return res.status(200).json({ 
            responseCode: 200, 
            responseDetails: 'Get Message data.', 
            data: data, 
            campaignHistory: campaignHistory,
            brandOwnerImagePath:brandOwnerImagePath,
            filePath:filePath, 
            influencerImagePath:influencerImagePath
          });
      })
      .catch(reason => {
        return res.status(200).json({ responseCode: 420, responseDetails: 'Something went wrong!.', "error": reason});
      });
    });
});

/**
 * Get message for influncer list
 */
router.post('/getInfluncerLatestMessage', auth, passport.authenticate('jwt', { session: false }),  async  function(req, res, next) {

  if(!req.userInfo){
    return res.status(200).json({
        responseCode: "420",
        responseDetails: "Something went wrong!",
    });
  }
  return db.campaignAssign.findAll({
      where:{influencer_id: req.userInfo.id},
      order: [['id', 'DESC']],
      include : [{model:db.influencer}, {model:db.campaign, include:[{model:db.brandowner}]}]      
    })
    .then(data => { 
      return res.status(200).json({ responseCode: 200, responseDetails: 'Get Message data.', data: data, brandOwnerImagePath:brandOwnerImagePath, filePath:filePath, influencerImagePath:influencerImagePath});
    })
    .catch(reason => {
      return res.status(200).json({ responseCode: 420, responseDetails: 'Something went wrong!.', "error": reason});
    });
})
/**
 * Save message from
 */
router.post('/saveMessage', [auth, upload.allType({path: messageFilePath, fieldName: 'up_file'})], passport.authenticate('jwt', { session: false }),  async function(req, res, next) {
  
  var save_data ={}
  save_data['file'] = (req.file && req.file.filename)?req.file.filename : '';
  save_data['campaign_id'] = req.body.campaign_id;
  save_data['influencer_id'] = req.body.influencer_id;
  save_data['brand_owner_id'] = req.body.brand_owner_id;
  save_data['message'] = req.body.message;
  save_data['message_from'] = req.body.message_from;
  save_data['created_at'] = sequelize.fn('NOW')
  return db.CampaignMessageConversation.create(save_data)
  .then(data => { 
    return res.status(200).json({ responseCode: 200, responseDetails: 'Get Message data.', data: data, brandOwnerImagePath:brandOwnerImagePath, filePath:filePath, influencerImagePath:influencerImagePath});
  })
  .catch(reason => {
    return res.status(200).json({ responseCode: 420, responseDetails: 'Something went wrong!.', "error": reason});
  });
})

/**
 * Save message from
 */
router.post('/saveContentMessage', [auth, upload.allType({path: messageFilePath, fieldName: 'up_file'})], passport.authenticate('jwt', { session: false }),  async function(req, res, next) {
  
  let campaign_id = req.body.campaign_id;
  let influencer_id = req.body.influencer_id;
  let brand_owner_id = req.body.brand_owner_id;
  //Create object for history table update
  var history_update={}
  history_update.campaign_id = campaign_id
  history_update.influencer_id = influencer_id
  history_update.brand_owner_id = brand_owner_id
  history_update.action = 'Content Received';
  history_update.taken_by = 'Influencer'
  history_update.created_at = sequelize.fn('NOW')
  
  //Notification Update data
  let notificationTemplateAlias = 'content_received';
  let notificationTemplate = await getNotificationTemplate({ alias: notificationTemplateAlias });
  //console.log(notificationTemplate)
  var notification_update = {}
  notification_update.title	=  (notificationTemplate && notificationTemplate.length  && notificationTemplate.title)?notificationTemplate.title:''
  notification_update.content	=  (notificationTemplate && notificationTemplate.length  && notificationTemplate.content)?notificationTemplate.content:''
  notification_update.sender_id = req.userInfo.id
  notification_update.receiver_id = brand_owner_id
  notification_update.campaign_id = campaign_id
  notification_update.receiver_type = 'BrandOwner'
  notification_update.created_at = sequelize.fn('NOW')

  var save_data ={}
  save_data['file'] = (req.file && req.file.filename)?req.file.filename : '';
  save_data['campaign_id'] = campaign_id;
  save_data['influencer_id'] = influencer_id;
  save_data['brand_owner_id'] = req.body.brand_owner_id?req.body.brand_owner_id:req.userInfo.id;
  save_data['message'] = req.body.message;
  save_data['message_from'] = req.body.message_from;
  save_data['created_at'] = sequelize.fn('NOW')
  return db.CampaignMessageConversation.create(save_data)
  .then(data => { 
    //console.log(req.file);
    if(req.file){
      //console.log(req.body.campaign_id+'==>'+req.body.influencer_id)
      return db.campaignAssign.update({'status':'CONTENT_SUBMITTED'}, {where:{'campaign_id':req.body.campaign_id, 'influencer_id':req.body.influencer_id}})
      .then(data => { 
        db.campaignHistory.create(history_update);
        db.notification.create(notification_update);
        return res.status(200).json({ responseCode: 200, responseDetails: 'update content', data: data});
      })
      .catch(reason => {
        return res.status(200).json({ responseCode: 420, responseDetails: 'Something went wrong!.', "error": reason});
      });
    }else{
      return res.status(200).json({ responseCode: 200, responseDetails: 'Get Message data.', data: data, brandOwnerImagePath:brandOwnerImagePath, filePath:filePath, influencerImagePath:influencerImagePath});
    }
    
  })
  .catch(reason => {
    return res.status(200).json({ responseCode: 420, responseDetails: 'Something went wrong!.', "error": reason});
  });
})

/**
 * Method to get notification template by given condition
 * @param {*} obj 
 */
const getNotificationTemplate = async obj => {
  return await db.notificationTemplate.findOne({
    where: obj,
  });
};


/**
 * Method to get campaign details
 * @param {*} obj 
 */
const getCampaign = async obj => {
  return await db.campaign.findOne({
    where: obj,
    attributes: ['id', 'brand_owner_id']
  });
};
/**
 * Method to get brandowner currency id
 * @param {*} obj 
 */
const getBrandOwnerCurrency = async obj => {
  return await db.brandowner.findOne({
    where: obj,
    attributes: ['id', 'currency'],
  });
};

/**
 * Method to get brandowner currency details
 * @param {*} obj 
 */
const getBrandOwnerCurrencyPertg = async obj => {
  return await db.currency.findOne({
    where: obj,
  });
};






module.exports = router;