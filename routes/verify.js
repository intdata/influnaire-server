var express = require('express');
var router = express.Router();
var moment = require('moment');
//**=============== Sequalize init =============== */
Sequelize = require("sequelize");
Op = Sequelize.Op;
Influencer = require("./../models/influencer");
BrandOwner = require("./../models/brandowner");

var influencerObj = Influencer(sequelize, Sequelize);
var brandOwnerObj = BrandOwner(sequelize, Sequelize);
//**=============== Sequalize init =============== */

/**
 * To verify Influencer email
 */
router.post('/influencer_email', async function(req, res, next) {
  return influencerObj.findOne({
    where: { email: req.body.email }
  })
  .then(user => { 
    if (user.is_email_verified) {
      return res.status(200).json({responseCode: 200, responseDetails: 'Email Already Verified.'});
      //res.render('notice_page.html', { message: 'Email Already Verified.' });
    } else if (moment(user.updated_at).add(15, 'm') < moment()) {
      return res.status(200).json({responseCode: 200, responseDetails: 'Token Expired!'});
      //res.render('notice_page.html', { message: 'Token Expired.' });
    } else {
      if(user.verication_token === req.body.verication_token){
        return user
            .update({ is_email_verified: 1, verication_token: '' })
            .then(updatedUser => {
              return res.status(200).json({responseCode: 200, responseDetails: 'User with ' + user.email + ' has been verified.'});
              //res.render('notice_page.html', { message: 'User with ' + user.email + ' has been verified.' });
            })
            .catch(reason => {
              return res.status(200).json({responseCode: 201, responseDetails: 'Verification failed!'});
              //res.render('notice_page.html', { message: 'Verification failed.' });
            });
      } else{
        return res.status(200).json({responseCode: 200, responseDetails: 'Token expired!'});
        //res.render('notice_page.html', { message: 'Token Expired.' });
      }
      return influencerObj.findOne({
        where: { verication_token: req.body.verication_token }
      });
    }
  })
  .catch(reason => {
    return res.status(200).json({responseCode: 420, responseDetails: 'Email not exist!', error: reason});
    //res.render('notice_page.html', { message: 'Email not found.' });
  });
  
});

/**
 * To verify Brand Owner email
 */
router.post('/brand_owner_email', async function(req, res, next) {

  return brandOwnerObj.findOne({
    where: { email: req.body.email }
  })
  .then(user => { 
    //console.log(user.createdAt.toString(), moment(user.createdAt), moment(user.createdAt).add(15, 'm'), moment(),);
    if (user.is_email_verified) {
      return res.status(200).json({responseCode: 200, responseDetails: 'Email Already Verified'});
      //res.render('notice_page.html', { message: 'Email Already Verified.' });
    } else if (moment(user.updated_at).add(15, 'm') < moment()) {   
      return res.status(200).json({responseCode: 200, responseDetails: 'Token Expired!'});
      //res.render('notice_page.html', { message: 'Token Expired.' });
    }  else {
      if(user.verication_token === req.body.verication_token){
        return user
            .update({ is_email_verified: 1, verication_token: '' })
            .then(updatedUser => {
              return res.status(200).json({responseCode: 200, responseDetails: 'User with ' + user.email + ' has been verified'});
              //res.render('notice_page.html', { message: 'User with ' + user.email + ' has been verified.' });
            })
            .catch(reason => {
              return res.status(200).json({responseCode: 201, responseDetails: 'Verification failed!'});
            });
      } else{
        return res.status(200).json({responseCode: 200, responseDetails: 'Token expired!'});
        //res.render('notice_page.html', { message: 'Token Expired.' });
      }
      return brandOwnerObj.findOne({
        where: { verication_token: req.body.verication_token }
      });
    }
  })
  .catch(reason => {
    return res.status(200).json({responseCode: 420, responseDetails: "Email not exist!", error: reason});
    //res.render('notice_page.html', { message: 'Email not found.' });
  });
});

module.exports = router;
