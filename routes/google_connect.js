var express = require('express');
var router = express.Router();
var moment = require('moment');
var https = require('https');
var http = require('http');
var fs = require('fs');
var url = require('url');
var readline = require('readline');
var {google} = require('googleapis');
var OAuth2 = google.auth.OAuth2;
var open = require("open")

let privatekey = require("./../config/google_privatekey.json");
const auth = require("../middleware/auth");

let db = require('./../models/associations');

// If modifying these scopes, delete your previously saved credentials
// at ~/.credentials/youtube-nodejs-quickstart.json
var SCOPES = [
                'https://www.googleapis.com/auth/youtube.readonly',
            ];
var TOKEN_DIR = (process.env.HOME || process.env.HOMEPATH ||
    process.env.USERPROFILE) + '/.credentials/';
var TOKEN_PATH = TOKEN_DIR + 'youtube-nodejs-quickstart.json';
var googleKeys = './config/google_keys.json';

/**
 * Method to get youtube connect request
 */
router.get('/youtube', auth, passport.authenticate('jwt', { session: false }), async function(req, res, next) {
  let userId = req.userInfo.id;

  // Load client secrets from a local file.
  fs.readFile(googleKeys, function processClientSecrets(err, content) {
    if (err) {
      console.log('Error loading client secret file: ' + err);
      return;
    }

    // Authorize a client with the loaded credentials, then call the YouTube API.
    authorize(JSON.parse(content), getChannel, res, userId);
  });
});

router.get('/youtube/callback/', async function(req, res, next) {
  var url_parts = url.parse(req.url.replace('#', '?'), true);
  var query = url_parts.query;
  // let accessToken = query['access_token'];
  let userId = query['state'];
  // console.log(query, userId, accessToken);

  fs.readFile(googleKeys, function processClientSecrets(err, content) {
    if (err) {
      console.log('Error loading client secret file: ' + err);
      return;
    }

    // Authorize a client with the loaded credentials, then call the YouTube API.
    let credentials = JSON.parse(content);
    var clientSecret = credentials.web.client_secret;
    var clientId = credentials.web.client_id;
    var redirectUrl = credentials.web.redirect_uris[1];
    let oauth2Client = new OAuth2(clientId, clientSecret, redirectUrl);

    oauth2Client.getToken(query['code'], function(err, token) {
      if (err) {
        console.log('Error while trying to retrieve access token', err);
        return;
      }
      oauth2Client.credentials = token;
      storeToken(token, userId);
      getChannel(oauth2Client, res, userId);
    });
  });

});

/**
 * Create an OAuth2 client with the given credentials, and then execute the
 * given callback function.
 *
 * @param {Object} credentials The authorization client credentials.
 * @param {function} callback The callback to call with the authorized client.
 */
function authorize(credentials, callback, res, userId) {
  var clientSecret = credentials.web.client_secret;
  var clientId = credentials.web.client_id;
  var redirectUrl = credentials.web.redirect_uris[1];
  var oauth2Client = new OAuth2(clientId, clientSecret, redirectUrl);

  db.influencerPlatformMap
  .findOne({where:{influencer_id:userId, platform_id:3}})
  .then(result =>{
    if(result && result.auth && result.auth != ''){
      // get the auth
      oauth2Client.credentials = JSON.parse(result.auth);
      callback(oauth2Client, res, userId);
    } else{
      // create auth
      getNewToken(oauth2Client, callback, res, userId);
    }
  }).catch(e =>{
    console.log(e);
    // create auth
    getNewToken(oauth2Client, callback, res, userId);
  });

}

/**
 * Get and store new token after prompting for user authorization, and then
 * execute the given callback with the authorized OAuth2 client.
 *
 * @param {google.auth.OAuth2} oauth2Client The OAuth2 client to get token for.
 * @param {getEventsCallback} callback The callback to call with the authorized
 *     client.
 */
async function getNewToken(oauth2Client, callback, res, userId) {

  // // configure a JWT auth client
  // let jwtClient = new google.auth.JWT(
  //     privatekey.client_email,
  //     null,
  //     privatekey.private_key,
  //     SCOPES
  //   );
  //   //authenticate request
  //   jwtClient.authorize(function (err, tokens) {
  //     if (err) {
  //       console.log(err);
  //       return;
  //     } else {
  //       console.log("Successfully connected!");
  //       oauth2Client.credentials = tokens;
  //       storeToken(tokens, userId);
  //       callback(oauth2Client, res, userId);
  //       //res.json(tokens);
  //     }
  // });
  // -----------------------------------------------------------------
  // oauth2Client.getToken('sss', function (err, tokens) {
  //       if (err) {
  //         console.log(err);
  //         return;
  //       } else {
  //         console.log("Successfully connected!", tokens);
  //         return;
  //         oauth2Client.credentials = tokens;
  //         storeToken(tokens, userId);
  //         callback(oauth2Client, res, userId);
  //         //res.json(tokens);
  //       }
  //   });

  var authUrl = oauth2Client.generateAuthUrl({
    //access_type: 'offline',
    //response_type: 'token',
    scope: SCOPES,
    state: userId
  });
  console.log('Authorize this app by visiting this url: ', authUrl);
  return open(authUrl);
  //return res.redirect(authUrl);

  // Set the configuration settings
  // const credentials = {
  //   client: {
  //     id: '1037055028710-0s1te2g060l2uimbn5ojvk1nqmcndmcq.apps.googleusercontent.com',
  //     secret: 'NLoEhs5-1z7uYFcrYimhb7Y3'
  //   },
  //   auth: {
  //     tokenHost: 'https://accounts.google.com/'
  //   }
  // };
  
  // // Initialize the OAuth2 Library
  // const oauth2 = require('simple-oauth2').create(credentials);

  // const tokenConfig = {
  //   scope: SCOPES, // also can be an array of multiple scopes, ex. ['<scope1>, '<scope2>', '...']
  // };
   
  // // Optional per-call http options
  // const httpOptions = {};
   
  // // Get the access token object for the client
  // try {
  //   const result = await oauth2.clientCredentials.getToken(tokenConfig, httpOptions);
  //   const accessToken = oauth2.accessToken.create(result);
  //   console.log('access token: ', accessToken);
  // } catch (error) {
  //   console.log('Access Token error', error.message);
  // }

  // // Authorization oauth2 URI
  // const authorizationUri = oauth2.authorizationCode.authorizeURL({
  //   redirect_uri: 'http://localhost:3000/callback',
  //   scope: SCOPES, // also can be an array of multiple scopes, ex. ['<scope1>, '<scope2>', '...']
  //   //state: 'offline'
  // });
  
  // // Redirect example using Express (see http://expressjs.com/api.html#res.redirect)
  // res.redirect(authorizationUri);
  
  // // Get the access token object (the authorization code is given from the previous step).
  // const tokenConfig = {
  //   code: '<code>',
  //   redirect_uri: 'http://localhost:3000/callback',
  //   scope: SCOPES, // also can be an array of multiple scopes, ex. ['<scope1>, '<scope2>', '...']
  // };
  
  // // Optional per-call http options
  // const httpOptions = {};
  
  // // Save the access token
  // try {
  //   const result = await oauth2.authorizationCode.getToken(tokenConfig, httpOptions);
  //   const accessToken = oauth2.accessToken.create(result);
  //   console.log('access token: ', accessToken);
    
  // } catch (error) {
  //   console.log('Access Token Error', error.message);
  // }

}

/**
 * Store token to disk be used in later program executions.
 *
 * @param {Object} token The token to store to disk.
 */
function storeToken(token, userId) {
  console.log(token);
  
  db.influencerPlatformMap
  .findOne({where:{influencer_id:userId, platform_id:3}})
  .then(result =>{
    if(result){
      // update auth
      result
        .update({auth:JSON.stringify(token)})
        .then(r => { console.log('auth saved!'); })
        .catch(e => { console.log(e); });
    } else{
      // create auth
      db.influencerPlatformMap
        .create({influencer_id: userId, platform_id: 3, auth: JSON.stringify(token)})
        .then(r => { console.log('auth saved!'); })
        .catch(e => { console.log(e); })
    }
  }).catch(e =>{
    console.log(e);
  });

}

/**
 * Lists the names and IDs of up to 10 files.
 *
 * @param {google.auth.OAuth2} auth An authorized OAuth2 client.
 */
function getChannel(auth, res, userId) {

  service = google.youtube('v3');
  service.channels.list({
    auth: auth,
    part: 'snippet,contentDetails,statistics',
    mine: 'true'
  }, function(err, response) {
    if (err) {
      console.log('The API returned an error: ' + err);
      return;
    }
    var channels = response.data.items;
    if (channels.length == 0) {
      console.log('No channel found.');
      return res.status(200).json({
          responseCode: 404,
          responseDetails: "No channel found.",
      });
    } else {
      console.log('This channel\'s ID is %s. Its title is \'%s\', and ' +
                  'it has %s views.',
                  channels[0].id,
                  channels[0].snippet.title,
                  channels[0].statistics.viewCount,
                  channels[0].statistics.commentCount,
                  channels[0].statistics.subscriberCount,
                  channels[0].statistics.videoCount,
                  userId);
      return res.status(200).json({
        responseCode: 200,
        responseDetails: "Success! Youtube connected.",
        details: channels[0].statistics,
        resp:response.data
      });
    }
  });
}


module.exports = router;
