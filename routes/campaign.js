var express = require('express');
var router = express.Router();

/** Create Sequalize object  */
Sequelize = require("sequelize");
Op = Sequelize.Op;
const auth = require("../middleware/auth");
db = require("./../models/associations");
const config = require("../config/default.json");
sharedService = require("./sharedService")
const campaignImagePath = config.adminAssetpath + "uploads/campaign/";
const campaignImageThumbPath = config.adminAssetpath + "uploads/campaign/thumb/";
var im = require('imagemagick');
var fs = require('fs');
const isImage = require('is-image');
const upload = require("../middleware/upload");
var qs = require('querystring');
const brandOwnerImagePath = config.adminBaseUrl + "uploads/brand_owner/";
const influencerImagePath = config.adminBaseUrl + "uploads/influencer/";
const messageFilePath = config.adminBaseUrl + "uploads/message_files/";
const allPaths = {
  brandOwnerImagePath: brandOwnerImagePath,
  influencerImagePath: influencerImagePath,
  messageFilePath: messageFilePath
}

/**
 * Get Job List
*/
router.post('/getAll', auth, passport.authenticate('jwt', { session: false }), async function (req, res, next) {

  if (!req.userInfo) {
    return res.status(200).json({
      responseCode: "420",
      responseDetails: "Something went wrong!",
    });
  }
  var filter = req.body.filter;
  var platform_id = req.body.platform_id;
  let brandOwnerId = req.userInfo.id;
  if (req.body.type && req.body.type == 'current') {
    //filter[Op.or] = [{status: 'IN_PROGRESS'}, {status: 'DRAFT'}]
    filter.brand_owner_id = brandOwnerId;
    filter.status = ['DRAFT', 'ANNOUNCED', 'IN_PROGRESS']
    //filter.end_date = {[Op.gte]:Sequelize.literal('CURDATE()')}
  } else if (req.body.type && req.body.type == 'past') {
    filter.brand_owner_id = brandOwnerId;
    filter.status = ['PUBLISHED']
    //filter.end_date = {[Op.lt]:Sequelize.literal('CURDATE()')}
  }

  let filterPlatform = {};
  let monthYear = '';

  if (platform_id != '') {
    filterPlatform.platform_id = platform_id;
  }
  if (req.body.monthYear) {

    monthYear = req.body.monthYear;
    let date = new Date(monthYear);
    let firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
    let lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
    filter[Op.and] = [
      {
        start_date: {
          [Op.gte]: new Date(firstDay),
          [Op.lt]: new Date(lastDay)
        }
      }
    ]
  }

  return db.campaign.findAll({
    where: filter,
    order: [['id', 'DESC']],
    include: [
      // {
      //   model:db.CampaignPlatformMap
      // },
      {
        model: db.campaignAssign,
        include: [{
          model: db.influencer
        }]
      },
      {
        attributes: ['campaign_id', 'platform_id'],
        where: filterPlatform,
        model: db.CampaignPlatformMap,
        include: [
          {
            attributes: ['name', 'icon', 'class_name'],
            model: db.platform
          }
        ]
      }
    ]
  })
    .then(data => {
      return res.status(200).json({ responseCode: 200, responseDetails: 'Get campaign data.', data: data, allPaths: allPaths });
    })
    .catch(reason => {
      return res.status(200).json({ responseCode: 420, responseDetails: 'Something went wrong!.', "error": reason });
    });
});

/**
 * Get Job By Id
*/
router.post('/campaignById', auth, passport.authenticate('jwt', { session: false }), async function (req, res, next) {
  return db.campaign.findOne({
    where: { id: req.body.id },
    order: [['id', 'DESC']],
    include: [{ model: db.brandowner },
    { model: db.CampaignPlatformMap, include: [{ model: db.platform, attributes: ['name', 'class_name'] }] },
    {
      model: db.campaignAssign,
      include: [{
        model: db.influencer,
        include: [{ model: db.influencerPlatformMap, include: [{ model: db.platform, attributes: ['name', 'class_name'] }] }]
      }]
    }]
  })
    .then(data => {
      return res.status(200).json({ responseCode: 200, responseDetails: 'Get Past campaign data.', data: data, allPaths: allPaths });
    })
    .catch(reason => {
      return res.status(200).json({ responseCode: 420, responseDetails: 'Something went wrong!.', "error": reason });
    });
});


/**
 * Remove campaign from draft
 */
router.post('/remove-from-draft', auth, passport.authenticate('jwt', { session: false }), async function (req, res, next) {

  if (!req.userInfo) {
    return res.status(200).json({
      responseCode: "420",
      responseDetails: "Something went wrong!",
    });
  }
  var brand_owner_id = req.userInfo.id;
  var campaign_id = req.body.id;
  if (!campaign_id) {
    return res.status(200).json({
      responseCode: "420",
      responseDetails: "Something went wrong!",
    });
  }
  var conditions = { 'brand_owner_id': brand_owner_id, 'id': campaign_id, 'status': 'DRAFT' }
  console.log(conditions)
  return db.campaign.destroy({ where: conditions })
    .then(retData => {
      db.campaignAssign.destroy({ where: { 'campaign_id': campaign_id } });
      return res.status(200).json({ responseCode: 200, responseDetails: '', data: retData });
    })
    .catch(reason => {
      return res.status(200).json({ responseCode: 420, responseDetails: 'Something went wrong!.', "error": reason });
    });
});

/**
 * Create Campaign
 */
router.post('/createCampaign', auth, passport.authenticate('jwt', { session: false }), async function (req, res, next) {

  if (!req.userInfo) {
    return res.status(200).json({
      responseCode: "420",
      responseDetails: "Something went wrong!",
    });
  }
  var data = {}
  data['brand_owner_id'] = req.userInfo.id
  data['brand_id'] = req.body.brand
  data['campaign_name'] = req.body.campaignName
  data['start_date'] = req.body.startDate.year + '-' + req.body.startDate.month + '-' + req.body.startDate.day
  data['proposals_due_date'] = req.body.dueDate.year + '-' + req.body.dueDate.month + '-' + req.body.dueDate.day
  data['tentative_launch_date'] = req.body.launchDate.year + '-' + req.body.launchDate.month + '-' + req.body.launchDate.day
  data['service_type'] = req.body.serviceType
  data['status'] = null
  data['post_date'] = sequelize.fn('NOW')
  data['campaign_id'] = sharedService.rand(10, 'A#') //Generate the campaign id
  return db.campaign.create(data)
    .then(retData => {
      return res.status(200).json({ responseCode: 200, responseDetails: '', data: retData });
    })
    .catch(reason => {
      return res.status(200).json({ responseCode: 420, responseDetails: 'Something went wrong!.', "error": reason });
    });
});

/** 
 * Update Campaign First Step
 */
router.post('/updateCampaignFirst', auth, passport.authenticate('jwt', { session: false }), async function (req, res, next) {

  var data = {}
  data['brand_owner_id'] = req.body.data.brand
  data['campaign_name'] = req.body.data.campaignName
  data['start_date'] = req.body.data.startDate.year + '-' + req.body.data.startDate.month + '-' + req.body.data.startDate.day
  data['proposals_due_date'] = req.body.data.dueDate.year + '-' + req.body.data.dueDate.month + '-' + req.body.data.dueDate.day
  data['tentative_launch_date'] = req.body.data.launchDate.year + '-' + req.body.data.launchDate.month + '-' + req.body.data.launchDate.day
  data['service_type'] = req.body.data.serviceType
  return db.campaign.update(data, { where: { 'id': req.body.id } })
    .then(retData => {
      return res.status(200).json({ responseCode: 200, responseDetails: '', data: retData });
    })
    .catch(reason => {
      return res.status(200).json({ responseCode: 420, responseDetails: 'Something went wrong!.', "error": reason });
    });
});

/**
 * Update Campaign Second Step
 */
router.post('/updateCampaignSecond', [auth], passport.authenticate('jwt', { session: false }), async function (req, res, next) {

  //console.log("body=>",req.body);
  var data = {}
  var objective = { 'CC': 'co_created_content', 'CR': 'content_report', 'AE': 'advance_event' }
  data['objective'] = objective[req.body.data.objectControl]

  if (req.body.data.objectControl == 'CC') {
    data['objective_tentative_launch_date'] = req.body.data.cocreatedLD.year + '-' + req.body.data.cocreatedLD.month + '-' + req.body.data.cocreatedLD.day
  } else if (req.body.data.objectControl == 'CR') {
    data['objective_tentative_launch_date'] = req.body.data.repostLD.year + '-' + req.body.data.repostLD.month + '-' + req.body.data.repostLD.day
  } else {
    data['objective_tentative_launch_date'] = req.body.data.eventLD.year + '-' + req.body.data.eventLD.month + '-' + req.body.data.eventLD.day
  }

  return db.campaign.update(data, { where: { 'id': req.body.id } })
    .then(retData => {
      return res.status(200).json({ responseCode: 200, responseDetails: '', data: retData });
    })
    .catch(reason => {
      return res.status(200).json({ responseCode: 420, responseDetails: 'Something went wrong!.', "error": reason });
    });
});

/** 
 * Assign Influncer to campaign
 */
router.post('/assignInfluncer', [auth], passport.authenticate('jwt', { session: false }), async function (req, res, next) {

  if (!req.userInfo) {
    return res.status(200).json({
      responseCode: "420",
      responseDetails: "Something went wrong!",
    });
  }
  var brandOwnerId = req.userInfo.id;

  var campaignId = req.body.campaignId;
  var exp_date = req.body.exp_date;
  let notificationTemplateAlias = 'campaign_assigned';
  let notificationTemplate = await getNotificationTemplate({ alias: notificationTemplateAlias });
  db.campaign.findOne({ where: { id: campaignId } }).then(campaign => {
    if (req.body.type && req.body.type == 'publish') {
      campaign.update({ status: 'ANNOUNCED' });
    } else {
      campaign.update({ status: 'DRAFT' });
    }
    var savedata = []
    var notificationSave = []
    req.body.influncers.forEach(function (influncer_id) {
      assignData = { campaign_id: campaign.id, expiry_date: campaign.invitation_expiery_date, proposal_due_date: campaign.proposals_due_date, influencer_id: influncer_id, message: campaign.inovation_message, assign_date: sequelize.fn('NOW') }
      savedata.push(assignData);
      let notificationData = { 'title': notificationTemplate.title, 'content': notificationTemplate.content, 'sender_id': brandOwnerId, 'receiver_id': influncer_id, 'campaign_id': campaign.id, 'receiver_type': 'Influencer' }
      notificationSave.push(notificationData)
    });

    //db.campaignAssign.destroy({ where:{ campaign_id: campaignId}})
    db.campaignAssign.bulkCreate(savedata).then(retData => {
      db.notification.bulkCreate(notificationSave)
      return res.status(200).json({ responseCode: 200, responseDetails: '', data: retData });
    })
      .catch(reason => {
        return res.status(200).json({ responseCode: 420, responseDetails: 'Something went wrong!.', "error": reason });
      });
  });
})

/**
 * Campaign Status Wise count for brandowner dashboard
*/
router.post('/status-wise-count', [auth], passport.authenticate('jwt', { session: false }), async function (req, res, next) {

  if (!req.userInfo) {
    return res.status(200).json({
      responseCode: "420",
      responseDetails: "Something went wrong!",
    });
  }
  var brandOwnerId = req.userInfo.id;
  let filter = {};
  let date = new Date();
  let firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
  let lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
  filter.brand_owner_id = brandOwnerId;
  filter.status = ['DRAFT', 'ANNOUNCED', 'IN_PROGRESS']
  filter[Op.and] = [
    {
      start_date: {
        [Op.gte]: new Date(firstDay),
        [Op.lt]: new Date(lastDay)
      }
    }
  ]

  return db.campaign.findAll({
    where: filter,
    attributes: ['status', [sequelize.fn('count', sequelize.col('id')), 'campaignCnt']],
    group: ['status']
  }).then(retData => {
    return res.status(200).json({ responseCode: 200, responseDetails: 'campaign step added', data: retData });
  })
    .catch(reason => {
      return res.status(200).json({ responseCode: 420, responseDetails: 'Something went wrong!.', "error": reason });
    });

});

/**
 * Update Campaign Third Step
 */
router.post('/updateCampaignThird', [auth, upload.multiple({ path: campaignImagePath, fieldName: 'photos[]' })], passport.authenticate('jwt', { session: false }), async function (req, res, next) {

  var data = {}
  var platforms = req.body.platforms
  data['invitation_expiery_date'] = req.body.invitationExpieryDate
  data['inovation_message'] = req.body.invitationMessage
  data['campaign_details'] = req.body.campaignDetails
  data['content_concept'] = req.body.contentConcept
  data['product_url'] = req.body.productUrl
  data['total_amount_invested'] = req.body.budget
  data['country_id'] = req.body.country
  //console.log('campaignImagePath is'+campaignImagePath);
  //data['uploaded_file'] = (req.file && req.file.filename)? req.file.filename:'';
  //data['status'] = 'Pending';

  //update multiple images/files



  //create thumb if image
  /*if (req.files) {
    if (req.files.length > 0) {

      for (i = 0; i <= req.files.length-1; i++) {
        imgName = req.files[i].filename;
        //console.log(isImage(campaignImagePath + req.files[i].filename));
        if (isImage(campaignImagePath + req.files[i].filename)) {
          console.log(campaignImagePath + imgName);
          fs.readFile(req.files[i].path, function (err, data) {
            fs.writeFile(campaignImagePath, data, function (err) {
              // write file to uploads/thumbs folder
              im.resize({
                srcPath: campaignImagePath + imgName,
                dstPath: campaignImageThumbPath + imgName,
                width: 200
              }, function (err, stdout, stderr) {
                
                if (err) {
                  console.error(err)
                  return
                }
              
              });
            });
          });
        }
      }
    }
  }*/

  //add campaign files to campaign_files table
  //console.log(req.files.length);return;
  
  if (req.files) {
    if (req.files.length > 0) {
      var platformData = [];
      for (i = 0; i <= req.files.length-1; i++) {          
          imgName = req.files[i].filename;
          var extension = imgName.split('.').pop();
          //console.log(imgName);return
          pdata = {}
          pdata['campaign_id'] = req.body.id;
          pdata['file_extension'] = extension;
          pdata['name'] = imgName;
          platformData.push(pdata);
      }
      db.campaignFiles.bulkCreate(platformData);
    }
  }

  return db.campaign.update(data, { where: { 'id': req.body.id } })
    .then(retData => {
      var platformData = [];

      for (i = 0; i < platforms.length; i++) {
        if (!isNaN(platforms[i]) && platforms[i] != 0) {
          pdata = {}
          pdata['campaign_id'] = req.body.id;
          pdata['platform_id'] = platforms[i];
          pdata['created_at'] = sequelize.fn('NOW');
          platformData.push(pdata);
        }

      }
      db.CampaignPlatformMap.bulkCreate(platformData, { returning: true }).then((result) => {
        return res.status(200).json({ responseCode: 200, responseDetails: 'campaign step added', data: retData });
      });
      //return res.status(200).json({ responseCode: 200, responseDetails: 'campaign step added', data: platformData});
    })
    .catch(reason => {
      return res.status(200).json({ responseCode: 420, responseDetails: 'Something went wrong!.', "error": reason });
    });

});

/**
 * Method to get notification template by given condition
 * @param {*} obj 
 */
const getNotificationTemplate = async obj => {
  return await db.notificationTemplate.findOne({
    where: obj,
  });
};


/**
 * Get Job and assigned influencer details By Id
*/
router.post('/assignedInfluencer', auth, passport.authenticate('jwt', { session: false }), async function (req, res, next) {
  return db.campaign.findOne({
    where: { id: req.body.id },
    order: [['id', 'DESC']],
    include: [{ model: db.brandowner },
    { model: db.CampaignPlatformMap, include: [{ model: db.platform, attributes: ['name', 'class_name'] }] },
    {
      model: db.campaignAssign,
      where: { id: 41 },
      include: [{
        model: db.influencer,
        include: [{ model: db.influencerPlatformMap, include: [{ model: db.platform, attributes: ['name', 'class_name'] }] }]
      }]
    }]
  })
    .then(data => {
      return res.status(200).json({ responseCode: 200, responseDetails: 'Get Past campaign data.', data: data, allPaths: allPaths });
    })
    .catch(reason => {
      return res.status(200).json({ responseCode: 420, responseDetails: 'Something went wrong!.', "error": reason });
    });
});


/**
 * Create createBrandOwnerPayment
 */
router.post('/createBrandOwnerPayment', auth, passport.authenticate('jwt', { session: false }), async function (req, res, next) {

  if (!req.body.orderdetails) {
    return res.status(200).json({
      responseCode: "420",
      responseDetails: "Something went wrong!",
    });
  }
  var retData = {}
  retData['invoice_number'] = 'INF' + req.body.orderdetails.invoice_number;;
  retData['brand_owner_id'] = req.body.orderdetails.brand_owner_id;
  retData['campaign_assign_id'] = req.body.orderdetails.campaign_assign_id;
  retData['transaction_id'] = req.body.orderdetails.transaction_id;
  retData['total_amount'] = req.body.orderdetails.total_amount;;
  retData['net_amount'] = req.body.orderdetails.net_amount;
  retData['gross_amount'] = req.body.orderdetails.gross_amount;
  retData['admin_commission'] = req.body.orderdetails.admin_commission;
  retData['admin_commission_pertg'] = req.body.orderdetails.admin_commission_pertg;
  retData['paypal_charge_brandowner_amt'] = req.body.orderdetails.paypal_charge_brandowner_amt;
  retData['paypal_charge_brandowner_pertg'] = req.body.orderdetails.paypal_charge_brandowner_pertg;
  retData['payment_type'] = req.body.orderdetails.payment_type;
  retData['currency'] = req.body.orderdetails.currency;
  retData['payment_status'] = req.body.orderdetails.payment_status;
  retData['transaction_id'] = req.body.orderdetails.transaction_id;
  retData['transaction_details'] = req.body.orderdetails.transaction_details;
  retData['default_currency_id'] = req.body.orderdetails.default_currency_id;
  retData['brand_currency_id'] = req.body.orderdetails.brand_currency_id;
  retData['currency_fee_pertg'] = req.body.orderdetails.currency_fee_pertg;
  retData['currency_fee_amt'] = req.body.orderdetails.currency_fee_amt;

  return db.BrandOwnerAdminTransaction.create(retData)
    .then(retData => {
      return res.status(200).json({ responseCode: 200, responseDetails: '', data: retData });
    })
    .catch(reason => {
      return res.status(200).json({ responseCode: 420, responseDetails: 'Something went wrong!.', "error": reason });
    });
});

/**
 * Create createBrandOwnerPayment
 */
router.post('/updateBrandOwnerPayment', auth, passport.authenticate('jwt', { session: false }), async function (req, res, next) {

  if (!req.body.orderdetails) {
    return res.status(200).json({
      responseCode: "420",
      responseDetails: "Something went wrong!",
    });
  }
  var retData = {}
  retData['payment_status'] = req.body.orderdetails.status;
  return db.BrandOwnerAdminTransaction.update(retData, { where: { 'transaction_id': req.body.orderdetails.transaction_id } })
    .then(retData => {
      return res.status(200).json({ responseCode: 200, responseDetails: '', data: retData });
    })
    .catch(reason => {
      return res.status(200).json({ responseCode: 420, responseDetails: 'Something went wrong!.', "error": reason });
    });
});

/**
 * Payment Analysis
 */
router.post('/payment-analysis', auth, passport.authenticate('jwt', { session: false }),  async function(req, res, next) {

  if(!req.body.campaignId){
    return res.status(200).json({
        responseCode: "420",
        responseDetails: "Something went wrong!",
    });
  }

  if(!req.userInfo){
    return res.status(200).json({
        responseCode: "420",
        responseDetails: "Something went wrong!",
    });
  }

  var campaignId = req.body.campaignId;
  return db.campaignAssign.findAll({
    where:{campaign_id: campaignId},
    attributes: ['status'],
    include:[{model:db.BrandOwnerAdminTransaction, attributes: ['total_amount', 'created_at', 'currency'],
      include:[{model: db.InfluencerAdminTransaction, attributes: ['total_amount', 'manual_payout_status', 'manual_payout_on'],
        include:[{model:db.influencer, attributes:['first_name', 'last_name', 'picture']}]
      }]
    }]
  }).then(retData => { 
    return res.status(200).json({ responseCode: 200, responseDetails: '', data: retData, influencerImagePath:influencerImagePath});
  })
  .catch(reason => {
    return res.status(200).json({ responseCode: 420, responseDetails: 'Something went wrong!.', "error": reason});
  });
    
});

/**
 * Follower Analysis
 */
router.post('/follower-analysis', auth, passport.authenticate('jwt', { session: false }),  async function(req, res, next) {

  if(!req.body.campaignId){
    return res.status(200).json({
        responseCode: "420",
        responseDetails: "Something went wrong!",
    });
  }

  if(!req.userInfo){
    return res.status(200).json({
        responseCode: "420",
        responseDetails: "Something went wrong!",
    });
  }

  var campaignId = req.body.campaignId;
  const tempSQL = sequelize.dialect.QueryGenerator.selectQuery('campaign_assign' ,{
    attributes: ['influencer_id'],
    where: {
      campaign_id: campaignId
    }})
    .slice(0,-1);

  return db.influencerPlatformMap.findAll({
    attributes:[[sequelize.fn('SUM', sequelize.col('followers')), 'total_followers'], 'influencer_id'],
    where:{'influencer_id': [sequelize.literal('(' + tempSQL + ')')]},
    include:[{model:db.influencer, attributes:['first_name', 'last_name', 'picture']}],
    group: ['influencer_id']
  }).then(retData => { 
      return res.status(200).json({ responseCode: 200, responseDetails: '', data: retData, influencerImagePath:influencerImagePath});
    })
    .catch(reason => {
      return res.status(200).json({ responseCode: 420, responseDetails: 'Something went wrong!.', "error": reason});
    });
    
});

/**
 * Like and comment Analusis
 */
router.post('/like-comment-analysis', auth, passport.authenticate('jwt', { session: false }),  async function(req, res, next) {

  if(!req.body.campaignId){
    return res.status(200).json({
        responseCode: "420",
        responseDetails: "Something went wrong!",
    });
  }

  if(!req.userInfo){
    return res.status(200).json({
        responseCode: "420",
        responseDetails: "Something went wrong!",
    });
  }

  var campaignId = req.body.campaignId;
  const tempSQL = sequelize.dialect.QueryGenerator.selectQuery('campaign_assign' ,{
    attributes: ['influencer_id'],
    where: {
      campaign_id: campaignId
    }})
    .slice(0,-1);

  return db.influencerPlatformMap.findAll({
    attributes:[[sequelize.fn('SUM', sequelize.col('total_like')), 'total_likes'], [sequelize.fn('SUM', sequelize.col('total_comments')), 'total_user_comments'], 'influencer_id'],
    where:{'influencer_id': [sequelize.literal('(' + tempSQL + ')')]},
    include:[{model:db.influencer, attributes:['first_name', 'last_name', 'picture']}],
    group: ['influencer_id']
  }).then(retData => { 
      return res.status(200).json({ responseCode: 200, responseDetails: '', data: retData, influencerImagePath:influencerImagePath});
    })
    .catch(reason => {
      return res.status(200).json({ responseCode: 420, responseDetails: 'Something went wrong!.', "error": reason});
    });
    
});

/**
 * Platform analysis of the campaign influncer
 */
router.post('/platform-count-analysis', auth, passport.authenticate('jwt', { session: false }),  async function(req, res, next) {

  if(!req.body.campaignId){
    return res.status(200).json({
        responseCode: "420",
        responseDetails: "Something went wrong!",
    });
  }

  if(!req.userInfo){
    return res.status(200).json({
        responseCode: "420",
        responseDetails: "Something went wrong!",
    });
  }

  var campaignId = req.body.campaignId;
  const tempSQL = sequelize.dialect.QueryGenerator.selectQuery('campaign_assign' ,{
    attributes: ['influencer_id'],
    where: {
      campaign_id: campaignId
    }})
    .slice(0,-1);

  return db.influencerPlatformMap.findAll({
    attributes:['influencer_id'],
    where:{'influencer_id': [sequelize.literal('(' + tempSQL + ')')]},
    include:[{model:db.influencer, attributes:['first_name', 'last_name', 'picture']}, 
    {model : db.platform, attributes:['name', 'icon', 'class_name']}]
  }).then(retData => { 
      return res.status(200).json({ responseCode: 200, responseDetails: '', data: retData, influencerImagePath:influencerImagePath});
    })
    .catch(reason => {
      return res.status(200).json({ responseCode: 420, responseDetails: 'Something went wrong!.', "error": reason});
    });
    
});

/**
 * Complete a campaign
 */

router.post('/complete-campaign', auth, passport.authenticate('jwt', { session: false }),  async function(req, res, next) {

  if(!req.body.campaignId){
    return res.status(200).json({
        responseCode: "420",
        responseDetails: "Something went wrong!",
    });
  }

  if(!req.userInfo){
    return res.status(200).json({
        responseCode: "420",
        responseDetails: "Something went wrong!",
    });
  }

  var campaignId = req.body.campaignId;
 
  return db.campaign.update({'status' : 'PUBLISHED'}, { where: { 'id': req.body.campaignId } })
  .then(retData => { 
      return res.status(200).json({ responseCode: 200, responseDetails: 'Campaign Completed', data: retData});
    })
    .catch(reason => {
      return res.status(200).json({ responseCode: 420, responseDetails: 'Something went wrong!.', "error": reason});
    });
    
});

module.exports = router;