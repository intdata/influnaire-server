var express = require('express');
var router = express.Router();
var moment = require('moment');

const auth = require("../middleware/auth");

db = require("./../models/associations");
Sequelize = require("sequelize");
var currencyObj = Currency(sequelize, Sequelize);

/**
 * Method to get all site settings
 */
router.post('/getSetting', auth, passport.authenticate('jwt', { session: false }),  async function(req, res, next) {

	let defaultCurrency = await getDeaultCurrency({ is_default: '1' });
  return db.siteSettings.findAll()
    .then(data => { 
      return res.status(200).json({ responseCode: 200, responseDetails: 'Get platform data.', data: data ,defaultCurrency:defaultCurrency});
    })
    .catch(reason => {
      return res.status(200).json({ responseCode: 420, responseDetails: 'Something went wrong!.', "error": reason});
    });
});

/**
 * Site Setting List
 */
router.get('/settingList',function(req, res, next) {
  return db.siteSettings.findAll()
    .then(data => { 
      return res.status(200).json({ responseCode: 200, responseDetails: 'Get platform data.', data: data});
    })
    .catch(reason => {
      return res.status(200).json({ responseCode: 420, responseDetails: 'Something went wrong!.', "error": reason});
    });
})

/**
 * Method to get default currency details
 * @param {*} obj 
 */
const getDeaultCurrency = async obj => {
  return await currencyObj.findOne({
    where: obj,
  });
};



module.exports = router;
