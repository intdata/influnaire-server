var express = require('express');
var router = express.Router();
var moment = require('moment');
var auth = require("../middleware/auth");

/** Create Sequalize object  */
Sequelize = require("sequelize");
Op = Sequelize.Op;

db = require("./../models/associations");
const config = require("../config/default.json");
const campaignImagePath = config.adminAssetpath + "uploads/campaign/";

const brandOwnerImagePath = config.adminBaseUrl + "uploads/brand_owner/";
const influencerImagePath = config.adminBaseUrl + "uploads/influencer/";
const messageFilePath = config.adminBaseUrl + "uploads/message_files/";
const allPaths={
      brandOwnerImagePath: brandOwnerImagePath,
      influencerImagePath:influencerImagePath,
      messageFilePath: messageFilePath
    }

/**
 * Method to format date YYYY-MM-DD
 * @param {*} date 
 */
function formatDate(date) {
  var d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();

  if (month.length < 2) 
      month = '0' + month;
  if (day.length < 2) 
      day = '0' + day;

  return [year, month, day].join('-');
}

/**
 * Method to get all jobs (ongoing and past) 
 * to influencer from brand owner
 */
router.post('/get-all-jobs', auth, passport.authenticate('jwt', { session: false }),  async function(req, res) {

  let { id, type } = req.body;
  if(!req.userInfo){
    return res.status(200).json({
        responseCode: 420,
        responseDetails: "Something went wrong!",
    });
  }

  var filter = {};
  var filterCampaign = {};
  if (!id) {
      id = req.userInfo.id;
  }
  filter['influencer_id'] = id;

  let platformFilter = {};
  let monthYear = '';

  if(req.body.platformId){
    platformFilter.platform_id = req.body.platformId
  }
  
  if(req.body.monthYear){
    monthYear = req.body.monthYear;
  }
  if(type && type =='current'){
    filter['status'] = ['CONFIRM', 'CONTENT_SUBMITTED', 'SUBMITTED', 'CONTENT_APPROVED'];
    filterCampaign['status'] = ['ANNOUNCED', 'IN_PROGRESS']
    
  }
  if(type && type =='past'){
    filter['status'] = 'PAID';
    filterCampaign['status'] = 'PUBLISHED'
  }
  if(monthYear){
    let date = new Date(monthYear);
    let firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
    let lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
    
    filterCampaign[Op.and] = [
      {
        start_date: {
          [Op.gte]: new Date(firstDay),
          [Op.lt]: new Date(lastDay)
        }
      }
    ]
  }
  console.log(filter);

  db.campaignAssign.findAll({
   
    where: filter,
      include:[{ 
          model : db.campaign,
          where: filterCampaign,
          include:[{ 
            model : db.brandowner
          },{ 
            model : db.brand
          },{ 
            model : db.CampaignPlatformMap,
            where:platformFilter
          }]
      }]
    })
    .then(jobs=>{
      return res.status(200).json({ responseCode: 200, responseDetails: 'Get campaign data.', Jobs: jobs, allPaths:allPaths});
    })
    .catch(reason => {
      return res.status(420).json({responseCode: 420, responseDetails: 'Something went wrong!', error: reason});
    });
});

/**
 * Get Job By Id
*/
router.post('/get-job-by-id', auth, passport.authenticate('jwt', { session: false }),  async  function(req, res, next) {
  if(!req.userInfo){
    return res.status(200).json({
        responseCode: 420,
        responseDetails: "Something went wrong!",
    });
  }

  if (!req.body.job_id) {
      return res.status(200).json({
          responseCode: "201",
          responseDetails: "Parameter missing.",
      });
  }
  influncer_id = req.userInfo.id
  return db.campaign.findOne({
      where:{id:req.body.job_id},
      order: [ [ 'id', 'DESC' ]],
      include : [{model : db.campaignFiles},{model:db.brandowner},{model: db.CampaignPlatformMap, include: [{model : db.platform,  attributes:['name', 'class_name']}]},{model:db.campaignAssign, include :[{model:db.influencer, where:{id:influncer_id}}]}]
    })
    .then(data => { 
      return res.status(200).json({ responseCode: 200, responseDetails: 'Get Past campaign data.', data: data, allPaths: allPaths});
    })
    .catch(reason => {
      return res.status(200).json({ responseCode: 420, responseDetails: 'Something went wrong!.', "error": reason});
    });
});
module.exports = router;