var express = require('express');
var router = express.Router();
var moment = require('moment');
var im = require('imagemagick');
var fs = require('fs');

const auth = require("../middleware/auth");
const upload = require("../middleware/upload");
const config = require("../config/default.json");

db = require("./../models/associations");
Sequelize = require("sequelize");
Op = Sequelize.Op;

const influencerImagePath = config.adminBaseUrl + "uploads/influencer/";
const pdfHttpPath = config.adminBaseUrl + "uploads/pdf/";
const pdfPath = config.adminAssetpath + "uploads/pdf/";
const accessurl = config.frontEndBaseUrl;

var pdf = require("pdf-creator-node");
var fs = require('fs');
var html = fs.readFileSync('./views/compare-pdf-template.html', 'utf8');

//var html = fs.readFileSync('htmlData', 'utf8');
var options = {
  format: "A3",
  orientation: "portrait",
  border: "10mm"
};



/**
 *Delete Single Brand
 */
router.post('/create', auth, passport.authenticate('jwt', { session: false }),  async function(req, res, next) {
  console.log(accessurl)
  console.log(influencerImagePath)
  if(!req.userInfo){
    return res.status(200).json({
        responseCode: "420",
        responseDetails: "Something went wrong!",
    });
  }
  let brand_owner_id = req.userInfo.id;
  var uploadedFilePath = pdfPath+"opt"+brand_owner_id+".pdf";
  var uploadedFileUrl = pdfHttpPath+"opt"+brand_owner_id+".pdf";

  db.influencer.findAll({where: { id: {[Op.in]: req.body.ids }},
    attributes: ['first_name', 'last_name', 'country', 'nationality', 'picture'],
    include: [
      { model: db.influencerPlatformMap, include:[{model: db.platform, attributes: ['name', 'icon', 'class_name']}]}, { model: db.campaignAssign},
      ]
    
  }).then(data => { 

    
    var document = {
      html: html,
      data: {
        selectedInfluncersData: data,
        accessurl:accessurl,
        influencerImagePath:influencerImagePath
      },
      path: uploadedFilePath
    };
    pdf.create(document, options)
    return res.status(200).json({ responseCode: 200, responseDetails: 'Get selected influncers.', data: uploadedFileUrl});
    
  })
  .catch(reason => {
    return res.status(200).json({ responseCode: 420, responseDetails: 'Something went wrong!.', "error": reason});
  });
});


module.exports = router;
