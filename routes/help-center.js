var express = require('express');
var router = express.Router();
var moment = require('moment');

const auth = require("../middleware/auth");

db = require("./../models/associations");

/**
 * Method to get all help list
 */
router.get('/get-questtion-answer', function(req, res, next) {
  let lang = req.query.lang;
  let type = req.query.type;

  if(!lang || lang == ''){
    lang = 'en';
  }

  return db.faq.findAll({
    where: { status: 'Active' },
    include: [{
      model: db.faqLang,
      where: { lang_code: lang },
      as: 'question_answer'
    }],
  })
  .then(faq => { 
    return res.status(200).json({ responseCode: 200, responseDetails: 'All active help list.', faq: faq});
  })
  .catch(reason => {
    return res.status(200).json({ responseCode: 420, responseDetails: 'Something went wrong!.', "error": reason});
  });
});

/**
 * Method to process talk to us data
 */
router.post('/submit-talk-to-us', auth, function(req, res, next) {
  let { name, email, subject, message, type, id } = req.body;

  if(!req.userInfo){
    return res.status(200).json({
        responseCode: "420",
        responseDetails: "Something went wrong!",
    });
  }

  if (!id) {
      id = req.userInfo.id;
      // return res.status(200).json({
      //     responseCode: "201",
      //     responseDetails: "Parameter missing.",
      // });
  }
  
  if (!name || !email || !subject || !message || !type) {
      return res.status(200).json({
          responseCode: "201",
          responseDetails: "Parameter missing.",
      });
  }

  let templateName = '';
  if(type == 'influencer'){
    templateName = 'influencer_talk_to_us';
  } else{
    templateName = 'brand_owner_talk_to_us';
  }

  let submitData = {
    name: name,
    email: email,
    subject: subject,
    message: message,
    creator_type: type == 'influencer' ? 'Influencer' : 'BrandOwner',
    created_by: id,
  }
  //console.log(submitData);
  
  // insert contact us data in db
  db.contactUs
    .create(submitData)
    .then((created) => {
      // console.log(user.get({
      //   plain: true
      // }))
      if(created){

        let keyValues = { 
          NAME: name,
          EMAIL: email,
          SUBJECT: subject,
          MESSAGE: message
        }

        db.siteSettings
        .findOne({where: {setting_label: 'info_email'}})
        .then((settings) =>{
          let mailTo = settings.setting_value;

          // send sign up mail
          db.emailTemplate.sendMailWithTemplate(templateName, mailTo, keyValues);
  
          return res.status(200).json({ responseCode: 200, responseDetails: 'Mail sent to admin successfully.'});
        }).catch(error => {
          // Ooops, do some error-handling
          console.log("Something went worng!");
          console.log(error);
          return res.json({responseCode: 420, responseDetails: 'Recipients email not found.', error: error});
        });

      }
    }).catch(error => {
      // Ooops, do some error-handling
      console.log("Something went worng!");
      console.log(error);
      return res.json({responseCode: 420, responseDetails: 'Something went wrong!', error: error});
    });
});


module.exports = router;
