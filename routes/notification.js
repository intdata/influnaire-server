var express = require('express');
var router = express.Router();

const auth = require("../middleware/auth");

db = require("./../models/associations");

/**
 * Method to get all notification
 * by receiver type
 */
router.post('/get-all-notifications', auth, passport.authenticate('jwt', { session: false }),  async function(req, res) {

  let { receiverType, receiverId } = req.body;

  if (!receiverType) {
    return res.status(200).json({
        responseCode: "201",
        responseDetails: "Parameter missing.",
    });
  }

  if (receiverType != 'Influencer' && receiverType != 'BrandOwner') {
    return res.status(200).json({
        responseCode: "201",
        responseDetails: "Wrong parameter value.",
    });
  }
  
  if(!req.userInfo){
    return res.status(200).json({
        responseCode: 420,
        responseDetails: "Something went wrong!",
    });
  }

  if (!receiverId) {
    receiverId = req.userInfo.id;
  }

  console.log(receiverType, receiverId);

  if(receiverType == 'Influencer'){
    db.notification.findAll({
      where: { 
        receiver_type: receiverType , 
        receiver_id: receiverId,
        deleted_by_receiver: '0',
        status: '1'
      },
      include:[{ 
        model : db.brandowner,
        attributes: ['fname', 'lname', 'company_name']
      }]
    })
    .then((notifications)=>{
        return res.status(200).json({ responseCode: 200, responseDetails: 'Get notification list.', notifications: notifications});
    })
    .catch(reason => {
      return res.status(420).json({responseCode: 420, responseDetails: 'Something went wrong!', error: reason});
    });
  } else{
    db.notification.findAll({
      where: { 
        receiver_type: receiverType , 
        receiver_id: receiverId,
        deleted_by_receiver: '0',
        status: '1'
      },
      include:[{ 
        model : db.influencer,
        attributes: ['first_name', 'last_name']
      }]
    })
    .then((notifications)=>{
        return res.status(200).json({ responseCode: 200, responseDetails: 'Get notification list.', notifications: notifications});
    })
    .catch(reason => {
      return res.status(420).json({responseCode: 420, responseDetails: 'Something went wrong!', error: reason});
    });
  }

  /**
   * Method to delete selected notifications
   * by receiver type
   */
  router.post('/delete-notification', auth, passport.authenticate('jwt', { session: false }),  async function(req, res) {
    let { userType, notificationIds } = req.body;

    if (!userType) {
      return res.status(200).json({
          responseCode: "201",
          responseDetails: "Parameter missing.",
      });
    }
  
    if (userType != 'Sender' && userType != 'Receiver') {
      return res.status(200).json({
          responseCode: "201",
          responseDetails: "Wrong parameter value.",
      });
    }
    
    if(!req.userInfo){
      return res.status(200).json({
          responseCode: 420,
          responseDetails: "Something went wrong!",
      });
    }
  
    if (!receiverId) {
      receiverId = req.userInfo.id;
    }
    
    let updateData = {};
    if(userType == 'Receiver'){
      updateData = {
        deleted_by_receiver : '1'
      }
    } else{
      updateData = {
        deleted_by_sender : '1'
      }
    }
    db.notification
      .update(updateData, {where: {id:notificationIds}})
      .then(result => {
        if(result){
          return res.status(200).json({responseCode: 200, responseDetails: 'Notification deleted successfully.'});
        } else{
          return res.status(200).json({responseCode: 420, responseDetails: 'Something went wrong!'});
        }
      }).catch(error => {
        return res.status(200).json({responseCode: 420, responseDetails: 'Something went wrong!', error: error});
      })

  });

});

module.exports = router;
