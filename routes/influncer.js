var express = require('express');
var router = express.Router();
var auth = require("../middleware/auth");

/** Create Sequalize object  */
Sequelize = require("sequelize");
Op = Sequelize.Op;

db = require("./../models/associations");

const config = require("../config/default.json");
const influencerImagePath = config.adminBaseUrl + "uploads/influencer/";


/**---------Get Job List -------*/
router.post('/getAll', auth, passport.authenticate('jwt', { session: false }),  async function(req, res, next) {

  var where = ''

  // if(req.body.country && req.body.country !="null"){
  //   where += ' AND country = "'+ req.body.country +'"'
  // }
  // if(req.body.age && req.body.age !="null"){
  //   where += ' AND (ABS(DATEDIFF(influencer.dob, NOW()) / 365.25)) <'+ req.body.age 
  // }
  // if(req.body.category && req.body.category !="null"){
  //   where += ' AND industry = "'+ req.body.category +'"'
  // }
  // if(req.body.email && req.body.email !="null"){
  //   where += ' AND email = "'+ req.body.email +'"'
  // }
  // if(req.body.gender && req.body.gender !="null"){
  //   where += ' AND gender = "'+ req.body.gender +'"'
  // }
  // if(req.body.nationality && req.body.nationality !="null"){
  //   where += ' AND nationality = "'+ req.body.nationality +'"'
  // }
  // if(req.body.platform_targeted && req.body.platform_targeted !="null"){
  //   where += ' AND platform_id = "'+ req.body.platform_targeted +'"'
  // }

  // sequelize.query("SELECT * FROM `influencer` WHERE 1 "+where, { type: sequelize.QueryTypes.SELECT})
  // .then(data => { 
  //     return res.status(200).json({ responseCode: 200, responseDetails: 'Get campaign data.', data: data, influencerImagePath:influencerImagePath});
  //   })
  //   .catch(reason => {
  //     return res.status(200).json({ responseCode: 420, responseDetails: 'Something went wrong!.', "error": reason});
  //   });
  var cond={}
  var platformCond = {}
  var required =false;
  if(req.body.country && req.body.country !="null"){
    cond.country = req.body.country;
  }
  if(req.body.category && req.body.category !="null"){
    cond.industry = req.body.category;
  }
  if(req.body.email && req.body.email !="null"){
    cond[Op.or] =[
      { email: req.body.email
      }, 
      {
        first_name: {[Op.substring]:req.body.email}
      }, 
      {
        last_name: {[Op.substring]:req.body.email}
      }
   ]
  }
  if(req.body.gender && req.body.gender !="null"){
    cond.gender = req.body.gender;
  }
  if(req.body.nationality && req.body.nationality !="null"){
    cond.nationality = req.body.nationality;
  }
  if(req.body.age && req.body.age !="null"){
    let age = req.body.age
    let range = {1:{'st':1,'end':10}, 2:{'st':11,'end':20}, 3:{'st':21,'end':30}, 4:{'st':31,'end':40}, 5:{'st':41,'end':50}, 6:{'st':61,'end':70}}

    if(age>=1 && age<=6){
      cond[Op.and] = [
        Sequelize.literal('(ABS(DATEDIFF(Influencer.dob, NOW()) / 365.25)) >='+ range[age].st +' AND (ABS(DATEDIFF(Influencer.dob, NOW()) / 365.25)) <='+ range[age].end)
      ]
    }else{
      cond[Op.and] = [Sequelize.literal('(ABS(DATEDIFF(Influencer.dob, NOW()) / 365.25)) >70')]
    }
    
  }
  if(req.body.platform_targeted && req.body.platform_targeted !="null"){
    platformCond.platform_id =  req.body.platform_targeted;
    required = true
  }
//console.log(cond)

  db.influencer.findAll({where: cond, include: [{model:db.influencerPlatformMap, include:[{model:db.platform, attributes:['name', 'class_name']}],  where: platformCond, required: required}]})
  .then(data => { 
    return res.status(200).json({ responseCode: 200, responseDetails: 'Get selected influncers.', data: data, influencerImagePath: influencerImagePath});
  })
  .catch(reason => {
    return res.status(200).json({ responseCode: 420, responseDetails: 'Something went wrong!.', "error": reason});
  });

});

router.post('/getSelected', auth, passport.authenticate('jwt', { session: false }),  async function(req, res, next) {
  
  db.influencer.findAll({where: { id: {[Op.in]: req.body.ids }}
  }).then(data => { 
    return res.status(200).json({ responseCode: 200, responseDetails: 'Get selected influncers.', data: data, influencerImagePath: influencerImagePath});
  })
  .catch(reason => {
    return res.status(200).json({ responseCode: 420, responseDetails: 'Something went wrong!.', "error": reason});
  });
});

/**
 * Get single influncer data by influncer id
 */

router.post('/get-influncer-by-id', auth, passport.authenticate('jwt', { session: false }),  async function(req, res, next) {
  
  if(!req.userInfo){
    return res.status(200).json({
        responseCode: "420",
        responseDetails: "Something went wrong!",
    });
  }

  db.influencer.findOne({
    where: { id: req.body.influncerId},
    include:[{model:db.influencerPlatformMap, include:[{model:db.platform, attributes:['name', 'class_name']}]}],
  }).then(data => { 
    return res.status(200).json({ responseCode: 200, responseDetails: 'Get selected influncers.', data: data, influencerImagePath: influencerImagePath});
  })
  .catch(reason => {
    return res.status(200).json({ responseCode: 420, responseDetails: 'Something went wrong!.', "error": reason});
  });
});

/**
 * Get Seleted influncer with related data
 */

router.post('/get-selected-with-relations', auth, passport.authenticate('jwt', { session: false }),  async function(req, res, next) {
  
  db.influencer.findAll({where: { id: {[Op.in]: req.body.ids }},
    attributes: ['first_name', 'last_name', 'country', 'nationality', 'picture'],
    include: [
        { model: db.influencerPlatformMap, include:[{model: db.platform, attributes: ['name', 'icon', 'class_name']}]}, { model: db.campaignAssign},
      ]
    
  }).then(data => { 
    return res.status(200).json({ responseCode: 200, responseDetails: 'Get selected influncers.', data: data, influencerImagePath: influencerImagePath});
  })
  .catch(reason => {
    return res.status(200).json({ responseCode: 420, responseDetails: 'Something went wrong!.', "error": reason});
  });
});

/**
 * Get proposal data
 */
router.post('/getPropoaslData', auth, passport.authenticate('jwt', { session: false }),  async function(req, res, next) {
  //brandowner_id =  req.userInfo.id;
  campaign_id = req.body.campaign_id
  invitation_id = req.body.invitation_id
  db.proposal.findOne({ where: { campaign_id:campaign_id, invitation_id:invitation_id}, include: [{model:db.proposalPlatformMap}]
  }).then(data => { 
    return res.status(200).json({ responseCode: 200, responseDetails: 'Get selected influncers.', data: data});
  })
  .catch(reason => {
    return res.status(200).json({ responseCode: 420, responseDetails: 'Something went wrong!.', "error": reason});
  });
});

/**
 * Get total influncer of a brand owner
 */
router.post('/getTotalCount', auth, passport.authenticate('jwt', { session: false }),  async function(req, res, next) {
  if(!req.userInfo){
    return res.status(200).json({
        responseCode: "420",
        responseDetails: "Something went wrong!",
    });
  }
  var date = new Date()
  var year = date.getFullYear();
  var month = date.getMonth();
  month = month + 1;
  var m = (month<10)?'0'+month : month;
  var sDate = '"'+year+'-'+m+'-'+01+'"';
  var eDate = '"'+year+'-'+m+'-'+31+'"';

  brandowner_id =  req.userInfo.id;
  db.campaignAssign.findAndCountAll({ where:{assign_date: { [Op.gte]: new Date(sDate),
    [Op.lte]: new Date(eDate)}}, group: ['influencer_id'], include: [{model:db.campaign, where: {'brand_owner_id':brandowner_id}}]
  }).then(data => { 
    return res.status(200).json({ responseCode: 200, responseDetails: 'Get selected influncers.', data: data});
  })
  .catch(reason => {
    return res.status(200).json({ responseCode: 420, responseDetails: 'Something went wrong!.', "error": reason});
  });
})

/**
 * Get influncer dashboard data
 */
router.post('/dashboard', auth, passport.authenticate('jwt', { session: false }),  async function(req, res, next) {
  
  if(!req.userInfo){
    return res.status(200).json({
        responseCode: "420",
        responseDetails: "Something went wrong!",
    });
  }
  var influencer_id = req.userInfo.id;

  var filter ={}
  var assignFilter ={'influencer_id':influencer_id}
  assignFilter['status'] = ['SUBMITTED', 'CONFIRM', 'CONTENT_SUBMITTED', 'CONTENT_APPROVED', 'PAID'];
  if(req.body.monthYear){
    monthYear = req.body.monthYear;
    var date = new Date(monthYear);
    var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
    var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
  }else{
   var date = new Date()
   var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
   var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
  }
    filter[Op.and] = [
      {
        start_date: {
          [Op.gte]: new Date(firstDay),
          [Op.lt]: new Date(lastDay)
        }
      }
    ]
    assignFilter[Op.and] = [
      {
        assign_date: {
          [Op.gte]: new Date(firstDay),
          [Op.lt]: new Date(lastDay)
        }
      }
    ]
  
  
  var campaign_assign_status = await db.campaignAssign.findAll({
    where:assignFilter,
    attributes:['status', [sequelize.fn('count', sequelize.col('id')), 'campaignCnt']],
    group: ['status']
  });
  var campaign_status = await db.campaign.findAll({
    where:filter,
    include:[{model:db.campaignAssign, where:{influencer_id:influencer_id}, attributes:[]}],
    attributes:['status', [sequelize.fn('count', sequelize.col('campaign_name')), 'campaignCnt']],
    group: ['status']
  })
  return res.status(200).json({ responseCode: 200, responseDetails: 'Get Dashboard Data Count.', data: {'campaign_assign_status':campaign_assign_status, 'campaign_status':campaign_status}});
})

/**
 * Save Influncer for a brandOwner
 */
router.post('/save-influncer-brandowner', auth, passport.authenticate('jwt', { session: false }),  async function(req, res, next) {
  if(!req.userInfo){
    return res.status(200).json({
        responseCode: "420",
        responseDetails: "Something went wrong!",
    });
  }
  var data = {}
  data.brand_owner_id = req.userInfo.id;
  data.influencer_id = req.body.influncer_id;
  data.created_at = sequelize.fn('NOW');
  //console.log(data);
  
  return db.SavedInfluncer.create(data)
    .then(retData => { 
      return res.status(200).json({ responseCode: 200, responseDetails: 'Influncer Saved', data: retData});
    })
    .catch(reason => {
      return res.status(200).json({ responseCode: 420, responseDetails: 'Something went wrong!.', "error": reason});
    });
})

/**
 * Get the Id List Of saved Influncers
 */
router.post('/get-saved-influncer-ids', auth, passport.authenticate('jwt', { session: false }),  async function(req, res, next) {
  if(!req.userInfo){
    return res.status(200).json({
        responseCode: "420",
        responseDetails: "Something went wrong!",
    });
  }
  var brand_owner_id = req.userInfo.id;
  
  return db.SavedInfluncer.findAll({where:{brand_owner_id:brand_owner_id}, attributes: ['influencer_id']})
    .then(retData => { 
      return res.status(200).json({ responseCode: 200, responseDetails: 'Influncer Ids', data: retData});
    })
    .catch(reason => {
      return res.status(200).json({ responseCode: 420, responseDetails: 'Something went wrong!.', "error": reason});
    });
})

/**
 * Remove saved Influncers
 */
router.post('/remove-saved-influncers', auth, passport.authenticate('jwt', { session: false }),  async function(req, res, next) {
  if(!req.userInfo){
    return res.status(200).json({
        responseCode: "420",
        responseDetails: "Something went wrong!",
    });
  }
  var savedInfluncerId = req.body.savedInfluncerId;
  //console.log(savedInfluncerId);
  
  return db.SavedInfluncer.destroy({where:{influencer_id:savedInfluncerId}})
    .then(retData => { 
      return res.status(200).json({ responseCode: 200, responseDetails: 'Influncer Ids', data: retData});
    })
    .catch(reason => {
      return res.status(200).json({ responseCode: 420, responseDetails: 'Something went wrong!.', "error": reason});
    });
})

/**
 * Get All Influncers
 */

router.post('/get-saved-influncers', auth, passport.authenticate('jwt', { session: false }),  async function(req, res, next) {
  if(!req.userInfo){
    return res.status(200).json({
        responseCode: "420",
        responseDetails: "Something went wrong!",
    });
  }
  var brand_owner_id = req.userInfo.id;
  
  return db.influencer.findAll({include: [{model:db.influencerPlatformMap, include:[{model:db.platform, attributes:['name', 'class_name']}]}, { model: db.SavedInfluncer, where: {'brand_owner_id':brand_owner_id} }]})
    .then(retData => { 
      return res.status(200).json({ responseCode: 200, responseDetails: 'Saved Influncers', data: retData, influencerImagePath:influencerImagePath});
    })
    .catch(reason => {
      return res.status(200).json({ responseCode: 420, responseDetails: 'Something went wrong!.', "error": reason});
    });
})


/**
 * Get Influncers of a campaign
 */

router.post('/get-influncers-by-campaign', auth, passport.authenticate('jwt', { session: false }),  async function(req, res, next) {
  if(!req.userInfo){
    return res.status(200).json({
        responseCode: "420",
        responseDetails: "Something went wrong!",
    });
  }
  var campaignId = req.body.campaignId;
  
  return db.influencer.findAll({attributes: ['id','first_name', 'last_name', 'picture'], include: [{ model: db.campaignAssign, where: {'campaign_id':campaignId}}]})
    .then(retData => { 
      return res.status(200).json({ responseCode: 200, responseDetails: 'Saved Influncers', data: retData, influencerImagePath:influencerImagePath});
    })
    .catch(reason => {
      return res.status(200).json({ responseCode: 420, responseDetails: 'Something went wrong!.', "error": reason});
    });
})

module.exports = router;