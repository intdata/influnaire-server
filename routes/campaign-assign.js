var express = require('express');
var router = express.Router();

/** Create Sequalize object  */
Sequelize = require("sequelize");
Op = Sequelize.Op;
const auth = require("../middleware/auth");
db = require("./../models/associations");
const config = require("../config/default.json");
sharedService = require("./sharedService")
const campaignImagePath = config.adminAssetpath + "uploads/campaign/";
const messageFilePath = config.adminAssetpath + "uploads/message_files/";
var im = require('imagemagick');
var fs = require('fs');
const upload = require("../middleware/upload");
var qs = require('querystring');
const brandOwnerImagePath = config.adminBaseUrl + "uploads/brand_owner/";
const filePath = config.adminBaseUrl + "uploads/message_files/";
const influencerImagePath = config.adminBaseUrl + "uploads/influencer/";

/**
 * Get latest message for brand owner panel
*/


/**
 * Get Campaign Assign status
 */
router.post('/status', auth, passport.authenticate('jwt', { session: false }),  async  function(req, res, next) {

  if(!req.userInfo){
    return res.status(200).json({
        responseCode: "420",
        responseDetails: "Something went wrong!",
    });
  }
  return db.campaignAssign.findOne({
      where:{influencer_id: req.body.influncer, campaign_id: req.body.campaign},
      attributes: ['status'] 
    })
    .then(data => { 
      return res.status(200).json({ responseCode: 200, responseDetails: 'Get Message data.', data: data.status});
    })
    .catch(reason => {
      return res.status(200).json({ responseCode: 420, responseDetails: 'Something went wrong!.', "error": reason});
    });
})

/**
 * Change Campaign Assign status
 */
router.post('/change-status', auth, passport.authenticate('jwt', { session: false }),  async  function(req, res, next) {

  if(!req.userInfo){
    return res.status(200).json({
        responseCode: "420",
        responseDetails: "Something went wrong!",
    });
  }
  var status = req.body.status
  var campaign_id = req.body.campaign_id
  var influncer_id = req.body.influncer_id
  //Create object for history table update
  var history_update={}
  history_update.campaign_id = campaign_id
  history_update.influencer_id = influncer_id
  history_update.brand_owner_id = req.userInfo.id
  history_update.action = 'Content Approved';
  history_update.taken_by = 'BrandOwner'
  history_update.created_at = sequelize.fn('NOW')
  
  //Notification Update data
  let notificationTemplateAlias = 'content_approved';
  let notificationTemplate = await getNotificationTemplate({ alias: notificationTemplateAlias });
  //console.log(notificationTemplate)
  var notification_update = {}
  notification_update.title	=  (notificationTemplate && notificationTemplate.length  && notificationTemplate.title)?notificationTemplate.title:''
  notification_update.content	=  (notificationTemplate && notificationTemplate.length  && notificationTemplate.content)?notificationTemplate.content:''
  notification_update.sender_id = req.userInfo.id
  notification_update.receiver_id = influncer_id
  notification_update.campaign_id = campaign_id
  notification_update.receiver_type = 'Influencer'
  notification_update.created_at = sequelize.fn('NOW')
  
  return db.campaignAssign.update({'status': status}, {where:{'campaign_id':campaign_id, 'influencer_id': influncer_id}})
    .then(data => { 
      db.campaignHistory.create(history_update);
      db.notification.create(notification_update);
      return res.status(200).json({ responseCode: 200, responseDetails: 'Get Message data.', data: status});
    })
    .catch(reason => {
      return res.status(200).json({ responseCode: 420, responseDetails: 'Something went wrong!.', "error": reason});
    });
})
/**
 * Method to get notification template by given condition
 * @param {*} obj 
 */
const getNotificationTemplate = async obj => {
  return await db.notificationTemplate.findOne({
    where: obj,
  });
};

/**
 * Save Review and rating for campaign
 */

router.post('/save-rating', auth, passport.authenticate('jwt', { session: false }),  async  function(req, res, next) {

  if(!req.userInfo){
    return res.status(200).json({
        responseCode: "420",
        responseDetails: "Something went wrong!",
    });
  }
  if(req.body.data){
    var reqData = req.body.data
    var assignIds = reqData['assignIds']
    for(let c = 0; c < assignIds.length; c++){
      var updateData ={};
      id = assignIds[c];
      updateData['rating'] = reqData['rating-'+id]
      updateData['review'] = reqData['review-'+id]
      await db.campaignAssign.update(updateData, {where:{'id':id}})
    }
    return res.status(200).json({ responseCode: 200, responseDetails: 'Rating & Review Updated'});
  }else{
    return res.status(200).json({
      responseCode: "420",
      responseDetails: "Something went wrong!",
    });
  }
  
})

/**
 * Get the campaign wise social media data for analysis
 */

router.post('/social-media-analysis', auth, passport.authenticate('jwt', { session: false }),  async  function(req, res, next) {

  if(!req.userInfo){
    return res.status(200).json({
        responseCode: "420",
        responseDetails: "Something went wrong!",
    });
  }
  var campaignId = req.body.campaignId
  //console.log("============================================>>>>>"+campaignId)
  return db.CampaignInfluencerPlatformMap.findAll({
    where: {'campaign_id': campaignId},
    include:[
      { attributes: ['first_name', 'last_name', 'picture'], model: db.influencer },
      { attributes: ['name', 'icon', 'class_name'], model: db.platform },  
    ]
  }).then(data => {
    return res.status(200).json({ responseCode: 200, responseDetails: 'Get campaign social data.', data: data});
  })
  .catch(reason => {
    return res.status(200).json({ responseCode: 420, responseDetails: 'Something went wrong!.', "error": reason });
  });
  // if(req.body.data){
  //   var reqData = req.body.data
  //   var assignIds = reqData['assignIds']
  //   for(let c = 0; c < assignIds.length; c++){
  //     var updateData ={};
  //     id = assignIds[c];
  //     updateData['rating'] = reqData['rating-'+id]
  //     updateData['review'] = reqData['review-'+id]
  //     await db.campaignAssign.update(updateData, {where:{'id':id}})
  //   }
  //   return res.status(200).json({ responseCode: 200, responseDetails: 'Rating & Review Updated'});
  // }else{
  //   return res.status(200).json({
  //     responseCode: "420",
  //     responseDetails: "Something went wrong!",
  //   });
  // }
  
})

module.exports = router;