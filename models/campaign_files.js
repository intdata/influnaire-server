'use strict';
module.exports = (sequelize, DataTypes) => {
  const campaignFiles = sequelize.define('campaignFiles', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    campaign_id: DataTypes.INTEGER,
    file_extension: DataTypes.STRING,
    name: DataTypes.STRING,
   
  }, {
    // define the table's name
    tableName: 'campaign_files',
    // don't add the timestamp attributes (updatedAt, createdAt)
    timestamps: false,
  });
  campaignFiles.associate = function(models) {
    // associations can be defined here
  };
  return campaignFiles;
};