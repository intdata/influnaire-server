'use strict';
module.exports = (sequelize, DataTypes) => {
  const influencerPlatformMap = sequelize.define('influencerPlatformMap', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    influencer_id: DataTypes.INTEGER,
    platform_id: DataTypes.INTEGER,
    readymade_content_price: DataTypes.FLOAT,
    generate_content_price: DataTypes.FLOAT,
    social_media_id: DataTypes.STRING,
    social_media_token: DataTypes.STRING,
    followers: DataTypes.STRING,
    total_like: DataTypes.STRING,
    save_posts: DataTypes.STRING,
    total_post: DataTypes.INTEGER,
    imptession_ratio: DataTypes.STRING,
    engagement_ratio: DataTypes.STRING,
    total_like: DataTypes.STRING,
    total_comments: DataTypes.STRING,
    followers: DataTypes.STRING,
    auth: DataTypes.TEXT,
    createdAt: {type: Sequelize.DATE, field: 'created_at'},
    updatedAt: {type: Sequelize.DATE, field: 'updated_at'},
  }, {
    // define the table's name
    tableName: 'influencer_platform_map',
    // don't add the timestamp attributes (updatedAt, createdAt)
    timestamps: true,
  });
  influencerPlatformMap.associate = function(models) {
    // associations can be defined here
  };
  return influencerPlatformMap;
};