'use strict';
module.exports = (sequelize, DataTypes) => {
  const CmsLang = sequelize.define('CmsLang', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    page_id: DataTypes.INTEGER,
    lang_code: DataTypes.STRING,
    title: DataTypes.STRING,
    description: DataTypes.STRING,
    short_description: DataTypes.STRING,
  }, {
    // define the table's name
    tableName: 'cms_lang',
    // don't add the timestamp attributes (updatedAt, createdAt)
    timestamps: false,
  });
  CmsLang.associate = function(models) {
    // associations can be defined here
  };
  return CmsLang;
};