'use strict';
module.exports = (sequelize, DataTypes) => {
  const SavedInfluncer = sequelize.define('SavedInfluncer', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    brand_owner_id: DataTypes.INTEGER,
    influencer_id: DataTypes.INTEGER,
    created_at: {type: Sequelize.DATE, field: 'created_at'}
  }, {
    // define the table's name
    tableName: 'saved_influencers',
    // don't add the timestamp attributes (updatedAt, createdAt)
    timestamps: false,
  });
  SavedInfluncer.associate = function(models) {
    // associations can be defined here
  };
  return SavedInfluncer;
};