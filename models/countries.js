'use strict';
module.exports = (sequelize, DataTypes) => {
  const Countries = sequelize.define('Countries', {
    countrycode: {
      type: DataTypes.STRING,
      primaryKey: true,
    },
    countryname: DataTypes.STRING,
    code: DataTypes.STRING,
    status: DataTypes.INTEGER
  }, {
    // define the table's name
    tableName: 'country',
    // don't add the timestamp attributes (updatedAt, createdAt)
    timestamps: false,
  });
  Countries.associate = function(models) {
    // associations can be defined here
  };
  return Countries;
};