'use strict';
module.exports = (sequelize, DataTypes) => {
  const Industries = sequelize.define('Industries', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    name: DataTypes.STRING,
    status: DataTypes.INTEGER,
    createdAt: {type: Sequelize.DATE, field: 'created_at'},
    updatedAt: {type: Sequelize.DATE, field: 'updated_at'},
  }, {
    // define the table's name
    tableName: 'industries',
    //freezeTableName: true,
    // don't add the timestamp attributes (updatedAt, createdAt)
    timestamps: true,
  });
  Industries.associate = function(models) {
    // associations can be defined here
    //models.Industries.hasMany(models.BrandOwner);
    // Industries.hasMany(models.BrandOwner, {
    //   foreignKey: 'industry', // this should match
    //   // sourceKey: 'id',
    //   // as: 'brnd'
    // })
  };
  return Industries;
};