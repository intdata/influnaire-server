'use strict';
module.exports = (sequelize, DataTypes) => {
  const CampaignPlatformMap = sequelize.define('CampaignPlatformMap', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    campaign_id: DataTypes.STRING,
    platform_id: DataTypes.INTEGER,
    created_at: {
      type: 'TIMESTAMP',
      defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
      allowNull: false
    },
    updated_at: {
      type: 'TIMESTAMP',
      defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
      allowNull: false
    }
 
  }, {
    // define the table's name
    tableName: 'campaign_platform_map',
    // don't add the timestamp attributes (updatedAt, createdAt)
    timestamps: false,
  });

  return CampaignPlatformMap;
};