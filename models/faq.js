'use strict';
module.exports = (sequelize, DataTypes) => {
  const Faq = sequelize.define('Faq', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    image: DataTypes.STRING,
    status: {
      type: DataTypes.ENUM('Active', 'Inactive'),
      defaultValue: 'Active'
    },
    createdAt: {type: Sequelize.DATE, field: 'created_at'},
    updatedAt: {type: Sequelize.DATE, field: 'updated_at'},
  }, {
    // define the table's name
    tableName: 'faq',
    //freezeTableName: true,
    //underscored: true,
    // don't add the timestamp attributes (updatedAt, createdAt)
    timestamps: true,
  });
  Faq.associate = function(models) {
    // associations can be defined here
  };
  return Faq;
};