'use strict';
module.exports = (sequelize, DataTypes) => {
  const InfluencerAdminTransaction = sequelize.define('InfluencerAdminTransaction', {
    id: {
      type: DataTypes.BIGINT,
      primaryKey: true,
      autoIncrement: true,
    },
    brand_owner_admin_transaction_id: DataTypes.BIGINT,
    influencer_id: DataTypes.INTEGER,
    total_amount: DataTypes.DECIMAL,
    net_amount: DataTypes.DECIMAL,
    admin_commission_influncer_pertg: DataTypes.STRING,
    admin_commission_influncer_amt: DataTypes.DECIMAL,
    paypal_charge_influencer_pertg: DataTypes.STRING,
    paypal_charge_influencer_amt: DataTypes.DECIMAL,
    //currency: DataTypes.STRING,
    currency_fee_pertg: DataTypes.STRING,
    currency_fee_amt: DataTypes.DECIMAL,
    default_currency_id: DataTypes.INTEGER,
    influencer_currency_id: DataTypes.INTEGER,
    payout_mail:DataTypes.STRING,
    transaction_id: DataTypes.STRING,
    invoice_number: DataTypes.STRING,
    payout_type: {
      type: DataTypes.ENUM('Auto', 'Manual'),
      defaultValue: 'Auto'
    },
    payout_status: {
      type: DataTypes.ENUM('SUCCESS','FAILED','PENDING','UNCLAIMED','RETURNED','ONHOLD','BLOCKED','REFUNDED','REVERSED'),
      defaultValue: 'PENDING'
    },
    manual_payout_status: {
      type: DataTypes.ENUM('PENDING','SUCCESS'),
      defaultValue: 'PENDING'
    },
    manual_payout_on: {type: Sequelize.DATE, field: 'created_at'},
    transaction_details: DataTypes.JSON,
    createdAt: {type: Sequelize.DATE, field: 'created_at'},
    updatedAt: {type: Sequelize.DATE, field: 'updated_at'},
  }, {
    // define the table's name
    tableName: 'influencer_admin_transaction',
    // don't add the timestamp attributes (updatedAt, createdAt)
    timestamps: false,
  });

  InfluencerAdminTransaction.associate = function(models) {
   
  };

  return InfluencerAdminTransaction;
};