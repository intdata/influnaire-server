'use strict';
module.exports = (sequelize, DataTypes) => {
  const Testimonial = sequelize.define('Testimonial', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    author: DataTypes.STRING,
    designation: DataTypes.STRING,
    description: DataTypes.STRING,
    image: DataTypes.STRING,
    status: DataTypes.INTEGER,
    createdAt: {type: Sequelize.DATE, field: 'created_at'},
    updatedAt: {type: Sequelize.DATE, field: 'updated_at'},
    created_by: DataTypes.INTEGER,
    updated_by: DataTypes.INTEGER,
  }, {
    // define the table's name
    tableName: 'testimonial',
    // don't add the timestamp attributes (updatedAt, createdAt)
    timestamps: false,
  });
  Testimonial.associate = function(models) {
    // associations can be defined here
  };
  return Testimonial;
};