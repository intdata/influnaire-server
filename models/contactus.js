'use strict';
module.exports = (sequelize, DataTypes) => {
  const ContactUs = sequelize.define('ContactUs', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    name: DataTypes.STRING,
    email: DataTypes.STRING,
    subject: DataTypes.TEXT,
    message: DataTypes.TEXT,
    creator_type: {
      type: DataTypes.ENUM('Influencer', 'BrandOwner', 'Other'),
      defaultValue: 'Other'
    },
    created_by: DataTypes.INTEGER,
    createdAt: {type: Sequelize.DATE, field: 'created_at'},
    updatedAt: {type: Sequelize.DATE, field: 'updated_at'},
  }, {
    // define the table's name
    tableName: 'contact_us',
    // don't add the timestamp attributes (updatedAt, createdAt)
    timestamps: true,
  });
  ContactUs.associate = function(models) {
    // associations can be defined here
  };
  return ContactUs;
};