'use strict';
module.exports = (sequelize, DataTypes) => {
  const CampaignHistory = sequelize.define('CampaignHistory', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
    },
    campaign_id: DataTypes.INTEGER,
    influencer_id: DataTypes.INTEGER,
    brand_owner_id: DataTypes.INTEGER,
    action: DataTypes.TEXT,
    taken_by: DataTypes.ENUM('Influencer', 'BrandOwner'),
    createdAt: {type: Sequelize.DATE, field: 'created_at'},
    updatedAt: {type: Sequelize.DATE, field: 'updated_at'},
  }, {
    // define the table's name
    tableName: 'campaign_history',
    // don't add the timestamp attributes (updatedAt, createdAt)
    timestamps: true,
  });
  return CampaignHistory;
};