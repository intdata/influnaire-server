'use strict';
module.exports = (sequelize, DataTypes) => {
  const notification = sequelize.define('notification', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    title: DataTypes.TEXT,
    content: DataTypes.TEXT,
    sender_id: DataTypes.INTEGER,
    receiver_id: DataTypes.INTEGER,
    campaign_id: DataTypes.INTEGER,
    receiver_type: {
      type: DataTypes.ENUM('Influencer', 'BrandOwner'),
      defaultValue: null
    },
    deleted_by_sender: {
      type: DataTypes.ENUM('0', '1'),
      defaultValue: '0'
    },
    deleted_by_receiver: {
      type: DataTypes.ENUM('0', '1'),
      defaultValue: '0'
    },
    status: {
      type: DataTypes.ENUM('0', '1'),
      defaultValue: '1'
    },
    createdAt: {type: Sequelize.DATE, field: 'created_at'},
    updatedAt: {type: Sequelize.DATE, field: 'updated_at'},
  }, {
    // define the table's name
    tableName: 'notification',
    // don't add the timestamp attributes (updatedAt, createdAt)
    timestamps: true,
  });
  notification.associate = function(models) {
    // associations can be defined here
  };
  return notification;
};