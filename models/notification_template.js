'use strict';
module.exports = (sequelize, DataTypes) => {
  const NotificationTemplate = sequelize.define('NotificationTemplate', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    lang_code: DataTypes.STRING, 
    alias: DataTypes.STRING,
    title: DataTypes.STRING,
    content: DataTypes.STRING,
    status: DataTypes.INTEGER,
    createdAt: {type: Sequelize.DATE, field: 'created_at'},
    updatedAt: {type: Sequelize.DATE, field: 'updated_at'},
  }, {
    // define the table's name
    tableName: 'notification_template',
    // don't add the timestamp attributes (updatedAt, createdAt)
    timestamps: true,
  });
  NotificationTemplate.associate = function(models) {
    // associations can be defined here
  };
  return NotificationTemplate;
};