'use strict';
module.exports = (sequelize, DataTypes) => {
  const CampaignInfluencerPlatformMap = sequelize.define('CampaignInfluencerPlatformMap', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    campaign_id: DataTypes.INTEGER,
    influencer_id: DataTypes.INTEGER,
    platform_id: DataTypes.INTEGER,
    social_media_token: DataTypes.STRING,
    auth: DataTypes.STRING,
    channel_id: DataTypes.STRING,
    total_like: DataTypes.STRING,
    total_post: DataTypes.INTEGER,
    followers: DataTypes.INTEGER,
    save_posts: DataTypes.INTEGER,
    total_comments: DataTypes.STRING,
    imptession_ratio: DataTypes.STRING,
    engagement_ratio: DataTypes.STRING,
    createdAt: {type: Sequelize.DATE, field: 'created_at'},
    updatedAt: {type: Sequelize.DATE, field: 'updated_at'},
  }, {
    // define the table's name
    tableName: 'campaign_influencer_platform_map',
    // don't add the timestamp attributes (updatedAt, createdAt)
    timestamps: true,
  });
  CampaignInfluencerPlatformMap.associate = function(models) {
    // associations can be defined here
  };
  return CampaignInfluencerPlatformMap;
};