'use strict';
module.exports = (sequelize, DataTypes) => {
  const CampaignMessageConversation = sequelize.define('CampaignMessageConversation', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
    },
    campaign_id: DataTypes.INTEGER,
    influencer_id: DataTypes.INTEGER,
    brand_owner_id: DataTypes.INTEGER,
    file: DataTypes.TEXT,
    message: DataTypes.TEXT,
    message_from: DataTypes.ENUM('Influencer', 'BrandOwner'),
    created_at: DataTypes.DATE,
    updated_at: DataTypes.DATE
  }, {
    // define the table's name
    tableName: 'campaign_message_conversation',
    // don't add the timestamp attributes (updatedAt, createdAt)
    timestamps: false,
  });
  return CampaignMessageConversation;
};