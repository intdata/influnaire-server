var express = require('express');
var path = require('path');
//var favicon = require('serve-favicon');
var logger = require('morgan');
var fs = require('fs');
var path = require('path');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var swaggerUi = require('swagger-ui-express'),
swaggerDocument = require('./swagger.json');

global.jwt = require('jsonwebtoken');
global.passport = require('passport');
global.passportJWT = require('passport-jwt');
// var session      = require('express-session');
// var flash    = require('connect-flash');
const config = require("./config/default.json");

//**=============== Sequalize init =============== */
mysql = require("mysql");
mysql_config = require("./config/mysql_config.js");
Sequelize = require("sequelize");

global.con = mysql.createConnection(mysql_config);

con.connect(function(err) {
  if (err) throw err;
  console.log("Mysql Connected with Sequalize!");
});

global.sequelize = new Sequelize(
  mysql_config.database,
  mysql_config.user,
  mysql_config.password,
  {
    host: mysql_config.host,
    dialect: "mysql",
    //operatorsAliases: false,
    // dialectOptions: {
    //     timezone: mysql_config.timezone
    // },
    dialectOptions: {
      //useUTC: false, //for reading from database
      dateStrings: true,
      typeCast: true,
      timezone: "+05:30"
    },
    timezone: "+05:30", //for writing to database
    pool: {
      max: 5,
      min: 0,
      acquire: 30000,
      idle: 10000,
    }
  }
);

sequelize
  .authenticate()
  .then(() => {
    console.log("Connection has been established successfully.");
  })
  .catch(err => {
    console.error("Unable to connect to the database:", err);
  });

//**=============== Sequalize commit =============== */

//use config module to get the privatekey, if no private key set, end the application
if (!config.myprivatekey) {
  console.error("FATAL ERROR: myprivatekey is not defined.");
  process.exit(1);
}

var app = express();

app.use(express.static(config.assetpath));

/////////////////// Passing db Object for router ends//////////////////////////
app.use(function (req, res, next) {         
   //res.setHeader('Access-Control-Allow-Origin', config.accessurl); 
   res.setHeader('Access-Control-Allow-Origin', '*'); 
   //res.setHeader('Access-Control-Allow-Origin', 'http://10.0.4.133:4204');  
   //res.setHeader('Access-Control-Allow-Origin', 'http://111.93.177.60'); 
   res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');    
   res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type,apikey,authorization');      
   res.setHeader('Access-Control-Allow-Credentials', true);      
   next();  
});

//**=============== import modules =============== */
var general = require('./routes/index');
var campaign = require('./routes/campaign');
var campaignAssign = require('./routes/campaign-assign');
var brand = require('./routes/brand');
var brandOwner = require('./routes/brandOwner');
var message = require('./routes/message');
var platform = require('./routes/platform');
var influncer = require('./routes/influncer');
var verification = require('./routes/verify');
var users = require('./routes/users');
var auth = require('./routes/auth');
var helpCenter = require('./routes/help-center');
var invitation = require('./routes/invitation');
var proposal = require('./routes/proposal');
var platform = require('./routes/platform');
var job = require('./routes/job');
var notification = require('./routes/notification');
var scheduler = require('./routes/scheduler');
var googleConnect = require('./routes/google_connect');
var paypal = require('./routes/paypal');
var paypalPayout = require('./routes/paypal_payout');
var pdf = require('./routes/pdf');
var setting = require('./routes/setting');




//**=============== Passport init =============== */
// app.use(session({
//   secret: 'ilovescotchscotchyscotchscotch', // session secret
//   resave: true,
//   saveUninitialized: true
// }));

let ExtractJwt = passportJWT.ExtractJwt;
global.JwtStrategy = passportJWT.Strategy;

global.jwtOptions = {};
jwtOptions.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
jwtOptions.secretOrKey = config.myprivatekey;

require('./config/passport')(passport); // pass passport for configuration

app.use(passport.initialize());
//app.use(passport.session()); // persistent login sessions
//app.use(flash()); // use connect-flash for flash messages stored in session

//**=============== Passport commit =============== */

var engines = require('consolidate');

app.engine('html', engines.mustache);
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'html');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
// log all requests to access.log
app.use(logger('common', {
  stream: fs.createWriteStream(path.join(__dirname, 'access.log'), { flags: 'a' })
}))
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));


//**=============== set initial routes =============== */
var basePath = '/api/v1';
app.use(basePath + '/', general);
app.use(basePath + '/verify', verification);
app.use(basePath + '/campaign', campaign);
app.use(basePath + '/campaign-assign', campaignAssign);
app.use(basePath + '/brand-owner', brandOwner);
app.use(basePath + '/brand', brand);
app.use(basePath + '/message', message);
app.use(basePath + '/platform', platform);
app.use(basePath + '/influncer', influncer);
app.use(basePath + '/users', users);
app.use(basePath + '/auth', auth);
app.use(basePath + '/help-center', helpCenter);
app.use(basePath + '/invitation', invitation);
app.use(basePath + '/proposal', proposal);
app.use(basePath + '/platform', platform);
app.use(basePath + '/job', job);
app.use(basePath + '/notification', notification);
app.use(basePath + '/scheduler', scheduler);
app.use(basePath + '/google_connect', googleConnect);
app.use(basePath + '/paypal', paypal);
app.use(basePath + '/paypal_payout', paypalPayout);
app.use(basePath + '/pdf', pdf);
app.use(basePath + '/setting', setting);


//**=============== swagger =============== */
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

// const expressSwagger = require('express-swagger-generator')(app);
 
// let options = {
//     swaggerDefinition: {
//         info: {
//             description: 'This node front end server',
//             title: 'Influnaire',
//             version: '1.0.0',
//         },
//         host: 'http://10.0.4.235:8001',
//         basePath: basePath,
//         produces: [
//             "application/json",
//             "application/xml"
//         ],
//         schemes: ['http', 'https'],
//         securityDefinitions: {
//             JWT: {
//                 type: 'apiKey',
//                 in: 'header',
//                 name: 'Authorization',
//                 description: "",
//             }
//         }
//     },
//     basedir: __dirname, //app absolute path
//     files: ['./routes/**/*.js'] //Path to the API handle folder
// };
// expressSwagger(options);


//**=============== error handeler =============== */
// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    app.set('view engine', 'pug');
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  app.set('view engine', 'pug');
  res.render('error', {
    message: err.message,
    error: {}
  });
});

app.listen(config.port);
//app.listen(app.get('port'));
console.log('server start running at port ' + config.port);
module.exports = app;
