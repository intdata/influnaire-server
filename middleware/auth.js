module.exports = function(req, res, next) {
  //get the token from the header if present
  const token = req.headers["x-access-token"] || req.headers["authorization"];
  //if no token found, return response (without going to the next middelware)
  if (!token) return res.status(200).send({responseCode: 401, responseDetails: 'Access denied. No Auth token provided.'});

  try {
    
    //if can verify the token, set req.user and pass to next middleware
    const decoded = jwt.verify(token.replace('Bearer ', ''), jwtOptions.secretOrKey);
    
    req.userInfo = decoded;
    next();
  } catch (ex) {
    //if invalid token
    res.status(200).json({responseCode: 400, responseDetails: 'Invalid Auth token.'});
  }
};